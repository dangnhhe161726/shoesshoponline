CREATE DATABASE  IF NOT EXISTS `shoes_shop_online` /*!40100 DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `shoes_shop_online`;
-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: localhost    Database: shoes_shop_online
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorization`
--

DROP TABLE IF EXISTS `authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorization` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL,
  `url` varchar(100) NOT NULL,
  `feature` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorization`
--

LOCK TABLES `authorization` WRITE;
/*!40000 ALTER TABLE `authorization` DISABLE KEYS */;
INSERT INTO `authorization` VALUES (107,0,'/home','Home Page'),(108,1,'/home','Home Page'),(109,2,'/home','Home Page'),(110,3,'/home','Home Page'),(111,4,'/home','Home Page'),(112,5,'/home','Home Page'),(113,0,'/categoryblog','Blogs List'),(114,1,'/categoryblog','Blogs List'),(115,2,'/categoryblog','Blogs List'),(116,3,'/categoryblog','Blogs List'),(117,4,'/categoryblog','Blogs List'),(118,5,'/categoryblog','Blogs List'),(119,0,'/blogdetailcontrol','Blog Details'),(120,1,'/blogdetailcontrol','Blog Details'),(121,2,'/blogdetailcontrol','Blog Details'),(122,3,'/blogdetailcontrol','Blog Details'),(123,4,'/blogdetailcontrol','Blog Details'),(124,5,'/blogdetailcontrol','Blog Details'),(125,0,'/product','Products List'),(126,1,'/product','Products List'),(127,2,'/product','Products List'),(128,3,'/product','Products List'),(129,4,'/product','Products List'),(130,5,'/product','Products List'),(131,0,'/productdetail','Product Details'),(132,1,'/productdetail','Product Details'),(133,2,'/productdetail','Product Details'),(134,3,'/productdetail','Product Details'),(135,4,'/productdetail','Product Details'),(136,5,'/productdetail','Product Details'),(137,0,'/cartdetail','Cart Details'),(138,4,'/cartdetail','Cart Details'),(139,4,'/checkoutcartdetail','Cart Details'),(140,4,'/cartcontact','Cart Contact'),(141,4,'/feedback','Feedback'),(142,0,'/login','User Login '),(143,0,'/register','User Register'),(144,0,'/forgotpassword','Reset Password'),(145,0,'/newpassword','New Password'),(146,0,'/validateotp','Validate OTP'),(147,0,'/verify','Verify by Email'),(148,3,'/addcustomer','Add Customer'),(149,3,'/searchcustomer','Search Customers'),(150,1,'/changepassword','Change Password'),(151,2,'/changepassword','Change Password'),(152,3,'/changepassword','Change Password'),(153,4,'/changepassword','Change Password'),(154,5,'/changepassword','Change Password'),(155,1,'/editprofile','User Profile'),(156,2,'/editprofile','User Profile'),(157,3,'/editprofile','User Profile'),(158,4,'/editprofile','User Profile'),(159,5,'/editprofile','User Profile'),(160,4,'/myorder','My Order'),(161,4,'/orderdetail','Order Information'),(162,3,'/marketingdashboard','Marketing Dashboard'),(163,3,'/postmanager','Posts List'),(164,3,'/postdetail','Post Details'),(165,3,'/slidermanager','Sliders List'),(166,3,'/detailslider','Slider Details'),(167,3,'/productlistmanager','Products List (Marketing)'),(169,3,'/listcustomer','Customers List'),(170,3,'/detailcustomer','Customer Details'),(171,3,'/listfeedbackmanager','Feedbacks List'),(172,3,'/detailfeedbackmanager','Feedback Details'),(173,3,'/loadpost','Load Post'),(174,3,'/loadslider','Load Slider'),(176,2,'/orderdashboard','Sale Dashboard'),(177,5,'/orderdashboard','Sale Dashboard'),(178,2,'/ordermanager','Orders List'),(179,5,'/ordermanager','Orders List'),(180,2,'/editorder','Order Details'),(181,5,'/editorder','Order Details'),(182,2,'/filteroder','Filter Orders'),(183,5,'/filteroder','Filter Orders'),(184,1,'/admindashboard','Admin Dashboard'),(185,1,'/userlistforadmin','Users List'),(186,1,'/usersearching','Users Searching'),(187,1,'/userfiltering','Users Filtering'),(188,1,'/userdetail','User Details'),(189,1,'/userupdate','User Update'),(190,1,'/adduser','Add new User'),(191,0,'/searchproduct','Search Product '),(192,1,'/searchproduct','Search Product '),(193,2,'/searchproduct','Search Product '),(194,3,'/searchproduct','Search Product '),(195,4,'/searchproduct','Search Product '),(196,5,'/searchproduct','Search Product '),(197,0,'/categorycontrol','Category Control'),(198,1,'/categorycontrol','Category Control'),(199,2,'/categorycontrol','Category Control'),(200,3,'/categorycontrol','Category Control'),(201,4,'/categorycontrol','Category Control'),(202,5,'/categorycontrol','Category Control'),(209,0,'/searchblog','Search Blog'),(210,1,'/searchblog','Search Blog'),(211,2,'/searchblog','Search Blog'),(212,3,'/searchblog','Search Blog'),(213,4,'/searchblog','Search Blog'),(214,5,'/searchblog','Search Blog'),(221,0,'/editcartdetail','Edit Cart Details'),(222,4,'/editcartdetail','Edit Cart Details'),(223,0,'/checkoutcartdetail','Checkout Cart Detail'),(224,4,'/checkoutcartdetail','Checkout Cart Detail'),(225,1,'/logout','Log out'),(226,2,'/logout','Log out'),(229,3,'/logout','Log out'),(230,4,'/logout','Log out'),(231,5,'/logout','Log out'),(232,0,'/contact','Contact'),(233,4,'/contact','Contact'),(234,4,'/buybackproduct','Buy Back Product'),(235,1,'/authorizationmanagement','Authorization Management'),(236,1,'/authorizationinsert','Authorization Insert'),(237,1,'/authorizationremove','Authorization Remove'),(238,2,'/editorder','Order Edit'),(240,5,'/editorder','Order Edit'),(241,0,'/logingooglehandler','Login via Google'),(242,3,'/editslider','Edit Slider'),(243,3,'/deleteslider','Delete Slider'),(244,3,'/addslider','Add Slider'),(245,4,'/payment','Cart Payment'),(246,4,'/checkoutvnpay','Checkout VN Pay'),(247,3,'/editproductmanager','Edit Product Details of Marketing'),(248,3,'/editandupdatesizeproductmanager','Edit Size'),(249,3,'/productdetailmanager','View Product Detail'),(250,3,'/addsubimageproduct','Add sub image of product'),(251,5,'/editorder2','Edit order for sale'),(252,2,'/editorder1','Edit Status Order'),(253,3,'/filterproductlistmanager','Filter Product List'),(254,3,'/addproductdetailmanager','Add Product'),(255,3,'/addpost','Add Post'),(256,3,'/postmanager','Post Manager'),(257,3,'/filterpostlistmanager','Post Filter');
/*!40000 ALTER TABLE `authorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog` (
  `blog_id` int NOT NULL AUTO_INCREMENT,
  `title` text,
  `author_id` int DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text,
  `thumbnail` text,
  `brief_infor` text,
  `categoryBlog_id` int DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`blog_id`),
  KEY `fk_categoryBlog` (`categoryBlog_id`),
  KEY `fk_User_Blog` (`author_id`),
  CONSTRAINT `fk_categoryBlog` FOREIGN KEY (`categoryBlog_id`) REFERENCES `category_blog` (`categoryBlog_id`),
  CONSTRAINT `fk_User_Blog` FOREIGN KEY (`author_id`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (13,'CÁCH LOẠI BỎ VẾT ĐEN TRÊN GIÀY DA TRẮNG | MẸO VỆ SINH GIÀY',39,'2023-07-18 10:17:01','Làm sạch giày bằng bột baking soda. Trộn 2 muỗng canh. baking soda với 1 muỗng cà phê. nước ấm. Dùng vải sạch bôi hỗn hợp này lên vết đen. Theo các chuyên gia của tạp chí Good Housekeeping, nhẹ nhàng chà hỗn hợp này vào giày da trắng theo chuyển động tròn cho đến khi phủ kín bề mặt giày.  Lau giày sạch sẽ. Loại bỏ cặn baking soda bằng nước ấm. Lau sạch giày bằng khăn ẩm không có xà phòng. Để khô trong năm phút.  Nếu vẫn còn một vài vết đen, hãy bôi kem đánh răng màu trắng lên đồ da. Kem đánh răng làm trắng nhẹ nhàng cho men răng sẽ đánh bay các vết đen mà không làm hỏng lớp da trắng.  Tình trạng giày da sau khi làm sạch. Dưỡng ẩm cho da bằng kem đánh giày màu trắng dành cho giày da. Phủ một lớp sơn bóng trong để dưỡng da và ngăn da trắng bị sẫm màu. Đánh giày bằng bàn chải lông mềm để giày thêm sáng bóng.Mẹo Nếu các vết đen trên giày da của bạn bao gồm một lớp bùn hoặc bụi bẩn, hãy sử dụng khăn lau da đã được tẩm sấy để loại bỏ vết bẩn.  Cảnh báo Tránh dùng các hóa chất mạnh như dung dịch tẩy sơn móng tay axeton hoặc các chất tẩy rửa mạnh như Borax, Comet hoặc Ajax để tẩy vết đen trên giày da. Những hóa chất này có thể làm đổi màu da mà không loại bỏ các vết đen.  Cách vệ sinh giày thể thao da Khi lần đầu tiên bạn lôi giày thể thao da ra khỏi hộp để mang, hầu như không có vết ố hay vết bẩn trên của chúng bề mặt. Tuy nhiên, điều đó thay đổi khi mặc bình thường. Làm sạch đôi giày thể thao da của bạn khi chúng bị bẩn hoặc có vết xước sẽ giúp giữ cho chúng trông hấp dẫn.  Chải sạch bụi trên bề mặt bằng bàn chải lông mềm hoặc vải khô.  Đổ đầy nước sạch vào một thùng nhỏ và để sang một bên. Giữ một miếng bọt biển dưới vòi nước chảy để làm ẩm nó. Vắt nó để loại bỏ nước thừa.  Đổ một lượng Xà phòng dầu Murphy hoặc xà phòng lỏng nhẹ lên bề mặt miếng bọt biển với kích thước bằng một đồng xu. Dùng ngón tay chà xát lên bề mặt miếng bọt biển để phân phối xà phòng.  Vệ sinh giày bằng miếng bọt biển theo chuyển động tròn. Dùng áp lực để loại bỏ các vết bẩn cứng đầu.  Nhúng một miếng vải sạch vào bình chứa nước và vắt nhẹ để loại bỏ chất lỏng dư thừa. Lau giày bằng khăn ướt để loại bỏ cặn xà phòng.  Chà xát giày bằng vải khô để loại bỏ độ ẩm dư thừa. Để chúng khô trong một khu vực thông gió tốt.Mẹo vệ sinh giày Sử dụng sản phẩm tẩy ma thuật để loại bỏtrên bề mặt vết bẩn giày thể thao da trắng.  Để thao tác nhanh, hãy lau vết bùn hoặc vết bẩn trên da bằng khăn lau em bé hoặc khăn ẩm. Lau khô đồ da bằng vải khô để loại bỏ hết độ ẩm trên bề mặt.  Để đánh bóng giày sau khi làm sạch, hãy thoa một chút kem da có màu bóng phù hợp với màu giày thể thao của bạn lên giày. Nhúng một phần vải sạch vào kem và xoa vào giày thể thao theo chuyển động tròn. Đánh bóng giày bằng vải khô để loại bỏ phần kem thừa.  Cảnh báo không đặt giày da của bạn trong một máy giặt để làm sạch chúng. Điều này có thể làm cong vênh hoặc gây hại cho đôi giày.','cách-vệ-sinh-giày-sneaker,-giày-thể-thao-chất-liệu-da-nhân-tạo.jpg','CÁCH LOẠI BỎ VẾT ĐEN TRÊN GIÀY DA TRẮNG | MẸO VỆ SINH GIÀY',1,1),(14,'TOP 7 KHĂN LAU GIÀY SNEAKERS TỐT NHẤT 2023',38,'2023-07-18 10:35:33','Khi bạn không có thời gian để sử dụng bộ dụng cụ vệ sinh giày thể thao đắt tiền và làm sạch từng chi tiết kỹ trên đôi giày yêu thích của mình, thì khăn lau là giải pháp phù hợp. Đừng nghĩ rằng bất kỳ khăn lau nào cũng có thể làm sạch giày. Hầu hết các loại khăn lau chẳng hạn như khăn ướt kháng khuẩn không được sản xuất riêng để làm sạch giày và thậm chí có thể làm hỏng kết cấu giày.  Hãy cùng điểm qua 7 loại khăn lau giày sneaker tốt nhất hiện có trên thị trường vệ sinh giày hiện nay. Khăn lau giày Crep Protect Ultimate Crept Protect đã kinh doanh trong lĩnh vực làm sạch giày thể thao trong một thời gian dài và các sản phẩm của họ cũng có thể được tìm thấy tại các nhà bán lẻ giày lớn và phân phối tại nhiều quốc gia trên thế giới. Trong khi Crep Protect nổi tiếng với loại xịt Nano. Nếu xịt Nano tạo ra một lớp phủ trong suốt giúp chống thấm nước và ngăn ngừa vết bẩn trên giày thể thao, thì khăn lau giày Crep Protect Ultimate có một khả năng làm sạch cấp tốc các vết bẩn bám trên giày. Khăn vệ sinh giày Sneaker LAB Câu chuyện về Sneaker LAB là một câu chuyện độc đáo. Sneaker LAB là một thương hiệu Nam Phi và cung cấp sản phẩm chăm sóc giày cho hơn 60 quốc gia và con số này vẫn đang tiếp tục tăng. Đối với những người yêu thích bảo vệ môi trường ngoài kia, tất cả các sản phẩm của Sneaker LAB đều thân thiện với môi trường, bao gồm cả sản phẩm khăn vệ sinh giày sneakers. Sneaker LAB chính thức được chứng nhận Thẻ xanh và các sản phẩm của họ được đựng trong bao bì có thể tái chế. Dung dịch vệ sinh giày của Sneaker Lab không chứa xà phòng hoặc hóa chất mạnh và có thể phân hủy sinh học. Nếu bạn đang tìm kiếm loại khăn lau thân thiện với môi trường thì Sneaker LAB Sneaker Wipes là một lựa chọn tuyệt vời. Khăn Lau SneakerRescue Nếu bạn đang muốn làm sạch giày thể thao của mình bằng các nguyên liệu hoàn toàn tự nhiên, thì Khăn lau giày SneakerRescue là một lựa chọn tuyệt vời. Khăn lau SneakerRescue Cleaning Wipes cũng tự quảng cáo là khăn lau đa năng, vì vậy chúng không chỉ có thể làm sạch giày thể thao mà còn cả túi đựng giày, áo khoác da và các vật dụng khác. Công thức hoàn toàn tự nhiên và không chứa hóa chất tẩy rửa mạnh nên khá an toàn cho da. Một thương hiệu chăm sóc giày thể thao đáng tin cậy khác là JasonMarkk. Bạn có thể tìm thấy các sản phẩm của họ tại rất nhiều quốc gia và các sàn TMĐT lớn như Amazon, Shopee,... Khăn lau nhanh JasonMarkk có kết cấu kép: mặt mềm dùng để làm sạch vết bẩn thông thường và mặt còn lại bao gồm các chấm nổi dành cho các vết bẩn cứng đầu hơn.  Pack 30 gói được đựng trong hộp với khăn lau được gói riêng, giúp bạn thuận tiện tách lẻ từng gói để mang theo. CleanKicks Shoe Cleaner Wipes CleanKicks là một trong những thương hiệu hàng đầu trong lĩnh vực làm sạch giày thể thao. Điều tuyệt vời về CleanKicks là toàn bộ dòng sản phẩm của công ty đặc biệt tập trung vào việc chăm sóc giày. Công ty tự mô tả là “công ty chăm sóc giày cao cấp hàng đầu”. Khăn lau giày CleanKicks được xếp hạng 4,5 sao trên Amazon và hơn 1.350 đánh giá khiến nó trở thành một trong những loại khăn ướt vệ sinh giày được đánh giá cao nhất trên Amazon. Tuy nhiên, hiện tại tìm mua sản phẩm này tại Việt Nam vẫn tương đối khó. Simple Shine. Shoe Wipes Điều tuyệt vời của những khăn lau này là chúng không chỉ dành cho giày thể thao mà còn có tác dụng với nhiều loại giày dép khác như giày công sở, giày lười, sandals, giày cao gót, giày bệt và boots. Chúng cũng tương đối rẻ với mức giá 30 khăn lau  10$ (khoảng ~230.000 VND). GOAT Premium Shoe Cleaner Wipes Nếu bạn không biết GOAT là viết tắt của từ gì, thì nó là từ viết tắt của “Greatest of All Time”. Mặc dù rất khó để nói liệu Khăn lau giày cao cấp của GOAT có thực sự là khăn lau giày tốt nhất mọi thời đại hay không, nhưng chúng đích thực là một loại khăn vệ sinh giày tốt.  GOAT cam kết rằng những khăn lau này sẽ hoạt động trên hầu hết các loại vải bao gồm da, nubuck, da lộn, vải bạt, nhựa vinyl, lưới và vải dệt kim. Lưu ý khi mua khăn lau giày Khi mua khăn vệ sinh giày, cân nhắc kết cấu của khăn lau, hóa chất được sử dụng và loại vật liệu nào chúng có thể được sử dụng. Nhiều khăn lau có họa tiết, hoặc có họa tiết ở một mặt và nhẵn ở mặt kia, để làm sạch các vết bẩn trên các chất liệu khác nhau. Lời khuyên và mẹo lau giày •	Cân nhắc tình trạng dị ứng và tình trạng da khi chọn khăn vệ sinh giày. Vì những khăn lau này được sản xuất để sử dụng bằng tay nên chúng đã sử dụng các hóa chất và vật liệu an toàn cho da, nhưng nếu bạn có làn da nhạy cảm, dị ứng hoặc các tình trạng khác, bạn nên kiểm tra kĩ bảng thành phần. •	Da lộn và nubuck là hai chất liệu khó làm sạch. Nếu bạn sở hữu những đôi giày như thế này, hay những đôi giày bằng vải, sử dụng sợi dệt, dệt kim, hãy tìm khăn lau dành riêng cho những chất liệu này. •	Luôn thử khăn lau trên một khu vực nhỏ và kín của giày (patch test) trước khi sử dụng khăn lau trên phần còn lại.','anh_blog_7.jpg','Là một sneakerhead, một trong những nhiệm vụ khó khăn và phiền toái nhất là giữ cho đôi giày thể thao của bạn luôn sạch sẽ. Có quá nhiều việc phải làm và quá ít thời gian. Khăn lau giày sneaker là lựa chọn hoàn hảo dành cho bạn.',4,1),(15,'LÀM THẾ NÀO ĐỂ LÀM VỆ SINH GIÀY THUYỀN (BOAT SHOES) ',38,'2023-07-18 10:42:11','Các bước vệ sinh giày thuyền tại nhà Làm theo các bước sau để làm sạch giày thuyền của bạn.  Bước 1. Tháo dây giày  Nếu giày thuyền của bạn có dây buộc có thể tháo rời, hãy tháo chúng ra và giặt riêng. Hầu hết dây buộc có thể được làm sạch bằng hỗn hợp đơn giản gồm nước và xà phòng rửa bát. Nếu giày không có dây buộc rời (hoặc không có dây buộc) thì bạn có thể bỏ qua bước này.  Bước 2. Vệ sinh giày thuyền  Lấy một miếng vải sợi nhỏ, làm ướt bằng nước thường và dùng nó để lau bên ngoài đôi giày. Đừng ngâm giày nếu không da sẽ bắt đầu thấm nước. Thao tác này sẽ loại bỏ mọi chất bẩn nhẹ, bụi, mảnh vụn hoặc cát. Bạn có thể làm điều này thường xuyên nếu muốn đôi giày thuyền của mình được làm sạch nhanh chóng giữa các lần vệ sinh. Bạn cũng có thể sử dụng chất tẩy rửa đồ da chuyên dụng tùy thuộc vào mức độ bẩn của chúng.  Bước 3. Thoa dầu dưỡng da  Sau khi làm sạch, hãy đảm bảo rằng giày của bạn đã khô hoàn toàn trước khi chuyển sang bước này. Thấm một ít chất dưỡng da vào một miếng giẻ (tốt nhất là sợi nhỏ) sau đó nhẹ nhàng xoa vào giày. Xoa đều để đảm bảo nó không ảnh hưởng đến màu sắc giày. Lau sạch phần dầu thừa bằng giẻ sạch. Vì phần dầu dưỡng da dư thừa có thể hút bụi bẩn và gây ra tác dụng ngược lại. Không nên đánh bóng cho giày thuyền.','anh_blog_5.jpg','Không mất nhiều thời gian để vệ sinh giày thuyền. Nhưng có sẵn những vật dụng phù hợp sẽ giúp quá trình làm sạch giày nhanh hơn và dễ dàng hơn, mang lại kết quả tốt hơn',4,1),(16,'TOP 5 NƯỚC TẨY TRẮNG GIÀY CỦA NHẬT ĐÁNG MUA NHẤT HIỆN TẠI',38,'2023-07-18 10:43:45','1. Lợi ích khi sử dụng nước tẩy trắng giày Một số lợi ích của việc sử dụng nước tẩy trắng giày mà ít người biết đến bao gồm:  Giúp đôi giày của bạn luôn sạch sẽ, bền đẹp và chống bám bụi mọi lúc.  Thời gian tẩy ố bằng nước tẩy trắng giày nhanh hơn nhiều so với các phương pháp truyền thống.  Phù hợp với nhiều loại giày khác nhau, giữ nguyên dáng giày, không ảnh hưởng đến cấu trúc dáng giày, tăng tuổi thọ cho sản phẩm. Sức mạnh làm sạch hiệu quả đối với những vết bẩn “cứng đầu” trên giày.  Thao tác sử dụng nước tẩy trắng giày để làm sạch giày đơn giản và nhanh chóng, chỉ cần đọc hướng dẫn sử dụng là có thể thực hiện dễ dàng.  Chất tẩy có mùi thơm tự nhiên, không nồng, không gây bết dính, đặc biệt đảm bảo an toàn cho sức khỏe người dùng và da tay.  Dễ dàng tìm mua nước tẩy trắng giày với giá cả phải chăng tại các cửa hàng giày dép, trung tâm thương mại, cửa hàng tạp hóa, v.v.','anh_blog_8.jpg','Để đôi giày luôn trắng sáng, sạch sẽ giúp bạn tự tin bước xuống phố, nước vệ sinh giày là sản phẩm hữu hiệu dành cho bạn. Nước tẩy trắng giày của Nhật là dòng sản phẩm được nhiều người tiêu dùng đánh giá cao và chất lượng. Dưới đây là top 5 sản phẩm vệ sinh giày của Nhật được yêu thích nhất.',4,1),(17,'HƯỚNG DẪN VỆ SINH GIÀY CAO GÓT TẠI NHÀ VÔ CÙNG ĐƠN GIẢN CHO HỘI CHỊ EM',38,'2023-07-18 10:45:48','1. Cách vệ sinh giày cao gót đúng tại nhà Vệ sinh mặt bên ngoài giày Với chất liệu da lộn: Đây là chất liệu khó vệ sinh và dễ hỏng khi vệ sinh không đúng cách, bạn nên tham khảo bài viết chi tiết Cách chăm sóc giày da lộn đúng chuẩn nhé!  Với các chất liệu da mờ, da bóng: Làm sạch các vết bẩn thông thường bên ngoài bắng khăn ẩm hoặc bàn chải mềm. Các vết bẩn cứng đầu bám dính bạn có thể dùng dung dịch nước giấm pha loãng với tỉ lệ 1giấm/5 nước. Hoặc dùng cồn y tế 70% có bán tại các tiệm thuốc tây để chùi, sau đó làm sạch lại với nước. Bạn tránh sử dụng hóa chất tại các vị trí có vết xước lớn hoặc da giày đang bị bong tróc. Việc làm này có thể vô tình khiến cho tính trạng hư hỏng của da thêm nghiêm trọng. Với các vết xước lớn, bong da bạn nên sử dụng các dịch vụ của các bên chuyên nghiệp để có thể xử lý trong tình trạng tốt nhất.Với chất liệu vải/satin/nhung: Đây là các chất liệu tương tự với quần áo và bạn có thể tự vệ sinh giày tại nhà bằng các dung dịch tẩy rửa có sẵn trong gia đình. Chỉ cần pha loãng các dung dịch như nước rửa chén hoặc bột giặt có tính tẩy vừa phải. Sau đó dùng bàn chải mềm hoặc khăn chà nhẹ lên vị trí các vết bám bẩn. Sau đó làm sạch lại bằng khăn ẩm và phơi giày ở nơi khô thoáng, tránh ánh nắng trực tiếp.','anh_blog_1.jpg','Đổi với chị em chúng ta, giày cao gót là một người bạn đồng hành không thể thiếu trong các sự kiện quan trọng và cần sự nữ tính. Một đôi giày đẹp và hợp với quần áo sẽ giúp nâng tầm bản thân bạn lên trong mắt mọi người xung quanh. Tất nhiên một đôi giày cao gót đẹp chắc chắc phải sạch và chỉnh chu. Vậy làm sao để đôi giày cao gót của chị em mình luôn như mới. Cùng xem hướng dẫn chăm sóc, cách vệ sinh giày cao gót tại nhà cho hội chị em nhé.',4,1),(18,'CÁCH CHỌN DÂY GIÀY PHÙ HỢP VÀ ĐẸP NHẤT CHO GIÀY CỦA BẠN',38,'2023-07-18 10:48:18','1. Chọn chất liệu dây giày Bước đầu tiên khi chọn dây giày là xem xét chất liệu. Dây giày được làm từ nhiều chất liệu khác nhau. Chất liệu được sử dụng cho dây giày phụ thuộc vào loại sợi. Các loại sợi phổ biến được sử dụng trong dây giày ngày nay là cotton, polyester kết cấu, polyester kéo thành sợi, nylon và polypropylene.   Riêng phần mút đầu dây giày (anglet) thường là một miếng nhựa hoặc kim loại bọc đầu dây giày lại. Mục đích của đầu dây giày này là giúp xỏ dây dễ dàng hơn và trang trí cho dây giày thêm đẹp. Một số anglet thậm chí còn được thiết kế với các ký tự độc đáo để nâng cao trải nghiệm của khách hàng.2. Thử nhiều loại dây khác nhau Để chọn dây giày ưng ý và đẹp nhất, hãy thử nhiều loại dây với nhiều màu sắc khác nhau. Dây giày có vẻ đơn giản và bình thường đối với một số người, nhưng chúng lại là một sản phẩm rất khác biệt. Từ các chất liệu truyền thống đến các chất liệu kỳ lạ và tổng hợp mọi màu sắc, các loại dây giày rất đa dạng.  Bạn có thể lựa chọn những màu nóng, rực rỡ cho những đôi giày mang ngoài trời hay những màu sắc pastel và gam màu lạnh cho văn phòng. Các màu dây giày cơ bản luôn có sẵn. Chúng chỉ bị giới hạn bởi số lượng hàng trong kho của cửa hàng mà bạn ghé mua.   Dây giày đầy màu sắc có thể tạo nên một phong cách thời trang thú vị cho mọi người ở mọi lứa tuổi. Thiết kế và hoa văn trên dây giày khác nhau tùy theo từng nhà bán lẻ và có thể bao gồm các thiết kế đơn giản như hoa văn hình, nhiều màu sắc hoặc thiết kế nhân vật phức tạp như siêu anh hùng hay xe hơi ở các loại dây giày không buộc dành cho trẻ em.  Tại Extrim có dây dẹt với hơn 40 phân loại màu cho bạn tha hồ lựa chọn dây giày có màu sắc mình thích. ','anh_bia_4.jpg','Nếu chẳng may bạn bị mất hoặc đứt dây giày thì buộc phải mua lại một loại dây giày mới cho giày của mình. Những điều bạn sẽ băn khoăn khi mua dây giày mới như là nên chọn loại dây giày nào, màu sắc nào sẽ phù hợp với giày, độ dài dây giày khoảng bao nhiêu,...Dây giày là thứ không thể thiếu của đôi giày và phần nào thể hiện cá tính, phong cách của người mang. Vì vậy việc chọn dây giày tưởng chừng đơn giản nhưng đòi hỏi bạn phải có một số hiểu biết nhất định để chọn được dây giày phù hợp cho mỗi đôi giày. Dây giày có nhiều kiểu dáng, chất liệu, màu sắc và độ dài khác nhau. Trong bài viết này Extrim sẽ chia sẻ cho bạn cách chọn dây giày đẹp và phù hợp nhất cho mỗi đôi giày.',5,1);
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Giày bóng đá',1),(2,'Giày quần vợt',1),(3,'Giày golf',1),(4,'Dép thể thao',1),(5,'Giày tennis',1),(6,'Giày tập gym',1),(7,'Giày yoga',1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_blog`
--

DROP TABLE IF EXISTS `category_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_blog` (
  `categoryBlog_id` int NOT NULL AUTO_INCREMENT,
  `categoryBlog_name` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`categoryBlog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_blog`
--

LOCK TABLES `category_blog` WRITE;
/*!40000 ALTER TABLE `category_blog` DISABLE KEYS */;
INSERT INTO `category_blog` VALUES (1,'Sửa chữa giày',1),(2,'Những tips nhỏ để bảo quản giày',1),(3,'Các mẫu giày hot trend 2023',1),(4,'Vệ sinh giày',1),(5,'Xu hướng giày trong tương lai',1);
/*!40000 ALTER TABLE `category_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback` (
  `feedBack_id` int NOT NULL AUTO_INCREMENT,
  `fullName` varchar(100) DEFAULT NULL,
  `rated_star` float DEFAULT NULL,
  `feedback` text,
  `image` text,
  `status` tinyint(1) DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`feedBack_id`),
  KEY `FK_Feedback_product_id` (`product_id`),
  KEY `FK__Feedback__userId__403A8C7D` (`userId`),
  CONSTRAINT `FK__Feedback__userId__403A8C7D` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`),
  CONSTRAINT `FK_Feedback_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (50,'Hàng dùng tuyệt vời',5,'Chất liệu khá tốt, shop giao hàng nhanh','tải xuống.jpg',0,68,35,'2023-07-18 08:52:20'),(51,'fsdfds',4,'vccc','2-1653445668-926-width740height481.jpg',1,65,36,'2023-07-24 08:45:18'),(52,'csas',5,'sccc','hfpqyV7B-IMG-Dubai-UAE.jpg',1,65,36,'2023-07-24 09:24:52'),(53,'sdfsđs',5,'cccc','62ed6ea71018a57a3ab0c8c959d78cb0.jpg',1,1,42,'2023-07-24 09:48:56'),(54,'css',1,'acss','62ed6ea71018a57a3ab0c8c959d78cb0.jpg',1,1,42,'2023-07-24 09:49:20'),(55,'daasss',2,'cccsss','62ed6ea71018a57a3ab0c8c959d78cb0.jpg',1,4,42,'2023-07-24 10:12:25'),(56,'sản phẩm đẹp',4,'giầy chắc đi mọi địa hình phù hợp với tôi với các cặp đôi khi đi chơi','hfpqyV7B-IMG-Dubai-UAE.jpg',1,3,42,'2023-07-24 10:14:28'),(57,'Giày đẹp',5,'Giày Siêu đẹp','Screenshot 2023-07-22 153754.png',1,39,36,'2023-07-25 15:05:20');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `orderDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_cost` float DEFAULT NULL,
  `fullName` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status_order` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `saler_id` int DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `FK__Order__status_or__76969D2E` (`status_order`),
  KEY `FK__Order__userId__412EB0B6` (`userId`),
  CONSTRAINT `FK__Order__status_or__76969D2E` FOREIGN KEY (`status_order`) REFERENCES `status_order` (`status_order_id`),
  CONSTRAINT `FK__Order__userId__412EB0B6` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (67,'2023-07-18 08:47:17',1250000,'Đăng Hồng Nguyễn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam,Xã Pom Lót,Huyện Điện Biên,Tỉnh Điện Biên',4,35,40,'abc'),(68,'2023-07-18 09:00:00',12050000,'Đăng Hồng Nguyễn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam,Xã Lâu Thượng,Huyện Võ Nhai,Tỉnh Thái Nguyên',1,36,37,''),(69,'2023-07-24 08:41:36',10050000,'Đăng Hồng Nguyễn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam,Xã Phúc Lộc,Huyện Ba Bể,Tỉnh Bắc Kạn',1,36,37,'fdsfs'),(70,'2023-07-24 08:58:25',6750000,'Đăng Hồng Nguyễn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam,Xã Yên Lương,Huyện Thanh Sơn,Tỉnh Phú Thọ',5,36,37,''),(71,'2023-07-24 09:23:45',9050000,'Đăng Hồng Nguyễn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam,Xã Đình Chu,Huyện Lập Thạch,Tỉnh Vĩnh Phúc',1,36,NULL,'cass'),(72,'2023-07-24 09:45:05',15050000,'Dũng Nguyễn','0857432886','số nhà 6 ngõ 470/17 đường Nguyễn Trãi Quận Thanh Xuân,Phường Hàng Trống,Quận Hoàn Kiếm,Thành phố Hà Nội',4,42,37,'ngugnug'),(73,'2023-07-24 10:10:00',13850000,'Dũng Nguyễn','0857432886','số nhà 6 ngõ 470/17 đường Nguyễn Trãi Quận Thanh Xuân,Xã Pải Lủng,Huyện Mèo Vạc,Tỉnh Hà Giang',4,42,37,'vvvaaamm'),(74,'2023-07-24 10:18:11',2048000,'Dũng Nguyễn','0857432886','số nhà 6 ngõ 470/17 đường Nguyễn Trãi Quận Thanh Xuân,Xã Lũng Cú,Huyện Đồng Văn,Tỉnh Hà Giang',2,42,37,''),(75,'2023-07-24 10:20:55',8450000,'Đăng Hồng Nguyễn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam,Xã Tống Trân,Huyện Phù Cừ,Tỉnh Hưng Yên',2,35,37,'hàng mà không ổn thì đánh thằng đăng'),(76,'2023-07-24 10:23:05',3400000,'Dũng Nguyễn','0857432886','số nhà 6 ngõ 470/17 đường Nguyễn Trãi Quận Thanh Xuân,Phường Kim Liên,Quận Đống Đa,Thành phố Hà Nội',3,42,37,'hàng cần nguyên mới'),(77,'2023-07-25 08:12:44',5050000,'Dinh Manh','0973344977','Cam Ranh, Khánh Hòa,Xã Lăng Can,Huyện Lâm Bình,Tỉnh Tuyên Quang',5,46,NULL,'ok'),(78,'2023-07-25 13:43:38',31150000,'Dinh Manh','0973344977','Cam Lâm, Khánh Hòa,Phường Gia Thụy,Quận Long Biên,Thành phố Hà Nội',1,47,NULL,'abc xyz'),(79,'2023-07-25 14:58:00',39050000,'Đăng Hồng Nguyễn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam,Phường Lý Thái Tổ,Quận Hoàn Kiếm,Thành phố Hà Nội',4,36,37,'abc xyz');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_detail` (
  `orderDetail_id` int NOT NULL AUTO_INCREMENT,
  `quantity` int DEFAULT NULL,
  `order_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `total_cost` int DEFAULT NULL,
  `size` int DEFAULT NULL,
  PRIMARY KEY (`orderDetail_id`),
  KEY `FK__Order_Det__order__4222D4EF` (`order_id`),
  KEY `FK__Order_Det__produ__4316F928` (`product_id`),
  CONSTRAINT `FK__Order_Det__order__4222D4EF` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`),
  CONSTRAINT `FK__Order_Det__produ__4316F928` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail` VALUES (76,1,67,68,1200000,36),(77,3,68,65,12000000,37),(78,2,69,1,10050000,37),(79,1,70,3,5550000,36),(80,1,70,39,1250000,36),(81,3,71,10,9050000,36),(82,3,72,1,15050000,36),(83,2,73,4,2850000,36),(84,2,73,3,11050000,36),(85,2,74,40,2048000,36),(86,2,75,35,8450000,36),(87,1,76,45,1000000,36),(88,2,76,39,2450000,36),(89,1,77,1,5000000,37),(90,3,78,39,3600000,36),(91,5,78,3,27500000,36),(92,5,79,39,6000000,36),(93,6,79,3,33000000,36);
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `original_prices` float DEFAULT NULL,
  `sale_prices` float DEFAULT NULL,
  `product_details` text,
  `brief_infor` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  KEY `FK__Product__categor__440B1D61` (`category_id`),
  CONSTRAINT `FK__Product__categor__440B1D61` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'GIÀY ĐÁ BÓNG FIRM GROUND COPA PURE.1',5500000,5000000,'Tên gọi bộ môn thể thao vua là có lý do. adidas Copa Pure loại bỏ xao nhãng cho cảm giác thoải mái tuyệt đối và chạm bóng hoàn hảo. Đôi giày đá bóng siêu nhẹ này có mũi giày bằng da Fusionskin may chần kết hợp mượt mà với cổ giày bằng vải dệt cho cảm giác chạm bóng êm ái và độ ổn định vững chãi. Đế ngoài chuyên dụng đảm bảo đẳng cấp trên sân cỏ tự nhiên.','Bóng đá,Sân cỏ đất cứng,2 colour',1,1,'2023-07-25 15:12:41'),(2,'GIÀY ĐÁ BÓNG FIRM GROUND X SPEEDPORTAL.3',3000000,2500000,'<h1>KHÁM PHÁ Ý NGHĨA CỦA TỐC ĐỘ VỚI ĐÔI GIÀY ĐÁ BÓNG CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Tâm trí. Thể. Giày. Tất cả kết nối chỉ trong nháy mắt. Hãy mang giày adidas X Speedportal để khai phá tốc độ đa chiều. Với thiết kế cho phép lướt bay trên sân cỏ tự nhiên, đôi giày đá bóng này có thân giày làm bằng vải dệt siêu nhẹ, có lớp phủ ngoài cùng thêm hai đinh ở mũi giày. Sự kết hợp của cổ giày bằng vải dệt kim phẳng co giãn và đai kéo gót bằng TPU cứng cáp đảm bảo bạn luôn sẵn sàng cho mọi cú bứt tốc công phá và di chuyển ở hai biên.</p>','<p>Bóng đá •  Sân cỏ mềm • 2 màu</p>',1,5,'2023-07-20 14:52:04'),(3,'GIÀY ĐÁ BÓNG FIRM GROUND PREDATOR ACCURACY.1 LOW',5800000,5500000,'<h1>ĐÔI GIÀY ĐÁ BÓNG CỔ THẤP CHO LỐI CHƠI CHUẨN XÁC, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Khả năng kiểm soát + Độ chính xác = Sự tự tin. Khi mục tiêu đã nằm trong tầm ngắm, hãy hướng tới sự hoàn hảo với adidas Predator Accuracy. Mẫu giày đá bóng cổ thấp này có thân giày HybridTouch mềm mại với các gai cao su High Definition Grip ở vùng tiếp xúc bóng. Ngoài mục đích tạo độ bám bóng chắc chắn, thiết kế này còn đảm bảo mũi giày luôn linh hoạt cho cử động thoải mái. Phù hợp cho sân cỏ tự nhiên, đế ngoài phân tách có phần đệm đế trước Power Facet cho cú sút đầy uy lực.</p>','<p>Bóng đá • Sân cỏ đất cứng • 2 màu</p>',1,1,'2023-07-20 14:44:15'),(4,'GIÀY ĐÁ BÓNG FIRM GROUND X SPEEDPORTAL.3',1470000,1400000,'<h1>KHÁM PHÁ Ý NGHĨA CỦA TỐC ĐỘ VỚI ĐÔI GIÀY ĐÁ BÓNG CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Tâm trí. Thể. Giày. Tất cả kết nối chỉ trong nháy mắt. Hãy mang giày adidas X Speedportal để khai phá tốc độ đa chiều. Với thiết kế cho phép lướt bay trên sân cỏ tự nhiên, đôi giày đá bóng này có thân giày làm bằng vải dệt siêu nhẹ, có lớp phủ ngoài cùng thêm hai đinh ở mũi giày. Sự kết hợp của cổ giày bằng vải dệt kim phẳng co giãn và đai kéo gót bằng TPU cứng cáp đảm bảo bạn luôn sẵn sàng cho mọi cú bứt tốc công phá và di chuyển ở hai biên.</p>','<p>Bóng đá • Sân cỏ đất cứng • 2 màu</p>',1,1,'2023-07-20 14:46:12'),(5,'GIÀY ĐÁ BÓNG FIRM GROUND COPA PURE.1',3850000,3800000,'<h1>ĐÔI GIÀY ĐÁ BÓNG BẰNG DA SIÊU NHẸ VÀ KHÔNG ĐƯỜNG MAY CHO TRẢI NGHIỆM HOÀN HẢO.</h1>  <p>Tên gọi bộ môn thể thao vua là có lý do. adidas Copa Pure loại bỏ xao nhãng cho cảm giác thoải mái tuyệt đối và chạm bóng hoàn hảo. Đôi giày đá bóng siêu nhẹ này có mũi giày bằng da Fusionskin may chần kết hợp mượt mà với cổ giày bằng vải dệt cho cảm giác chạm bóng êm ái và độ ổn định vững chãi. Đế ngoài chuyên dụng đảm bảo đẳng cấp trên sân cỏ tự nhiên.</p>','<p>Bóng đá • Sân cỏ đất cứng • 3 màu</p>',1,1,'2023-03-02 10:00:00'),(6,'GIÀY BÓNG ĐÁ SÂN CỎ TỰ NHIÊN KHÔNG DÂY X SPEEDFLOW.3',2400000,2100000,'<h1>ĐÔI GIÀY ĐÁ BÓNG SIÊU NHẸ DÀNH CHO PHIÊN BẢN TỐC ĐỘ CỦA BẠN.</h1>  <p>Quan sát, đọc tình huống rồi dứt điểm. Và lặp lại. Khi tinh thần nhạy bén hòa hợp cùng sự nhanh nhẹn của cơ thể, bạn sẽ trở thành phiên bản nhanh nhất của chính mình. Hãy kiếm tìm nhịp điệu của bạn và bỏ lại mọi thứ phía sau. Thân giày Speedskin trong mờ của đôi giày bóng đá adidas X này giúp bạn luôn thoải mái khi bùng nổ sức mạnh chiến đấu. Bên dưới là đế ngoài bằng TPU giúp bạn bứt tốc trên mặt sân cỏ tự nhiên. Thiết kế không dây ôm chân vừa vặn giúp bạn vững vàng trong các pha đổi hướng và xoay người để luôn nhanh hơn một bước.</p>','<p>Bóng đá •  Sân cỏ tự nhiên • 2 màur</p>',1,1,'2023-04-03 10:00:00'),(7,'GIÀY TRAE UNLIMITED',2300000,2100000,'<h1>ĐÔI GIÀY BÓNG RỔ TRAE YOUNG CHO ĐỘ BÁM MƯỢT MÀ.</h1>  <p>Trae Young có lối chơi của riêng mình. Đôi giày bóng rổ adidas signature của anh có thiết kế cổ thấp và đế giữa Bounce linh hoạt theo mọi chuyển động của bạn. 3 Sọc classic nay khoác lên mình thiết kế răng cưa sần, chạy từ mũi giày đến phần giữa bàn chân. Đế ngoài bằng cao su với các vùng bám hai bên tạo độ ma sát khi bạn đổi hướng hoặc bứt tốc về phía rổ.</p>','<p>Bóng rổ •  mới</p>',1,1,'2022-12-31 10:00:00'),(8,'GIÀY TRAE 2',1900000,1800000,'<h1>ĐÔI GIÀY SIGNATURE ĐẾN TỪ ADIDAS BASKETBALL VÀ TRAE YOUNG.</h1>  <p>Khi áp lực dâng cao, Trae Young vẫn lạnh như băng. Anh là một trong những cầu thủ gan góc nhất — sẵn sàng đón bóng từ bất cứ vị trí nào trên sân hoặc chuyền bóng khi cả đội cần anh nhất. Với thiết kế nhằm nâng tầm lối chơi tốc độ và linh hoạt của Trae, đôi giày adidas Basketball này có đế giữa kết hợp đệm BOOST và Lightstrike cùng thân giày bằng vải dệt kim có dây giày bán phần, thích ứng theo bàn chân bạn. Nhờ đó bạn có thể tấn công rổ với cảm giác nâng đỡ và thoải mái tuyệt đối.</p>','<p>Bóng rổ • 1 màu</p>',1,1,'2023-05-04 10:00:00'),(9,'GIÀY HARDEN VOLUME 7',4500000,4200000,'<h1>ĐÔI GIÀY SIGNATURE ĐẾN TỪ ADIDAS BASKETBALL VÀ JAMES HARDEN.</h1>  <p></p>','<p>Bóng rổ • James Harden</p>',1,1,'2023-05-31 10:00:00'),(10,'GIÀY DAME 8',3200000,3000000,'<h1>ĐÔI GIÀY SIGNATURE DAMIAN LILLARD CÓ SỬ DỤNG THÀNH PHẦN TÁI CHẾ.</h1>  <p>Tôn trọng tuyệt đối. Damian Lillard đã làm nên tên tuổi nhờ ra tay vào đúng những khoảnh khắc then chốt, ở những tình huống ngàn cân treo sợi tóc. Đôi giày signature kết hợp giữa adidas Basketball và Dame này tôn vinh một trong những cầu thủ dứt điểm xuất sắc nhất trong làng bóng rổ, với họa tiết graphic Dame và thân giày đầy màu sắc lấy cảm hứng từ lần đầu tiên anh thi đấu và đánh bại một số cầu thủ bóng rổ xuất sắc nhất thế giới. Đế ngoài và đế giữa Bounce Pro có thiết kế mô phỏng lối chơi thần tốc và chuẩn xác đến mức hủy diệt của Dame.</p>','<p>Bóng rổ •  Damian Lillard •  2 màu</p>',1,1,'2023-05-04 10:00:00'),(11,'GIÀY ULTRABOOST LIGHT',3100000,3000000,'<h1>NĂNG LƯỢNG VƯỢT TRỘI. THANH THOÁT TRÊN CHÂN.</h1>  <p>Trải nghiệm nguồn năng lượng vượt trội với giày Ultraboost Light mới, phiên bản Ultraboost nhẹ nhất của chúng tôi. Sự kỳ diệu nằm ở đế giữa Light BOOST, thế hệ mới của đệm adidas BOOST. Thiết kế phân tử độc đáo của mẫu giày này đạt đến chất liệu mút xốp BOOST nhẹ nhất từ trước đến nay. Với hàng trăm hạt BOOST bùng nổ năng lượng cùng cảm giác êm ái và thoải mái tột đỉnh, đôi chân bạn thực sự sẽ được trải nghiệm tất cả.</p>','<p>Thể thao • 4 colour •  mới</p>',1,1,'2022-12-31 10:00:00'),(12,'GIÀY ULTRABOOST 1.0',4500000,4000000,'<h1>ĐÔI GIÀY SNEAKER HOÀN TRẢ NĂNG LƯỢNG CÓ SỬ DỤNG SỢI PARLEY OCEAN PLASTIC.</h1>  <p>Khi đi dạo trong công viên cũng như chạy bộ cuối tuần cùng bạn bè, đôi giày adidas Ultraboost 1.0 này sẽ giúp bạn luôn thoải mái. Thân giày adidas PRIMEKNIT ôm chân nhẹ nhàng, cùng đệm BOOST ở đế giữa nâng niu từ bước chân đầu tiên cho tới tận cây số cuối cùng. Đế ngoài Stretchweb linh hoạt tự nhiên cho sải bước tràn đầy năng lượng, cùng chất liệu cao su Continental™ Rubber tạo độ bám cần thiết để bạn luôn vững bước.</p>','<p>Chạy bộ •  1 màu</p>',1,1,'2023-02-11 10:00:00'),(13,'GIÀY TENNIS ADIZERO UBERSONIC 4',2400000,2280000,'<h1>ĐÔI GIÀY SÂN CỨNG GIÚP BẠN TĂNG TỐC, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Biểu hiện đỉnh cao của tốc độ trên sân tennis. Khi mang đôi giày adizero Ubersonic 4 siêu nhẹ này, bạn có thể chắc chắn rằng đây sẽ là trợ thủ cho lối chơi năng động của bạn. Với thiết kế dành cho lối đánh linh hoạt, thân giày bằng vải lưới dệt ôm khít và được gia cố từ bên trong để tăng cường ổn định khi di chuyển sang bên và trượt cứu bóng. Bên trên đế ngoài bền bỉ phù hợp mặt sân cứng là đế giữa Lightstrike cho cảm giác sát đất giúp bạn phản xạ nhanh.</p>','<p>Nữ • Quần vợt</p>',1,2,'2022-12-31 10:00:00'),(14,'GIÀY AVACOURT',3900000,3800000,'<h1>ĐÔI GIÀY TENNIS MỚI CỦA CHÚNG TÔI VỚI THIẾT KẾ ƯU TIÊN PHÁI NỮ.</h1>  <p>Giày AVACOURT được chế tác dựa trên nhu cầu của nữ giới và kích cỡ trung bình của vận động viên nữ nhằm mang lại sự ổn định và êm ái tại mọi vị trí cần thiết. Được trang bị đế giữa Bounce Pro siêu nhẹ, mẫu giày này được chế tác dựa trên nhu cầu của nữ giới và kích cỡ trung bình của vận động viên nữ. Hệ thống Torsion System bằng TPU đem đến sự cân bằng tối ưu giữa độ ổn định và linh hoạt cho nữ giới, đồng thời cổ giày làm từ chất liệu mềm mại nhằm mang lại sự thoải mái tại vị trí nơi bàn chân nữ giới có xu hướng bị cọ xát nhiều hơn.</p>','<p>Nữ • Quần vợt</p>',1,2,'2023-05-31 10:00:00'),(15,'GIÀY Nike Legend Essential 3 Next Nature',1909000,1500000,'<h1>ĐÔI TRAINER CHO CẢM GIÁC THOẢI MÁI TUYỆT VỜI.</h1>  <p>Gặp gỡ huấn luyện viên đủ linh hoạt để chịu được sự khắc nghiệt của lớp nhóm nhịp độ nhanh hoặc một ngày nặng nhọc trong phòng tập tạ. Được trang bị gót phẳng, vật liệu có độ mài mòn cao và đế linh hoạt, nó mang đến sự thoải mái và hỗ trợ sẵn sàng để đến phòng tập. Xem các đốm trên đế ngoài? Điều đó có nghĩa là nó được chế tạo với ít nhất 8% vật liệu Nike Grind, được làm từ phế liệu từ quy trình sản xuất giày dép.﻿</p>','<p>Nam • Gym</p>',1,2,'2023-04-06 10:00:00'),(16,'GIÀY Nike Metcon 8 AMP',4109000,3999000,'<h1>Nhiều lợi ích hơn</h1>  <p>Lưới nhẹ với lớp phủ kết cấu ở những khu vực có độ mài mòn cao giúp giữ cho bàn chân của bạn mát mẻ mà không làm giảm độ bền.</p>','<p>Nam • Gym </p>',1,2,'2023-01-31 10:00:00'),(17,'GIÀY FLUIDFLOW 2.0',2099000,1999000,'<h1>ĐÔI TRAINER CHO CẢM GIÁC THOẢI MÁI TUYỆT VỜI.</h1>  <p>Lịch trình hôm nay của bạn có bao gồm chạy bộ hay không cũng chẳng quan trọng. Cảm giác siêu thoáng khí và đế giữa êm ái trợ lực mang đến lợi thế cho đôi giày adidas này. Bất kể chạy bộ hay xuống phố. Bạn đã có cho mình đôi giày sẵn sàng đồng hành và chinh phục ngày mới.</p>','<p>Nam • Gym</p>',1,2,'2023-05-31 10:00:00'),(18,'GIÀY ZNCHILL LIGHTMOTION+',3000000,2975000,'<h1>ĐÔI GIÀY SNEAKER THỂ THAO VỚI CÔNG NGHỆ THÔNG MINH, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Thêm một ví dụ về tinh hoa sneaker của adidas. 3 Sọc đặt bên trên đế giữa LIGHTMOTION tạo lớp đệm thoải mái. Đế ngoài bằng cao su hầm hố tăng thêm độ bám khi di chuyển giữa các bề mặt trơn trượt và gồ ghề. Bạn sẽ không cần thay giày, ngay cả khi lịch trình dày đặc.</p>','<p>Nam • Gym • Mới </p>',1,2,'2023-05-31 10:00:00'),(19,'GIÀY ADIDAS X MARIMEKKO SUPERNOVA 2.0',3200000,2720000,'<h1>ĐÔI GIÀY CHẠY BỘ ADIDAS X MARIMEKKO CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Marimekko sáng tạo nên các họa tiết nhằm khích lệ mọi người biểu đạt bản thân. Với một runner, điều đó đồng nghĩa với việc xỏ đôi giày chạy bộ adidas của bạn và xuất phát. Bất kể bạn chạy bộ vì mục tiêu thành tích hay muốn cho đầu óc minh mẫn, đôi giày này sẽ hỗ trợ bạn nhờ đế giữa kết hợp. Đệm Bounce bật nảy kết hợp cùng đệm BOOST đàn hồi cho cảm giác mềm mại và tràn đầy năng lượng.</p>','<p>Nam • Chạy</p>',1,2,'2023-05-25 10:00:00'),(20,'GIÀY DURAMO SL 2.0',1600000,1330000,'<h1>ĐÔI GIÀY CHẠY BỘ ĐI HÀNG NGÀY, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Khi tập gym buổi sáng, làm việc vặt trong ngày hay đi chơi tối cùng bạn bè, hãy giữ cho đôi chân bạn luôn thoải mái và đảm bảo phong cách chuẩn chỉnh với đôi giày adidas này. Kết hợp cùng chiếc quần short chạy bộ ưa thích, bạn sẽ có được phong cách thể thao mà casual. Thân giày bằng vải lưới giúp bạn luôn mát mẻ, cùng lớp đệm siêu nhẹ nâng niu từng sải bước.</p>','<p>Nam • Chạy bộ</p>',1,2,'2022-12-11 10:00:00'),(21,'GIÀY CHẠY BỘ SUPERNOVA 2',2500000,2240000,'<h1>TẬN HƯỞNG CẢM GIÁC THOẢI MÁI VỚI GIÀY SUPERNOVA 2</h1>  <p>Chạy bộ đâu cần phải khó chịu? Đôi giày này mang lại cho bạn cảm giác thoải mái trong mọi buổi chạy, trên mọi địa hình. Từ sự kết hợp hoàn hảo giữa đệm Bounce và đệm Boost tăng cường ở đế giữa cho tới cấu trúc lưỡi gà và viền gót giày lót đệm hoàn toàn mới, hãy trải nghiệm cảm giác thoải mái trên từng bước chạy.</p>','<p>Nam • Chạy</p>',1,2,'2023-04-01 10:00:00'),(22,'GIÀY GALAXY 6.1',1445000,1330000,'<h1>ĐÔI GIÀY THỂ THAO THOẢI MÁI CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Xỏ giày và xuất phát. Đôi giày chạy bộ adidas này sẽ giúp bạn luôn thoải mái dẫu cho ngày mới kéo dài vô tận. Đây là một đôi giày hàng ngày tuyệt vời, với thân giày bằng vải lưới siêu nhẹ và thoáng khí giúp đôi chân bạn luôn mát mẻ, cùng đế giữa Cloudfoam đàn hồi. Đế ngoài bằng cao su bám vững vàng trên mọi bề mặt, từ sân cỏ ẩm ướt tới sân đất nện. Bạn có thể thoải mái thay đổi kế hoạch mà không cần đổi giày.</p>','<p>Nam • Chạy</p>',1,2,'2023-06-04 10:00:00'),(23,'GIÀY GALAXY 6',1000000,960000,'<h1>ĐÔI GIÀY THỂ THAO THOẢI MÁI CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Xỏ giày và xuất phát. Đôi giày chạy bộ adidas này sẽ giúp bạn luôn thoải mái dẫu cho ngày mới kéo dài vô tận. Đây là một đôi giày hàng ngày tuyệt vời, với thân giày bằng vải lưới siêu nhẹ và thoáng khí giúp đôi chân bạn luôn mát mẻ, cùng đế giữa Cloudfoam đàn hồi. Đế ngoài bằng cao su bám vững vàng trên mọi bề mặt, từ sân cỏ ẩm ướt tới sân đất nện. Bạn có thể thoải mái thay đổi kế hoạch mà không cần đổi giày.</p>','<p>Nam • Chạy</p>',1,3,'2023-01-08 10:00:00'),(24,'GIÀY TRAINER DROPSET',3500000,2450000,'<h1>ĐÔI GIÀY TẬP LUYỆN ĐA NĂNG CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Chinh phục các bài tập sức mạnh và tập gym không chút do dự. Đôi giày tập luyện adidas này giúp bạn nâng tầm cuộc chơi. Gót giày cứng cáp tạo độ ổn định khi bạn nâng tạ, và mũi giày mềm mại mang đến sự mềm dẻo trong những bài tập yêu cầu độ nhanh nhẹn và linh hoạt. Đế giữa mật độ kép giúp bàn chân luôn ổn định và thoải mái để bạn duy trì tập trung.</p>','<p>Nữ • Tập luyện</p>',1,3,'2023-02-17 10:00:00'),(25,'GIÀY TRAINER V',1540000,1399000,'<h1>ĐÔI GIÀY CHẠY BỘ CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Đôi giày adidas này kết hợp kiểu dáng và cảm giác của giày chạy bộ với phong cách phù hợp mọi lúc mọi nơi. Thân giày linh hoạt và cổ giày lót đệm Geofit tạo độ ôm vừa khít và co giãn vừa phải, cùng lớp đệm siêu nhẹ mang đến cảm giác thoải mái từ bước chân đầu tiên tới tận sải bước cuối cùng trong ngày.</p>','<p>Nam • tập luyện</p>',1,3,'2023-03-08 10:00:00'),(26,'GIÀY ALPHABOUNCE RC 2.0',2200000,2100000,'<h1>MẪU GIÀY NÀY HỖ TRỢ CẢ CHẠY BỘ VÀ LUYỆN TẬP.</h1>  <p>Mẫu giày trung tính đa năng để chạy hoặc tập bổ trợ dành cho vận động viên. Mẫu giày này có thân giày trên bằng vải lưới không đường may ôm trọn bàn chân như một đôi tất. Các phần hỗ trợ và co giãn cho phép chuyển động theo nhiều hướng. Lớp đệm đàn hồi, có độ bật nảy cao cho cảm giác thoải mái dài lâu.</p>','<p>Nam • Sportwear</p>',1,3,'2023-02-23 10:00:00'),(27,'RAPIDMOVE TRAINER W',3200000,3099000,'<h1>ĐÔI GIÀY DÀNH CHO HIIT VÀ CÁC BÀI TẬP LINH HOẠT, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Nhẹ nhàng chinh phục lớp HIIT cùng một đôi giày đáng tin cậy. Đôi giày tập adidas này hỗ trợ chuyển động linh hoạt. Thân giày bằng vải dệt kim kỹ thuật có các vùng nâng đỡ được bố trí hợp lý. Lớp đệm Lightstrike siêu nhẹ và đàn hồi, cùng hệ thống Torsion System bất đối xứng giúp bạn duy trì ổn định khi chuyển hướng nhanh và di chuyển đa hướng.</p>','<p>Nam • tập luyện</p>',1,3,'2022-12-11 10:00:00'),(28,'GIÀY GOLF ZOYSIA',3300000,2999000,'<h1>ĐÔI GIÀY GOLF THOẢI MÁI CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Tự tin thể hiện với đôi giày golf adidas thoải mái được thiết kế dành riêng cho nữ này. Với sự thoải mái và đa năng lấy cảm hứng từ giày chạy bộ, thân giày sử dụng kết hợp nhiều chất liệu và bề mặt hoàn thiện cho vẻ ngoài và cảm giác vừa vặn. Với đế ngoài đinh liền bằng cao su, đôi giày này cho phép bạn bước đi thoải mái, vung gậy tự tin và sải bước ra khỏi sân golf với phong cách đơn giản mà cực chất. Chơi golf theo cách của riêng bạn — thoải mái, tràn đầy năng lượng và sẵn sàng cho quãng thời gian giao lưu sau vòng golf.</p>','<p>Nữ • đánh golf</p>',1,3,'2023-04-19 10:00:00'),(29,'GIÀY GOLF LO BOOST ADICROSS',4000000,3900000,'<h1>ĐÔI GIÀY GOLF ĐINH LIỀN TÁO BẠO CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Cầm gậy, mang đôi giày golf adidas này và vạch ra hướng đi của riêng bạn trên sân. Mang cảm hứng phiêu lưu, đôi giày golf Lo Adicross kết hợp thiết kế táo bạo và công nghệ cao phù hợp ở cả trong và ngoài sân đấu. Từ thân giày kháng nước cho tới đế giữa BOOST toàn phần, đôi giày này giúp bạn sải bước trên sân golf với phong cách thời thượng và cảm giác sung sức. Đế ngoài đinh liền Gripmore® cho độ bám tăng cường, phù hợp ở sân golf tới sân tập cũng như khi xuống phố.</p>','<p>Đánh golf •  2 màu</p>',1,3,'2023-05-04 10:00:00'),(30,'GIÀY ĐINH LIỀN BOA CODECHAOS 22',4400000,4400000,'<h1>ĐÔI GIÀY GOLF ĐINH LIỀN CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Bước ra sân golf, nâng tầm phong cách và tăng cường phong độ. Mọi chi tiết của đôi giày golf đinh liền BOA adidas CODECHAOS22 đều có tính năng riêng. Từ tư thế ổn định nhờ đế ngoài bao bọc tới độ bám hỗ trợ swing nhờ công nghệ Twist Grip và cảm giác trợ lực nhờ đế BOOST toàn phần, đôi giày này tối ưu mọi thứ bạn cần trên sân — bao gồm phong cách. Hệ thống BOA® Fit System giúp bạn tinh chỉnh độ ôm và nâng đỡ chỉ bằng một nút xoay. Hiệu năng tour kết hợp cùng phong cách đỉnh cao.</p>','<p>Nữ • Đánh golf</p>',1,3,'2023-02-02 10:00:00'),(31,'GIÀY GOLF STAN SMITH',3400000,3300000,'<h1>ĐÔI GIÀY GOLF PHONG CÁCH TENNIS CỔ ĐIỂN CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Dòng giày adidas Stan Smith trứ danh nay đã có mặt trên sân golf. Dành cho các vòng đấu giao lưu và những ngày casual, đôi Giày Golf Stan Smith này kết hợp phong cách tennis cổ điển với các chi tiết đậm chất golf. Đế ngoài đinh liền và đế giữa êm ái kết hợp cùng thân giày bằng da mang đến cho bạn phong cách phù hợp từ sân golf tới clubhouse và hơn thế nữa mà không có vẻ gì là vừa chơi một vòng golf.</p>','<p>Nam • Đánh golf</p>',1,3,'2022-12-31 10:00:00'),(32,'GIÀY GOLF SUPERSTAR',3400000,3300000,'<h1>ĐÔI GIÀY GOLF ADIDAS SUPERSTAR CỔ ĐIỂN CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Nâng tầm cuộc chơi đầy phong cách. Khởi động trên sân với phong cách biểu tượng của đôi Giày Golf adidas Superstar. Từ mũi vỏ sò bằng cao su cho tới thiết kế 3 Sọc classic, đôi giày golf phong cách bóng rổ này hòa trộn di sản adidas đặc trưng với thiết kế đế đinh tạo nền tảng vững chãi cho những cú drive mạnh mẽ và những cú putt chuẩn xác.</p>','<p>Nam •  Đánh golf</p>',1,3,'2023-02-01 10:00:00'),(33,'GIÀY PUREBOOST 23',3700000,3499000,'<h1>ĐÔI GIÀY CHẠY BỘ HÀNG NGÀY ĐÀN HỒI CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Chỉ cần tranh thủ chạy bộ một chút cũng có thể giúp bạn lấy lại tâm trạng và nạp đầy năng lượng cho ngày mới. Hãy mang đôi giày chạy bộ adidas rộng chiều ngang này để chinh phục cự ly mỗi ngày. Đế BOOST toàn phần hoàn trả năng lượng bất tận giúp bạn không ngừng tiến bước. Thân giày bằng vải lưới kỹ thuật giúp lưu thông khí, cùng đế ngoài bằng cao su Stretchweb linh hoạt tự nhiên cho sải bước ngập tràn năng lượng.</p>','<p>Chạy bộ nam •  mới •  2 màu</p>',1,6,'2023-07-20 14:57:50'),(34,'GIÀY ADIZERO ADIOS PRO 3',4500000,4400000,'<h1>ĐÔI GIÀY CHẠY BỘ ĐƯỜNG DÀI TỐC ĐỘ DÀNH CHO TẬP LUYỆN VÀ THI ĐẤU.</h1>  <p>Để đạt được mục tiêu, từng giây phút đều quan trọng. Phong độ xuất sắc đòi hỏi trang phục high-tech cho tốc độ tối ưu. Xin giới thiệu sản phẩm thi đấu chủ đạo siêu nhẹ mới nhất giúp bạn bứt phá giới hạn không chút phân tâm.</p>','<p>Chạy bộ •  pro</p>',1,2,'2023-07-20 14:58:15'),(35,'GIÀY ULTRABOOST 1.0',4500000,4200000,'<h1>ĐÔI GIÀY SNEAKER HOÀN TRẢ NĂNG LƯỢNG CÓ SỬ DỤNG SỢI PARLEY OCEAN PLASTIC.</h1>  <p>Khi đi dạo trong công viên cũng như chạy bộ cuối tuần cùng bạn bè, đôi giày adidas Ultraboost 1.0 này sẽ giúp bạn luôn thoải mái. Thân giày adidas PRIMEKNIT ôm chân nhẹ nhàng, cùng đệm BOOST ở đế giữa nâng niu từ bước chân đầu tiên cho tới tận cây số cuối cùng. Đế ngoài Stretchweb linh hoạt tự nhiên cho sải bước tràn đầy năng lượng, cùng chất liệu cao su Continental™ Rubber tạo độ bám cần thiết để bạn luôn vững bước.</p>','<p>Nữ sportswear •  2 màu</p>',1,1,'2023-07-20 14:59:07'),(36,'GIÀY ADIDAS SURU365 X DISNEY MICKEY MOUSE TRẺ EM',1600000,1499000,'<h1>ĐÔI GIÀY VUI NHỘN ĐẾN TỪ ADIDAS X DISNEY, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Hãy chuẩn bị để bé sẵn sàng phiêu lưu với đôi giày trẻ em adidas x Disney này. Nổi bật với họa tiết hình chú Chuột Mickey nhà Disney phủ toàn bộ, đôi giày trainer này khơi dậy niềm vui của bé trong mọi hoạt động. Quai dán giúp bé đi giày nhanh chóng. Kiểu dáng thoáng mát cùng 3 Sọc cho diện mạo mới mẻ.</p>','<p>Trẻ em • Sportswear</p>',1,1,'2023-07-20 14:59:31'),(37,'GIÀY TENNIS BARRICADE',4000000,2400000,'<h1>ĐÔI GIÀY SÂN CỨNG NÂNG ĐỠ CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Thống trị sân đấu để kiểm soát đối thủ. Đôi giày tennis adidas Barricade này giúp bạn thể hiện hết mình để ghi điểm tuyệt đối. Hệ thống dây giày tiện dụng cho độ ôm tùy chỉnh đúng nghĩa và các hạt xốp Sensepod Geofit giúp cố định gót chân. Bên dưới là đế giữa Bounce êm ái và phần shank bằng TPU giữa bàn chân mang lại sự thoải mái và ổn định. Dây giày lệch về một bên để tránh bị ảnh hưởng do những cú trượt cứu bóng gắt gao, làm nên một đôi giày bền bỉ hệt như lối chơi của bạn.</p>','<p>Nữ • Quần vợt</p>',1,1,'2023-07-20 14:59:46'),(38,'GIÀY TENNIS ADIZERO UBERSONIC 4',3800000,2280000,'<h1>ĐÔI GIÀY SÂN CỨNG GIÚP BẠN TĂNG TỐC, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p>Tốc độ giúp bạn tiết kiệm thời gian. Thời gian quý báu giúp bạn ghi điểm. Chính vì vậy đôi giày tennis adidas Adizero Ubersonic 4 này ra đời nhằm giúp bạn chạm tới bóng nhanh hơn. Thân giày bằng vải dệt co giãn ôm chân cùng đệm gót giày bên ngoài cho cảm giác chắc chắn khi bạn bứt tốc. Các vùng gia cố bên trong tăng cường ổn định khi di chuyển sang bên và trượt cứu bóng. Đế giữa Lightstrike thanh mảnh kết hợp với đế ngoài Adiwear siêu bám đảm bảo bạn luôn phản ứng nhanh nhất trên mặt sân cứng.</p>','<p>Nữ  •  Quần vợt</p>',1,1,'2023-07-20 15:00:02'),(39,'GIÀY GRAND COURT CLOUDFOAM LIFESTYLE COURT COMFORT',2000000,1200000,'<h1>ĐÔI GIÀY CASUAL VỚI 3 SỌC ÁNH KIM, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1>  <p></p>','<p>Nữ  •  Quần vợt</p>',1,1,'2023-07-20 15:00:32'),(40,'DÉP ADILETTE 22',1400000,999000,'<h1>ĐÔI DÉP ĐẬM CHẤT TƯƠNG LAI, CÓ NGUỒN GỐC TỰ NHIÊN.</h1>  <p>Để làm ra thiết kế cho đôi dép adidas này, chúng tôi đã tìm kiếm bản đồ địa hình minh họa những chuyến du hành tới sao Hoả và những giai đoạn không gian khác nhau của một hành tinh mới. Nguồn cảm hứng đến từ tương lai không chỉ dừng lại ở đó. Dép còn được làm từ chất liệu sử dụng cây mía, đây là bước tiến hướng đến một tương lai bền vững hơn. Hãy đi đôi dép này trên nhiều loại địa hình từ ướt đến khô.</p>','<p>Nam  •  Cổ điển</p>',1,4,'2023-01-31 10:00:00'),(41,'DÉP ADILETTE COMFORT',1200000,1000000,'<h1>ĐÔI SANDAL THỂ THAO KINH ĐIỂN CHO CẢM GIÁC NHẸ NHÀNG.</h1>  <p>adilette đã trở thành đã trở thành item ưa thích trên bãi biển và hồ bơi kể từ năm 1972. Họa tiết logo adidas Badge of Sport dập nổi trên quai dép mềm mại mang đến chất thể thao cho phiên bản mới của đôi dép biểu tượng này. Lòng dép ôm chân mang đến độ vững chãi đã làm nên tên tuổi của mẫu dép này.</p>','<p>Nam  •  Dép </p>',1,4,'2023-01-04 10:00:00'),(42,'DÉP ADILETTE AQUA',700000,600000,'<h1>ĐÔI DÉP THANH THOÁT VÀ DỄ ĐI CHO CẢM GIÁC KHÔ RÁO THOẢI MÁI.</h1>  <p>Đôi khi thiết kế đơn giản là thiết kế tuyệt vời nhất, và đôi dép adidas này khắc họa rõ nét tinh thần đó. Với cấu trúc nguyên khối, lòng dép đúc bằng nhựa EVA nâng niu từng sải bước và ráo nước nhanh. Kiểu dáng thuôn gọn tôn vinh khí chất adidas: vẻ ngoài thanh thoát phù hợp cho mọi ngày.</p>','<p>Nam  •  Thể thao</p>',1,4,'2023-02-03 10:00:00'),(43,'DÉP ADILETTE ADIFOM',1500000,1400000,'<h1>ĐÔI DÉP ADIDAS ADILETTE ĐỘT PHÁ VỚI THIẾT KẾ NGUYÊN KHỐI.</h1>  <p>Đôi dép adidas kết hợp cảm giác thoải mái và biểu đạt bản thân với thiết kế đậm chất tương lai lấy cảm hứng từ siêu vũ trụ. Tinh thần bền vững toát lên trên kiểu dáng adilette classic, đôi dép này có quai bằng mút foam nguyên khối. 3 Sọc màu tương phản thể hiện niềm tự hào thương hiệu.</p>','<p>Nam  •  Cổ điển </p>',1,4,'2023-01-01 10:00:00'),(44,'DÉP NHÀ TẮM ADILETTE',600000,550000,'<h1>DÉP QUAI NGANG LÓT ĐỆM CHO BÉ CẢM GIÁC THOẢI MÁI SAU GIỜ BƠI.</h1>  <p>Nuông chiều đôi chân bé khi ra khỏi bể bơi với đôi dép trẻ em thoải mái này. Nhanh khô và mềm mại, đôi dép này nâng niu bàn chân bé với lớp đệm siêu nhẹ. Biểu tượng adidas cỡ lớn táo bạo cho vẻ ngoài kinh điển.</p>','<p>Trê em • Thể thao •  3 màu  </p>',1,4,'2022-11-30 10:00:00'),(45,'DÉP XỎ NGÓN ADICANE',1000000,950000,'<h1>ĐÔI DÉP SANDAL THANH LỊCH CÓ NGUỒN GỐC TỰ NHIÊN.</h1>  <p>Khi thư giãn bên hồ bơi hay dạo bước trên bờ biển, đôi dép xỏ ngón adidas này sẽ mang đến phong cách cho bạn. Thiết kế xỏ ngón classic dễ mang, cùng lòng dép đúc giúp đôi chân bạn luôn thoải mái vào những ngày nóng nực nhất.</p>','<p>Thể thao • 2 màu </p>',1,4,'2023-01-04 10:00:00'),(46,'DÉP SỤC AQUALETTE OCEAN ADIDAS X MARIMEKKO',2000000,1700000,'<h1>ĐÔI DÉP SỤC ADIDAS X MARIMEKKO ĐỒNG HÀNH CÙNG BẠN KHI VẬN ĐỘNG CŨNG NHƯ THƯ GIÃN.</h1> <p>Sau thời gian luyện tập cật lực, vẻ ngoài nâng tầm là cách hoàn hảo để tiếp tục duy trì năng lượng. Chính là đôi Dép Sục adidas adilette: một lựa chọn dễ dàng để bỏ vào trong túi tập gym hoặc mang vào mỗi khi về nhà. Từ lòng dép chạm khắc tới họa tiết cá tính từ nhà thiết kế Phần Lan Marimekko, đôi dép sục này sẽ hỗ trợ đôi chân bạn và tiếp thêm sức mạnh cần thiết để giúp bạn giữ vững động lực cho mục tiêu sắp tới.</p>','<p>Nữ • Sportswear </p>',1,5,'2023-04-01 10:00:00'),(47,'DÉP ADILETTE PREMIUM',2100000,1800000,'<h1>ĐÔI DÉP DỄ CHỊU KẾT HỢP CÁC CHẤT LIỆU THANH THOÁT.</h1> <p>Những đôi dép casual luôn sẵn sàng mỗi khi bạn cần nhanh chóng có được cảm giác thoải mái. Nhưng đôi dép adidas adilette này không chỉ dừng lại ở đó. Sử dụng các chất liệu thời trang đưa phong cách lên tầm cao mới, mẫu dép đa năng này sẽ nâng tầm mọi outfit của bạn. Quai dép bằng da và lớp lót cho phong cách và cảm giác tinh tế, cùng lòng dép bằng chất liệu bần độc đáo nâng niu từng sải bước. Thiết kế quai ngang tạo hiệu ứng sần, và logo Ba Lá gần gót chân mang đến nét thể thao.</p>','<p>Nam • Cổ điển</p>',1,5,'2023-01-03 10:00:00'),(48,'GIÀY ĐÁ BÓNG FIRM GROUND PREDATOR ACCURACY.1 LOW',6000000,5800000,'<h1>ĐÔI GIÀY ĐÁ BÓNG CỔ THẤP CHO LỐI CHƠI CHUẨN XÁC, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1> <p>Khả năng kiểm soát + Độ chính xác = Sự tự tin. Khi mục tiêu đã nằm trong tầm ngắm, hãy hướng tới sự hoàn hảo với adidas Predator Accuracy. Mẫu giày đá bóng cổ thấp này có thân giày HybridTouch mềm mại với các gai cao su High Definition Grip ở vùng tiếp xúc bóng. Ngoài mục đích tạo độ bám bóng chắc chắn, thiết kế này còn đảm bảo mũi giày luôn linh hoạt cho cử động thoải mái. Phù hợp cho sân cỏ tự nhiên, đế ngoài phân tách có phần đệm đế trước Power Facet cho cú sút đầy uy lực.</p>','<p>Bóng đá  • Sân cỏ thât, đất cứng  • 3 màu </p>',1,5,'2023-03-31 10:00:00'),(49,'GIÀY ĐÁ BÓNG FIRM GROUND LOW PREDATOR ACCURACY.3',2100000,1999000,'<h1>CHINH PHỤC MỤC TIÊU VỚI ĐÔI GIÀY ĐÁ BÓNG CỔ THẤP CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1> <p>Kiểm soát tốt + độ chính xác cao = sự tự tin. Khi mục tiêu đã nằm trong tầm ngắm, hãy hướng tới sự hoàn hảo với adidas Predator Accuracy. Đôi giày đá bóng này có thân giày cổ thấp bằng vải dệt có lớp phủ ngoài, cùng vân nổi High Definition Texture phủ khắp vùng tiếp xúc bóng. Các chi tiết dập chìm và họa tiết in nổi kết hợp giúp bạn kiểm soát mọi đường chuyền và tấn công. Bên dưới là đế ngoài chuyên dụng giúp bạn thống trị mặt sân cỏ tự nhiên.</p>','<p>Bóng đá  • Sân cỏ thât, đất cứng  • 3 màu</p>',1,5,'2023-05-31 10:00:00'),(50,'X GHOSTED.3 FG',1800000,1700000,'<h1>ĐÔI GIÀY ĐÁ BÓNG CỔ THẤP CHO LỐI CHƠI CHUẨN XÁC, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1> <p>Khả năng kiểm soát + Độ chính xác = Sự tự tin. Khi mục tiêu đã nằm trong tầm ngắm, hãy hướng tới sự hoàn hảo với adidas Predator Accuracy. Mẫu giày đá bóng cổ thấp này có thân giày HybridTouch mềm mại với các gai cao su High Definition Grip ở vùng tiếp xúc bóng. Ngoài mục đích tạo độ bám bóng chắc chắn, thiết kế này còn đảm bảo mũi giày luôn linh hoạt cho cử động thoải mái. Phù hợp cho sân cỏ tự nhiên, đế ngoài phân tách có phần đệm đế trước Power Facet cho cú sút đầy uy lực.</p>','<p>Bóng đá  • Sân cỏ thât, đất cứng  </p>',1,5,'2023-01-24 10:00:00'),(51,'X GHOSTED.3 FG',1800000,1700000,'<h1>ĐÔI GIÀY ĐÁ BÓNG CỔ THẤP CHO LỐI CHƠI CHUẨN XÁC, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1> <p>Khả năng kiểm soát + Độ chính xác = Sự tự tin. Khi mục tiêu đã nằm trong tầm ngắm, hãy hướng tới sự hoàn hảo với adidas Predator Accuracy. Mẫu giày đá bóng cổ thấp này có thân giày HybridTouch mềm mại với các gai cao su High Definition Grip ở vùng tiếp xúc bóng. Ngoài mục đích tạo độ bám bóng chắc chắn, thiết kế này còn đảm bảo mũi giày luôn linh hoạt cho cử động thoải mái. Phù hợp cho sân cỏ tự nhiên, đế ngoài phân tách có phần đệm đế trước Power Facet cho cú sút đầy uy lực.</p>','<p>Bóng đá  • Sân cỏ thât, đất cứng  </p>',1,5,'2023-01-24 10:00:00'),(52,'GIÀY ĐÁ BÓNG FIRM GROUND X SPEEDPORTAL.3',2100000,1470000,'<h1>KHÁM PHÁ Ý NGHĨA CỦA TỐC ĐỘ VỚI ĐÔI GIÀY ĐÁ BÓNG CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.</h1> <p>Tâm trí. Thể. Giày. Tất cả kết nối chỉ trong nháy mắt. Hãy mang giày adidas X Speedportal để khai phá tốc độ đa chiều. Với thiết kế cho phép lướt bay trên sân cỏ tự nhiên, đôi giày đá bóng này có thân giày làm bằng vải dệt siêu nhẹ, có lớp phủ ngoài cùng thêm hai đinh ở mũi giày. Sự kết hợp của cổ giày bằng vải dệt kim phẳng co giãn và đai kéo gót bằng TPU cứng cáp đảm bảo bạn luôn sẵn sàng cho mọi cú bứt tốc công phá và di chuyển ở hai biên.</p>','<p>Bóng đá  • Sân cỏ thât, đất cứng  • 2  màu</p>',1,5,'2023-05-01 10:00:00'),(64,'Giày Thể Thao Adidas Duramo Sl Shoes FW8678 Màu Xanh Navy Size 39',900000,1000000,'Giày mềm mại, siêiu quấm hút','Giày-giày quần vợt',1,2,'2023-07-14 00:52:48'),(65,'GIÀY ULTRABOOST LIGHT 23',5200000,4000000,'ĐÔI GIÀY CHẠY BỘ THƯỜNG NGÀY SIÊU NHẸ CÓ SỬ DỤNG SỢI PARLEY OCEAN PLASTIC.\r\nDòng sản phẩm chạy bộ adidas mới nhất hòa trộn giữa phong cách và công năng, mang lại diện mạo và cảm giác thoải mái khi bạn chinh phục cự ly. Các sản phẩm chuyên dụng này được thiết kế nhằm giảm thiểu sao nhãng khi chạy bộ cường độ cao, nhờ đó bạn có thể tập trung tận hưởng buổi chạy.\r\n\r\nTận hưởng từng sải bước với đôi giày chạy bộ Ultraboost Light 23 này. Là mẫu giày Ultraboost nhẹ nhất của chúng tôi, đôi giày này làm từ chất liệu boost nhẹ hơn 30% vì chẳng ai muốn đôi chân nặng nề. Hàng trăm hạt boost bùng nổ nguồn năng lượng tràn trề mỗi khi bàn chân bạn tiếp đất.\r\n\r\nSản phẩm này có sử dụng sợi Parley Ocean Plastic. Đây là một trong số rất nhiều sáng kiến thể hiện cam kết của chúng tôi hướng tới Chấm Dứt Rác Thải Nhựa.','Core Black / Core Black / Lucid Lime',1,1,'2023-07-14 00:56:44'),(66,'GIÀY ULTRA 4D',3000000,2000000,'ĐÔI GIÀY HẤP THỤ LỰC TÁC ĐỘNG, CÓ SỬ DỤNG CHẤT LIỆU TÁI CHẾ.\r\nBất kể bạn di chuyển tới đâu, đôi giày này sẽ đảm bảo cảm giác thoải mái trên suốt hành trình. Sử dụng đế giữa adidas 4D in 3D, đôi giày này có khả năng hấp thụ lực tác động tối ưu và mang lại độ ổn định trên các bề mặt cứng hoặc không bằng phẳng. Thân giày adidas PRIMEKNIT ôm chân vừa vặn và nâng đỡ, thích ứng tự nhiên theo từng sải bước. Trên hết, đôi giày này còn sở hữu cá tính và phong cách nâng tầm mọi outfit.\r\n\r\nLàm từ một loạt chất liệu tái chế, thân giày có chứa tối thiểu 50% thành phần tái chế. Sản phẩm này đại diện cho một trong số rất nhiều các giải pháp của chúng tôi hướng tới chấm dứt rác thải nhựa.','Core Black / Cloud White / Cloud White',1,1,'2023-07-14 01:00:57'),(67,'GIÀY ULTRA 4D',1500000,1000000,'ĐÔI GIÀY CHẠY BỘ TINH CHỈNH, CÓ SỬ DỤNG THÀNH PHẦN TÁI CHẾ.\r\nMỗi ngày mới là một cơ hội để cải thiện: những kỳ vọng, cách nhìn, và buổi chạy của bạn. Giày Ultra 4D được mã hóa nhằm giúp bạn chinh phục mục tiêu, bắt đầu với đế giữa 4D sử dụng công nghệ in 3D cho sải bước ổn định. Lớp đệm bền bỉ giúp bạn chinh phục cự ly, cùng thân giày adidas PRIMEKNIT ôm chân và nâng đỡ từng sải bước.\r\n\r\nLàm từ một loạt chất liệu tái chế, thân giày có chứa tối thiểu 50% thành phần tái chế. Sản phẩm này đại diện cho một trong số rất nhiều các giải pháp của chúng tôi hướng tới chấm dứt rác thải nhựa.','Cloud White / Night Metallic / Beam Yellow',1,1,'2023-07-14 01:06:53'),(68,'GIÀY RUNFALCON 3',2000000,1200000,'abc','abc',0,3,'2023-07-18 08:49:50'),(69,'GIÀY RUNFALCON 3',2000000,1200000,'abc','abc',1,3,'2023-07-18 10:04:38'),(70,'GIÀY ĐÁ BÓNG FIRM GROUND COPA PURE.3333',234324,2,'dfdsfs','2343243',0,2,'2023-07-25 15:15:36');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_many_img`
--

DROP TABLE IF EXISTS `product_many_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_many_img` (
  `products_many_images_id` int NOT NULL AUTO_INCREMENT,
  `products_images_id` int DEFAULT NULL,
  `images` text,
  PRIMARY KEY (`products_many_images_id`),
  KEY `products_images_id` (`products_images_id`),
  CONSTRAINT `product_many_img_ibfk_1` FOREIGN KEY (`products_images_id`) REFERENCES `products_images` (`products_images_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_many_img`
--

LOCK TABLES `product_many_img` WRITE;
/*!40000 ALTER TABLE `product_many_img` DISABLE KEYS */;
INSERT INTO `product_many_img` VALUES (1,1,'adidas-ultra4d-core-black-carbon-5 (1).jpg'),(2,1,'product_3.jpg'),(3,1,'product_4.jpg'),(60,63,'1_1.jpg'),(61,63,'1_2.jpg'),(62,63,'1_3.jpg'),(63,64,'2_1.jpg'),(64,64,'2_2.jpg'),(65,64,'2_3.jpg'),(66,65,'4.jpg'),(67,65,'3_3.jpg'),(68,65,'3_4.jpg'),(69,66,'4.jpg'),(70,66,'4_2.jpg'),(71,66,'4_4.jpg'),(72,66,'4_1.jpg'),(73,66,'4_5.jpg'),(74,67,'5_1.jpg'),(75,68,'5_1.jpg'),(76,68,'5_2.jpg'),(77,68,'5_4.jpg'),(78,68,'5_3.jpg'),(79,39,'anh_giay_bong_da.jpg'),(80,69,'');
/*!40000 ALTER TABLE `product_many_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_images`
--

DROP TABLE IF EXISTS `products_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products_images` (
  `products_images_id` int NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `images` text,
  PRIMARY KEY (`products_images_id`),
  KEY `FK__Products___produ__46E78A0C` (`product_id`),
  CONSTRAINT `FK__Products___produ__46E78A0C` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_images`
--

LOCK TABLES `products_images` WRITE;
/*!40000 ALTER TABLE `products_images` DISABLE KEYS */;
INSERT INTO `products_images` VALUES (1,1,'6.jpg'),(2,2,'images.jpg'),(3,3,'product_2.jpg'),(4,4,'product_3.jpg'),(5,5,'product_5.jpg'),(6,6,'product_6.jpg'),(7,7,'product_7.jpg'),(8,8,'product_8.jpg'),(9,9,'product_9.jpg'),(10,10,'product_10.jpg'),(11,11,'product_11.jpg'),(12,12,'product_12.jpg'),(13,13,'product_13.jpg'),(14,14,'product_14.jpg'),(15,15,'product_15.jpg'),(16,16,'product_16.jpg'),(17,17,'product_17.jpg'),(18,18,'product_18.jpg'),(19,19,'product_19.jpg'),(20,20,'product_20.jpg'),(21,21,'product_21.jpg'),(22,22,'product_22.jpg'),(23,23,'product_23.jpg'),(24,24,'product_24.jpg'),(25,25,'product_25.jpg'),(26,26,'product_26.jpg'),(27,27,'product_27.jpg'),(28,28,'product_28.jpg'),(29,29,'product_29.jpg'),(30,30,'product_30.jpg'),(31,31,'product_31.jpg'),(32,32,'product_32.jpg'),(33,33,'product_33.jpg'),(34,34,'product_34.jpg'),(35,35,'product_35.jpg'),(36,36,'product_36.jpg'),(37,37,'product_37.jpg'),(38,38,'product_38.jpg'),(39,39,'giay-chay-bo-nam-nike-md-runner-2-midnight-749794-410-mau-xanh-size-41-64a39b18b301d-04072023110752.jpg'),(40,40,'product_40.jpg'),(41,41,'product_41.jpg'),(42,42,'product_42.jpg'),(43,43,'product_43.jpg'),(44,44,'product_44.jpg'),(45,45,'product_45.jpg'),(46,46,'product_46.jpg'),(47,47,'product_47.jpg'),(48,48,'product_48.jpg'),(49,49,'product_49.jpg'),(50,50,'product_50.jpg'),(51,51,'product_51.jpg'),(63,64,'1.jpg'),(64,65,'2.jpg'),(65,66,'3_2.jpg'),(66,67,'4_3.jpg'),(67,68,'5.jpg'),(68,69,'5.jpg'),(69,70,'');
/*!40000 ALTER TABLE `products_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin',1),(2,'Sale',1),(3,'Marketing',1),(4,'Customer',1),(5,'Sale Manager',1);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `size` (
  `size_id` int NOT NULL AUTO_INCREMENT,
  `size` int DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  PRIMARY KEY (`size_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `size_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size`
--

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` VALUES (1,36,1,80,1),(2,36,1,194,2),(3,36,1,286,3),(4,36,1,8,4),(5,36,1,122,5),(6,36,1,124,6),(7,36,1,22,7),(8,36,1,534,8),(9,36,1,234,9),(10,36,1,121,10),(11,36,1,235,11),(12,36,1,234,12),(13,36,1,123,13),(14,36,1,124,14),(15,36,1,452,15),(16,36,1,2345,16),(17,36,1,2345,17),(18,36,1,34,18),(19,36,1,34,19),(20,36,1,34,20),(21,36,1,34,21),(22,36,1,5,22),(23,36,1,346,23),(24,36,1,36,24),(25,36,1,346,25),(26,36,1,3,26),(27,36,1,46,27),(28,36,1,36,28),(29,36,1,346,29),(30,36,1,23,30),(31,36,1,2,31),(32,36,1,6,32),(33,36,1,3463,33),(34,36,1,463,34),(35,36,1,32,35),(36,36,1,4,36),(37,36,1,3,37),(38,36,1,64,38),(39,36,0,0,39),(40,36,1,32,40),(41,36,1,6,41),(42,36,1,63,42),(43,36,1,46,43),(44,36,1,3,44),(45,36,1,65,45),(46,36,1,66,46),(47,36,1,66,47),(48,36,1,66,48),(49,36,1,66,49),(50,36,1,66,50),(52,37,1,92,1),(53,38,1,93,1),(54,39,1,95,1),(55,40,1,95,1),(56,37,1,99,2),(57,38,0,100,2),(58,39,0,100,2),(59,40,0,90,2),(112,36,1,100,64),(113,37,1,111,64),(114,38,1,112,64),(115,39,1,113,64),(116,40,1,114,64),(117,36,1,100,65),(118,37,1,108,65),(119,38,1,200,65),(120,39,1,100,65),(121,40,1,201,65),(122,36,1,12,66),(123,37,1,13,66),(124,38,1,17,66),(125,40,1,100,66),(126,36,1,1,67),(127,37,1,19,67),(128,43,1,1000,67),(129,36,0,0,68),(130,36,1,1,69),(131,37,1,11,69),(132,40,1,12,69),(133,39,1,111,69),(134,37,1,10,39),(135,39,1,11,39),(136,36,1,2,70);
/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slider` (
  `slider_id` int NOT NULL AUTO_INCREMENT,
  `slider_title` varchar(100) DEFAULT NULL,
  `slider_image` text,
  `backlink` text,
  `note` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  PRIMARY KEY (`slider_id`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `slider_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` VALUES (1,'123','anh_bia_4.jpg','http://localhost:9999/OnlineShop_SWP391/product?page=1&cateId=1&pageSize=6','123ewgqg',1,39),(30,'Top 5 loại giày mới nhất','elle-viet-nam-blog-giay-the-thao6.jpeg','http://localhost:9999/OnlineShop_SWP391/categoryblog','Top 5 loại giày mới nhất',1,39),(31,'Top 5 loại giày mới nhất','anh_bia_3.jpg','http://localhost:9999/OnlineShop_SWP391/contact','Giày bóng đá',1,39),(33,'LÀM THẾ NÀO ĐỂ LÀM VỆ SINH GIÀY THUYỀN (BOAT SHOES) ','anh_blog_3.jpg','http://localhost:9999/OnlineShop_SWP391/categorycontrol?cateId=4','LÀM THẾ NÀO ĐỂ LÀM VỆ SINH GIÀY THUYỀN (BOAT SHOES) ',1,39);
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_order`
--

DROP TABLE IF EXISTS `status_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_order` (
  `status_order_id` int NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`status_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_order`
--

LOCK TABLES `status_order` WRITE;
/*!40000 ALTER TABLE `status_order` DISABLE KEYS */;
INSERT INTO `status_order` VALUES (1,'Chờ xác nhận'),(2,'Vận chuyển'),(3,'Đang giao'),(4,'Hoàn Thành'),(5,'Đã hủy '),(6,'Trả hàng/Hoàn tiền');
/*!40000 ALTER TABLE `status_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userId` int NOT NULL AUTO_INCREMENT,
  `fullName` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `avatar` text,
  `gender` tinyint(1) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (35,'Đăng Hồng Nguyễn','Dang2002@',NULL,1,'nuyenhongdang@gmail.com','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam',NULL,1,5),(36,'Đăng Hồng Nguyễn','Dang2002@','Screenshot 2023-07-25 132648.png',1,'dangnhhe161726@fpt.edu.vn','0981750193','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam',NULL,1,4),(37,'Nguyễn Tuấn Dũng','Dung2002@',NULL,1,'dungnthe161793@fpt.edu.vn','0981123124','Ha Noi',NULL,1,2),(38,'Phạm Dương Thanh Quý','Quy2002@','thiet-ke-nha-lo-goc-08.jpg',1,'quypdthe163964@fpt.edu.vn','0987654321','Ninh Bình -  Hà Nội',NULL,1,1),(39,'Đào Thế Anh','Anh2002@','Screenshot 2023-05-01 211349.png',1,'theanh0102nd@gmail.com','0981273645','Nam Dinh - Ha Noi',NULL,1,3),(40,'Phạm Văn Mạnh','Manh2002@',NULL,0,'hasagi608@gmail.com','0918273645','Vĩnh Ngọc-Đông Anh-Hà Nội-Việt Nam',NULL,1,2),(42,'Dũng Nguyễn','Dang2002@',NULL,0,'captainmorric@gmail.com','0857432886','số nhà 6 ngõ 470/17 đường Nguyễn Trãi Quận Thanh Xuân',NULL,1,4),(43,'Dinh Manh','Manh2002@',NULL,1,'ff','0973344977','Cam Ranh, Khánh Hòa',NULL,NULL,NULL),(44,'Dinh Manh','Manh2002@',NULL,1,'ff','0973344977','Cam Ranh, Khánh Hòa',NULL,1,4),(45,'Dinh Manh','Manh2002@',NULL,1,'ff','0973344977','Cam Ranh, Khánh Hòa',NULL,1,4),(46,'Dinh Manh','Manh2002@','Screenshot 2023-07-17 115015.png',1,'gh','0973344977','Cam Ranh, Khánh Hòa',NULL,1,4),(47,'Dinh Manh','Manh2002@',NULL,1,'dinhmanh3802@gmail.com','0973344977','Cam Lâm, Khánh Hòa',NULL,1,4);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-25 22:21:17
