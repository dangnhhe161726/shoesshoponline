<%-- 
    Document   : DashBoad
    Created on : Jul 5, 2023, 9:43:45 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <head>
        <title>Danh sách nhân viên | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>
    <style>
        .row-container {
            display: flex;
            flex-wrap: wrap;
        }

        .row-container > div[class*="col-"] {
            display: flex;
            flex-direction: column;
        }

        .row-container .widget-small {
            flex: 1;
        }
    </style>
    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="/index.html"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../Manager/Dashbroad.jsp"/>  
        <main class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="app-title">
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><b>Bảng điều khiển</b></a></li>
                        </ul>
                        <div id="clock"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Left-->
                <div class="col-md-12 col-lg-6">
                    <div class="row">
                        <!-- col-6 -->
                        <div class ="row-container"> 
                            <div class="col-md-6">
                                <div class="widget-small primary coloured-icon"><i class='icon bx bxs-user-account fa-3x'></i>
                                    <div class="info">
                                        <h6 style="color: red">Tổng khách hàng</h6>
                                        <p><b>${a}</b></p>
                                        <p class="info-tong">Tổng số khách hàng được quản lý.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- col-6 -->
                            <div class="col-md-6">
                                <div class=" widget-small info coloured-icon"><i class='icon bx bxs-shopping-bags fa-3x'></i>

                                    <div class="info">
                                        <h6 style="color: red">Tổng sản phẩm</h6>
                                        <p><b>${b}</b></p>
                                        <p class="info-tong">Tổng số sản phẩm được quản lý.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- col-6 -->

                            <div class="col-md-6">
                                <div class="widget-small warning coloured-icon"><i class='icon bx bxs-data fa-3x'></i>
                                    <div class="info">
                                        <h6 style="color: red"> Tổng lợi nhuận</h6>
                                        <p> <i class="fas fa-money-bill-wave"></i> <b>${c}</b></p>
                                        <p class="info-tong">Tổng số lợi nhuận.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- col-6 -->
                            <div class="col-md-6">
                                <div class="widget-small primary coloured-icon"><i class='icon bx bxs-user-account fa-3x'></i>
                                    <div class="info">
                                        <h6 style="color: red">Tổng Phản hồi</h6>
                                        <p><b>${x}</b></p>
                                        <p class="info-tong">Tổng số Phản hồi.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class=" widget-small info coloured-icon"><i class='icon bx bxs-shopping-bags fa-3x'></i>
                                    <div class="info">
                                        <h6 style="color: red">Tổng đơn hàng</h6>
                                        <p><b>${y}</b></p>
                                        <p class="info-tong">Tổng số đơn hàng.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="widget-small warning coloured-icon"><i class='icon bx bxs-data fa-3x'></i>
                                    <!--                  <i class="icon bx bxs-error-alt fa-3x"></i>-->
                                    <div class="info">
                                        <h6 style="color: red">Tổng thu nhập</h6>
                                        <p> <i class="fas fa-money-bill-wave"></i> <b>${o}</b></p>
                                        <p class="info-tong">Tổng thu nhập 7 ngày gần nhất.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tile">
                                <h3 class="tile-title">Sản phẩm bán chạy nhất</h3>
                                <div>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Tên Sản Phẩm</th>
                                                <th>Tổng_số lượng</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${list}" var="c">   
                                                <tr>
                                                    <td>${c.productid}</td>
                                                    <td>${c.productname}</td> 
                                                    <td>${c.totalQuantity}</td>
                                                </tr>                    
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tile">
                                <form action="marketingdashboard" method="post">
                                    <input type="date" name="d1" class="form-control form-control-sm" value="${d1}">
                                    <input type="date" name="d2" class="form-control form-control-sm"  value="${d2}">
                                    <button type="submit" class="btn btn-success register btn-block"><i class="fa fa-user-md"></i> Lọc</button>
                                </form>
                            </div>
                        </div>
                    </div>   
                    <div class="col-md-12">
                        <div class="tile">
                            <h3 class="tile-title">Thống kê 6 tháng doanh thu</h3>
                            <div class="col-md-7 mb-4" >
                                <canvas id="myChart" width="600" height="700"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--END right-->





            <!--<div class="container">
              <div class="row">
                <div class="col-md-6 offset-md-6">
                  <div class="row justify-content-end">
                    <div class="col-md-6 mb-4">
                      <canvas id="myChart" width="200" height="500"></canvas>
                    </div>
                  </div>
                  <form action="dashboard" method="post">
                    <div class="mb-3">
                      <input type="date" name="d1" class="form-control form-control-sm">
                    </div>
                    <div class="mb-3">
                      <input type="date" name="d2" class="form-control form-control-sm">
                    </div>
                    <div class="mb-3">
                      <button type="submit" class="btn btn-success register btn-block"><i class="fa fa-user-md"></i> Register</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>-->




            <!--END right-->





        </main>
        <script src="js/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="js/popper.min.js"></script>
        <script src="https://unpkg.com/boxicons@latest/dist/boxicons.js"></script>
        <!--===============================================================================================-->
        <script src="js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>
        <!--===============================================================================================-->
        <script src="js/plugins/pace.min.js"></script>
        <!--===============================================================================================-->
        <script type="text/javascript" src="js/plugins/chart.js"></script>
        <!--===============================================================================================-->
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


        <script>
        const ctx = document.getElementById('myChart');
        const dataValues = [
            <c:forEach var="value" items="${dataValues}">
                ${value},
            </c:forEach>
        ];
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Order', 'Feedback', 'User', ],
                datasets: [{
                        label: '# of Votes',
                        data: dataValues,
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
        </script>



        <!--  <script type="text/javascript">
           var data = {
          labels: ["Ngày 1", "Ngày 2", "Ngày 3", "Ngày 4", "Ngày 5", "Ngày 6","Ngày 7"],
          datasets: [{
            label: "Dữ liệu đầu tiên",
            fillColor: "rgba(255, 213, 59, 0.767), 212, 59)",
            strokeColor: "rgb(255, 212, 59)",
            pointColor: "rgb(255, 212, 59)",
            pointStrokeColor: "rgb(255, 212, 59)",
            pointHighlightFill: "rgb(255, 212, 59)",
            pointHighlightStroke: "rgb(255, 212, 59)",
            data: [
          
            ]
          }]
        };
        
        //      {
        //        label: "Dữ liệu kế tiếp",
        //        fillColor: "rgba(9, 109, 239, 0.651)  ",
        //        pointColor: "rgb(9, 109, 239)",
        //        strokeColor: "rgb(9, 109, 239)",
        //        pointStrokeColor: "rgb(9, 109, 239)",
        //        pointHighlightFill: "rgb(9, 109, 239)",
        //        pointHighlightStroke: "rgb(9, 109, 239)",
        //        data: [48, 48, 49, 39, 86, 10]
        //      }
              ]
            };
            var ctxl = $("#lineChartDemo").get(0).getContext("2d");
            var lineChart = new Chart(ctxl).Line(data);
        
            var ctxb = $("#barChartDemo").get(0).getContext("2d");
            var barChart = new Chart(ctxb).Bar(data);
          </script>-->
        <script type="text/javascript">
            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }
        </script>
    </body>

</html>
