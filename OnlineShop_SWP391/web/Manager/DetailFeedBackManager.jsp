<%-- 
    Document   : AddCustomer
    Created on : Jun 21, 2023, 3:52:19 PM
    Author     : ADMIN
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Thêm nhân viên | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="http://code.jquery.com/jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

        <style>
            .custom-select {
                font-size: 16px; /* Adjust the font size as needed */
                height: 50px; /* Adjust the height as needed */
                width: 250px; /* Adjust the width as needed */
            }

            .toggle-switch input[type="radio"] {
                display: none;
            }

            .toggle-switch label {
                display: inline-block;
                width: 50%;
                padding: 10px 0;
                text-align: center;
                cursor: not-allowed;
                background-color: lightgray;
            }

            .toggle-switch input[type="radio"]:checked + label {
                background-color: dodgerblue;
                color: white;
            }

            .Choicefile {
                display: block;
                background: #14142B;
                border: 1px solid #fff;
                color: #fff;
                width: 150px;
                text-align: center;
                text-decoration: none;
                cursor: pointer;
                padding: 5px 0px;
                border-radius: 5px;
                font-weight: 500;
                align-items: center;
                justify-content: center;
            }

            .Choicefile:hover {
                text-decoration: none;
                color: white;
            }

            #uploadfile,
            .removeimg {
                display: none;
            }

            #thumbbox {
                position: relative;
                width: 100%;
                margin-bottom: 20px;
            }

            .removeimg {
                height: 25px;
                position: absolute;
                background-repeat: no-repeat;
                top: 5px;
                left: 5px;
                background-size: 25px;
                width: 25px;
                /* border: 3px solid red; */
                border-radius: 50%;

            }

            .removeimg::before {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                content: '';
                border: 1px solid red;
                background: red;
                text-align: center;
                display: block;
                margin-top: 11px;
                transform: rotate(45deg);
            }

            .removeimg::after {
                /* color: #FFF; */
                /* background-color: #DC403B; */
                content: '';
                background: red;
                border: 1px solid red;
                text-align: center;
                display: block;
                transform: rotate(-45deg);
                margin-top: -2px;
            }
        </style>
    </head>

    <body class="app sidebar-mini rtl">

        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb">
                    <li class="breadcrumb-item">Chỉnh sửa người dùng</li>
                    <li class="breadcrumb-item"><a href="listfeedback">Quay lại danh sách</a></li>
                </ul>
            </div>
            <jsp:include page="../Manager/Dashbroad.jsp"/> 
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <h3 class="tile-title">Tạo mới bảng kê lương</h3>
                        <div class="tile-body">
                            <div class="row">
                                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                    <img height="300" width="300" alt="Thumb image" id="thumbimage" src="LoadImg/${profile.avatar}">
                                    <label class="control-label">Ảnh Đại Diện</label>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Họ và Tên</label>
                                    <input value="${profile.fullName} " name="name" class="form-control" type="text" readonly >
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Email</label>
                                    <input name="email"  value="${profile.email}"  class="form-control" type="email" readonly >
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Sản Phẩm</label>
                                    <input name="address" value="${profile.getProduct().getProductname()}" class="form-control" type="text" readonly>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Số Điện Thoại</label>
                                    <input value="${profile.mobile}" name="phone" class="form-control" type="text"  readonly>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="exampleSelect1 control-label">Giới Tính</label>
                                    <div style="display: flex;" class="toggle-switch">
                                        <input type="radio" name="gender" value="1" id="male" ${profile.gender ? "checked" : ""} disabled>
                                        <label for="male">Name</label>
                                        <input type="radio" name="gender" value="0" id="female" ${!profile.gender ? "checked" : ""} disabled>
                                        <label for="female">Nữ</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Nội Dung</label>
                                    <input value="${profile.getFeedback().getFeedback()}" class="form-control" type="text"readonly >
                                </div>
                                <div class="form-group col-md-3">     
                                    <label class="exampleSelect1 control-label"> Sao:</label>
                                    <div class="star-rating">
                                        <c:set var="rating" value="${profile.feedback.ratedStar}" />
                                        <div class="stars">
                                            <c:forEach begin="1" end="5" varStatus="loop">
                                                <c:choose>
                                                    <c:when test="${loop.index <= rating}">
                                                        <span class="star">&#9733;</span> <!-- Ký tự ngôi sao Unicode -->
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span class="star">&#9734;</span> <!-- Ký tự ngôi sao rỗng Unicode -->
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>                                             
                                <form method="post" action="detailfeedbackmanager" > 
                                    <input type="text" hidden="" value="${idf}" name="idf">
                                    <input type="hidden" class="form-control-sm" name="id" value="${profile.userId}">
                                    <div class="form-group col-md-4">
                                        <label for="exampleSelect1" class="control-label">Trạng thái</label>
                                        <select name="status" class="form-control">
                                            <option value="1" ${profile.feedback.status == 'true' ? 'selected' : ''}>Active</option>
                                            <option value="0" ${profile.feedback.status == 'false' ? 'selected' : ''}>Inactive</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-sm" style="width: 150px; height: 50px; margin-top: 100px;">
                                        <i class="fa fa-user-md"></i> Lưu Lại
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </main>

    <!--MODAL-->

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <span class="thong-tin-thanh-toan">
                                <h5>Tạo chức vụ mới</h5>
                            </span>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="control-label">Nhập tên chức vụ mới</label>
                            <input class="form-control" type="text" required>
                        </div>
                    </div>
                    <BR>
                    <button class="btn btn-save" type="button">Lưu lại</button>
                    <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                    <BR>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <!--MODAL-->

    <!-- Essential javascripts for application to work-->
    <script>

        function readURL(input, thumbimage) {
            if (input.files && input.files[0]) { //Sử dụng  cho Firefox - chrome
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#thumbimage").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else { // Sử dụng cho IE
                $("#thumbimage").attr('src', input.value);

            }
            $("#thumbimage").show();
            $('.filename').text($("#uploadfile").val());
            $('.Choicefile').css('background', '#14142B');
            $('.Choicefile').css('cursor', 'default');
            $(".removeimg").show();
            $(".Choicefile").unbind('click');

        }
        $(document).ready(function () {
            $(".Choicefile").bind('click', function () {
                $("#uploadfile").click();

            });
            $(".removeimg").click(function () {
                $("#thumbimage").attr('src', '').hide();
                $("#myfileupload").html('<input type="file" id="uploadfile"  onchange="readURL(this);" />');
                $(".removeimg").hide();
                $(".Choicefile").bind('click', function () {
                    $("#uploadfile").click();
                });
                $('.Choicefile').css('background', '#14142B');
                $('.Choicefile').css('cursor', 'pointer');
                $(".filename").text("");
            });
        })
    </script>
    <style>
        .star-rating {
            display: flex;
            align-items: center;
        }

        .stars {
            display: inline-flex;
        }

        .star {
            color: #FFD700; /* Màu sắc của ngôi sao */
            font-size: 20px; /* Kích thước của ngôi sao */
        }
    </style>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>

</body>

</html>