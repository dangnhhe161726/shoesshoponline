<%-- 
    Document   : ManagerOrder
    Created on : Jul 4, 2023, 3:32:03 PM
    Author     : pc
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>
    <body onload="time()" class="app sidebar-mini rtl">
        <jsp:include page="../Manager/Dashbroad.jsp"/> 

        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Danh sách đơn hàng</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            <c:if test="${sessionScope.role_id == 5}">
                                <div class="row element-button">
                                    <div class="col-sm-2">
                                        <div style="display: flex;">
                                            <p style="width: 120px; height: 10px; font-size: 15px; margin-right: 10px; padding: 0 10px;">Lọc sản phẩm:</p>
                                            <form id="myForm" action="ordermanager" method="post">
                                                <select name="mainSelect" style="margin-right: 10px; height: 30px; width: 150px; border-radius: 5px;" onchange="submitForm()">
                                                    <option ${sessionScope.option == null?"selected":""} value="">Lựa chọn</option>
                                                    <option ${sessionScope.option == "0"?"selected":""} value="0">Tên Người Bán</option>
                                                    <option ${sessionScope.option == "1"?"selected":""} value="1">Tình Trạng</option>
                                                </select>

                                            </form>
                                            <form id="myForm1" action="filteroder" method="post">
                                                <c:if test="${sessionScope.option == 0}">
                                                    <select style="height: 30px; border-radius: 5px;" name="filterOrder" onchange="submitForm1()">
                                                        <option ${optionFilter == 0?"selected":""} value="0">Tất cả</option>

                                                        <c:forEach var="i" items="${listSale}">
                                                            <option ${optionFilter == i.getUserId()?"selected":""} value="${i.getUserId()}">${i.getFullName()}</option>
                                                        </c:forEach>


                                                    </select>
                                                </c:if>

                                                <c:if test="${sessionScope.option == 1}">
                                                    <select style="height: 30px; border-radius: 5px;" name="filterOrder" onchange="submitForm1()">
                                                        <option ${optionFilter == 0?"selected":""} value="0">Tất cả</option>

                                                        <c:forEach var="i" items="${listStatus}">
                                                            <option ${optionFilter == i.getStatusOrderId()?"selected":""} value="${-i.getStatusOrderId()}">${i.getStatusName()}</option>
                                                        </c:forEach>
                                                    </select>
                                                </c:if>

                                            </form>


                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>

                                        <th>ID đơn hàng</th>
                                        <th>Ngày Đặt</th>
                                        <th>Khách hàng</th>
                                        <th>Đơn hàng</th>
                                        <th>Tổng tiền</th>
                                        <th>Tình trạng</th>
                                        <th>Tính năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${sessionScope.role_id == 2}">
                                        <c:forEach items="${listObyId}" var="o">   
                                            <tr>

                                                <td>${o.getOrderid()}</td>
                                                <td>${o.getOrderdate()}</td>
                                                <td>${o.getFullname()}</td>
                                                <td>${o.getStatusOrder().getProduct().getProductname()}</td>
                                                <td><fmt:formatNumber pattern="##.#" value="${o.getTotalCost()}"/> d</td>
                                                <td><span class="badge bg-success">${o.getStatusOrder().getStatusName()}</span></td>
                                                <td>

                                                    <button class="btn btn-primary btn-sm edit" type="button" title="Sửa"><a href="editorder?OId=${o.getOrderid()}"><i class="fa fa-edit"></i></a>
                                                    </button>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${sessionScope.role_id == 5}">
                                        <c:forEach items="${listO}" var="o">   
                                            <tr>

                                                <td>${o.getOrderid()}</td>
                                                <td>${o.getOrderdate()}</td>
                                                <td>${o.getFullname()}</td>
                                                <td>${o.getStatusOrder().getProduct().getProductname()}</td>
                                                <td><fmt:formatNumber pattern="##.#" value="${o.getTotalCost()}"/> d</td>
                                                <td><span class="badge bg-success">${o.getStatusOrder().getStatusName()}</span></td>
                                                <td>

                                                    <button class="btn btn-primary btn-sm edit" type="button" title="Sửa"><a href="editorder?OId=${o.getOrderid()}"><i class="fa fa-edit"></i></a>
                                                    </button>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>


            function deleteRow(r) {
                var i = r.parentNode.parentNode.rowIndex;
                document.getElementById("myTable").deleteRow(i);
            }
            jQuery(function () {
                jQuery(".trash").click(function () {
                    swal({
                        title: "Cảnh báo",

                        text: "Bạn có chắc chắn là muốn xóa đơn hàng này?",
                        buttons: ["Hủy bỏ", "Đồng ý"],
                    })
                            .then((willDelete) => {
                                if (willDelete) {
                                    swal("Đã xóa thành công.!", {

                                    });
                                }
                            });
                });
            });
            oTable = $('#sampleTable').dataTable();
            $('#all').click(function (e) {
                $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
                e.stopImmediatePropagation();
            });

            //EXCEL
            // $(document).ready(function () {
            //   $('#').DataTable({

            //     dom: 'Bfrtip',
            //     "buttons": [
            //       'excel'
            //     ]
            //   });
            // });


            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }
            //In dữ liệu
            var myApp = new function () {
                this.printTable = function () {
                    var tab = document.getElementById('sampleTable');
                    var win = window.open('', '', 'height=700,width=700');
                    win.document.write(tab.outerHTML);
                    win.document.close();
                    win.print();
                }
            }
            //     //Sao chép dữ liệu
            //     var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

            // copyTextareaBtn.addEventListener('click', function(event) {
            //   var copyTextarea = document.querySelector('.js-copytextarea');
            //   copyTextarea.focus();
            //   copyTextarea.select();

            //   try {
            //     var successful = document.execCommand('copy');
            //     var msg = successful ? 'successful' : 'unsuccessful';
            //     console.log('Copying text command was ' + msg);
            //   } catch (err) {
            //     console.log('Oops, unable to copy');
            //   }
            // });


            //Modal
            $("#show-emp").on("click", function () {
                $("#ModalUP").modal({backdrop: false, keyboard: false})
            });
            //Thẻ submit thẻ Form
            function submitForm() {
                document.getElementById("myForm").submit();
            }

            function submitForm1() {
                document.getElementById("myForm1").submit();
            }
        </script>
    </body>

</html>
