<%-- 
    Document   : EditPost
    Created on : Jul 4, 2023, 4:05:22 PM
    Author     : quyde
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách đơn hàng | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link href=".//css/main.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <jsp:include page="../Manager/Dashbroad.jsp"/>       
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb">
                    <li class="breadcrumb-item">Chỉnh sửa slider</li>
                    <li class="breadcrumb-item"><a href="postmanager">PostList</a></li>

                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <h3 class="tile-title">Tạo mới bảng kê lương</h3>

                        <div class="tile-body">
                            <form name="pull" action="editpost" method="post" enctype="multipart/form-data" class="row">
                                <div class="row">
                                    <div style="text-align: center;" class="form-group col-md-12">
                                        <label class="control-label">Thumbnail</label>
                                        <div class="image-container">
                                            <img style="width: 200px; height: 250px; margin-bottom: 20px;" class="preview-image" id="imagePreview" src="img/${detail.thumbnail}" >
                                        </div>  
                                        <input name="thumbnail"  class="form-control" type="file" accept="image/*" onchange="loadFile(event)" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Người update</label>
                                        <input value="${detail.u.fullName}"  class="form-control" type="text" readonly >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Blog Id</label>
                                        <input name="id" value="${detail.blogId}" class="form-control" type="text" readonly >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="exampleSelect1" class="control-label">Blog Category</label>
                                        <select name="categoryblog" class="form-control" id="exampleSelect1">
                                            <c:forEach items="${listCB}" var="o">
                                                <c:choose>
                                                    <c:when test="${o.categoryBlogId eq detail.cb.categoryBlogId}">
                                                        <option value="${o.categoryBlogId}" selected>${o.categoryBlogName}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${o.categoryBlogId}">${o.categoryBlogName}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Updated date</label>
                                        <input name="id" value="${detail.updateDate}" class="form-control" type="text" readonly >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Title</label>
                                        <input name="title" value="${detail.title}" class="form-control" type="text" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Brief Infor</label>
                                        <input name="briefinfor" value="${detail.briefInfor}" class="form-control" type="text" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Content</label>
                                        <input name="content" value="${detail.content}" class="form-control" type="text" >
                                    </div>    
                                    <div class="form-group col-md-4">
                                        <label for="exampleSelect1" class="control-label">Trạng thái</label>
                                        <select name="status" class="form-control">
                                            <option value="1" ${detail.status == 'true' ? 'selected' : ''}>Active</option>
                                            <option value="0" ${detail.status == 'false' ? 'selected' : ''}>Inactive</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button  class="btn btn-save" type="submit">Lưu lại</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <!--MODAL-->

        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <span class="thong-tin-thanh-toan">
                                    <h5>Tạo trạng thái mới</h5>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Nhập tên trạng thái mới</label>
                                <input class="form-control" type="text" required>
                            </div>
                        </div>
                        <BR>
                        <button class="btn btn-save" type="button">Lưu lại</button>
                        <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                        <BR>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <!--
        MODAL
        -->


        <!-- Essential javascripts for application to work-->

        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
    </body>

</html>