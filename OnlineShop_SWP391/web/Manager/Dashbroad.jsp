<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : Dashbroad
    Created on : Jun 27, 2023, 8:22:09 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- Navbar-->

        <header class="app-header">     
            <ul class="app-nav">
                <li>
                    <a class="app-sidebar__user" href="home"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
                        <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146ZM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5Z"/>
                        </svg></a>
                </li>
                <li><a class="app-nav__item" href="logout"><i class='bx bx-log-out bx-rotate-180'></i></a>
                </li>
            </ul>
        </header>

        <!-- Sidebar menu-->

        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <aside class="app-sidebar">
            <div class="app-sidebar__user">
                <div>
                    <p class="app-sidebar__user-name"><b>${sessionScope.fullName}</b></p>
                    <p class="app-sidebar__user-designation">Chào mừng bạn trở lại</p>
                </div>
            </div>
        </div>
        <hr>
        <ul class="app-menu">
            <!--Admin dashboard-->
            <c:if test="${sessionScope.role_id == 1}">
                <li><a class="app-menu__item " href="admindashboard"><i class='app-menu__icon bx bx-tachometer'></i><span
                            class="app-menu__label">Admin Dashboard</span></a></li>
                        </c:if>
            <!--Sale dashboard-->
            <c:if test="${sessionScope.role_id == 2 || sessionScope.role_id == 5}">
                <li><a class="app-menu__item " href="orderdashboard"><i class='app-menu__icon bx bx-tachometer'></i><span
                            class="app-menu__label">Sale Dashboard</span></a></li>
                        </c:if>
            <!--Marketing dashboard-->
            <c:if test="${sessionScope.role_id == 3}">
                <li><a class="app-menu__item " href="marketingdashboard"><i class='app-menu__icon bx bx-tachometer'></i><span
                            class="app-menu__label">Marketing Dashboard</span></a></li>
                        </c:if>      
                        <c:if test="${sessionScope.role_id == 3}">
                <li><a class="app-menu__item" href="listcustomer"><i class='app-menu__icon bx bx-user-voice'></i>
                        <span class="app-menu__label">Quản lý khách hàng</span></a></li>
                    </c:if>
                    <c:if test="${sessionScope.role_id == 1}">
                <li><a class="app-menu__item" href="userlistforadmin"><i class='app-menu__icon bx bx-id-card'></i>
                        <span class="app-menu__label">Quản lý người dùng</span></a></li>
                    </c:if>
                    <c:if test="${sessionScope.role_id == 3}">
                <li><a class="app-menu__item" href="slidermanager"><i class='app-menu__icon bx bx-calendar-check'></i> 
                        <span class="app-menu__label">Quản lý slider</span></a></li>
                    </c:if>
                    <c:if test="${sessionScope.role_id == 3}">
                <li><a class="app-menu__item" href="productlistmanager"><i class='app-menu__icon bx bx-purchase-tag-alt'></i>
                        <span class="app-menu__label">Quản lý sản phẩm</span></a></li>
                    </c:if>
                    <c:if test="${sessionScope.role_id == 2 || sessionScope.role_id == 5}">
                <li><a class="app-menu__item" href="ordermanager"><i class='app-menu__icon bx bx-cart-alt'></i>
                        <span class="app-menu__label">Quản lý đơn hàng</span></a></li>
                    </c:if>

            <c:if test="${sessionScope.role_id == 3}">
                <li><a class="app-menu__item" href="postmanager"><i class='app-menu__icon bx bx-task'></i>
                        <span class="app-menu__label">Quản lý bài đăng</span></a></li>
                    </c:if>
                    <c:if test="${sessionScope.role_id == 3}">
                <li><a class="app-menu__item" href="listfeedbackmanager"><i class='app-menu__icon bx bx-run'></i>
                        <span class="app-menu__label">Quản lý phản hồi</span></a></li>
                    </c:if>
                    <c:if test="${sessionScope.role_id == 1}">
                <li><a class="app-menu__item" href="authorizationmanagement"><i class='app-menu__icon bx bx-cog'></i>
                        <span class="app-menu__label">Cài đặt chức năng</span></a></li>
                    </c:if>
        </ul>
    </aside>
</body>
</html>
