<%-- 
    Document   : OrderDetail
    Created on : Jul 6, 2023, 8:47:23 PM
    Author     : pc
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Thông Tin Đơn Hàng | Quản trị Sale</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <jsp:include page="../Manager/Dashbroad.jsp"/> 

        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="ordermanager"><b>Danh Sách Đơn Hàng \ </b></a><b>Thông Tin Đơn Hàng</b></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">

                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th colspan="2">Sản Phẩm</th>
                                        <th>Kích thước</th>
                                        <th>Số lượng</th>
                                        <th>Ngày Đặt</th>
                                        <th>Giá Tiền</th>
                                        <th>Tình Trạng</th>
                                        <th>Người Bán</th>
                                        <th>Tổng</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    <c:forEach var="o" items="${listO}">   
                                        <tr>
                                            <td><img src="img/${o.getOrderDetail().getStatusOrder().getPi().getImages()}" width="50" height="50" alt="alt"/>
                                            </td>
                                            <td>${o.getOrderDetail().getStatusOrder().getPi().getProduct().getProductname()}</td>
                                            <td>
                                                ${o.getOrderDetail().getSize()}
                                            </td>
                                            <td>${o.getOrderDetail().getQuantity()}</td>
                                            <td>${o.getOrderdate()}</td>
                                            <td> <fmt:formatNumber pattern="##.#" value="${o.getOrderDetail().getStatusOrder().getPi().getProduct().getSalePrices()}"/>đ</td>
                                            <td><span class="badge bg-success">${o.getOrderDetail().getStatusOrder().getStatusName()}</td>
                                            <c:if test ="${o.getOrderDetail().getStatusOrder().getPi().getProduct().getUser().getFullName()== null}">
                                                <td>Chưa Có Người Bán</td>
                                            </c:if>
                                            <c:if test ="${o.getOrderDetail().getStatusOrder().getPi().getProduct().getUser().getFullName()!= null}">
                                                <td>${o.getOrderDetail().getStatusOrder().getPi().getProduct().getUser().getFullName()}</td>
                                            </c:if>
                                            <td> <fmt:formatNumber pattern="##.#" value="${o.getOrderDetail().getStatusOrder().getPi().getProduct().getSalePrices() * o.getOrderDetail().getQuantity()}"/>đ</td>
                                        </tr>
                                    </c:forEach>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="12" class="text-right" style="font-weight: bold;">Tổng tiền Hóa Đơn</th>
                                    </tr>
                                    <tr>
                                        <th colspan="12" class="text-right"><fmt:formatNumber pattern="##.#" value="${listOrder.totalCost}"/>đ</th>
                                    </tr>
                                </tfoot>

                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tile">

                                        <div class="tile-body">
                                            <div class="row">
                                                <div class="form-group  col-md-4">
                                                    <label class="control-label">ID đơn hàng ( Nếu không nhập sẽ tự động phát sinh )</label>
                                                    <input class="form-control" type="text"value="${listOrder.getOrderid()}"readonly>
                                                </div>
                                                <div class="form-group  col-md-4">
                                                    <label class="control-label">Tên khách hàng</label>
                                                    <input class="form-control" type="text"value="${listOrder.getFullname()}"readonly>
                                                </div>
                                                <div class="form-group  col-md-4">
                                                    <label class="control-label">Số điện thoại khách hàng</label>
                                                    <input class="form-control" type="text" value=" ${listOrder.getMobile()}"readonly>
                                                </div>
                                                <div class="form-group  col-md-4">
                                                    <label class="control-label">Địa chỉ khách hàng</label>
                                                    <input class="form-control" type="text" value="${listOrder.getAddress()}"readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="exampleSelect1" class="control-label">Tình trạng</label>

                                                    <form id="myForm" action="editorder" method="post">
                                                        <input type="hidden" name="Oid" value="${requestScope.Oid}"/>
                                                        <select class="form-control" id="exampleSelect1" name="Sid">
                                                            <option>-- Chọn tình trạng --</option>
                                                            <c:forEach var="i" items="${listStatus}">
                                                                <option ${i.getStatusOrderId()== requestScope.Sid ?"selected":"" } value="${i.getStatusOrderId()}">${i.getStatusName()}</option>
                                                            </c:forEach>

                                                        </select>
                                                        <br>
                                                        <input type="hidden" name="Oid" value="${listOrder.getOrderid()}">
                                                        <button type="submit" class="btn btn-save" onclick="submitForm()">Lưu lại</button>
                                                    </form>
                                                </div> 

                                                <div class="form-group col-md-4">
                                                    <c:if test="${sessionScope.role_id == 5}">
                                                        <label for="exampleSelect1" class="control-label">Người Bán</label>

                                                        <form id="myForm1" action="editorder2" method="post">
                                                            <input type="hidden" name="Oid" value="${requestScope.Oid}"/>
                                                            <select class="form-control" id="exampleSelect1" name="Slid">
                                                                <option>-- Chọn Người Bán --</option>
                                                                <c:forEach var="i" items="${listSale}">
                                                                    <option ${i.getUserId() == requestScope.Slid?"selected":"" } value="${i.getUserId()}">${i.getFullName()}</option>
                                                                </c:forEach>

                                                            </select>
                                                            <br>
                                                            <input type="hidden" name="Oid" value="${listOrder.getOrderid()}">
                                                            <button type="submit" class="btn btn-save" onclick="this.form.submit()">Lưu lại</button>
                                                        </form>
                                                    </c:if>

                                                </div> 
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        </main>
                        <!-- Essential javascripts for application to work-->
                        <script src="js/jquery-3.2.1.min.js"></script>
                        <script src="js/popper.min.js"></script>
                        <script src="js/bootstrap.min.js"></script>
                        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                        <script src="src/jquery.table2excel.js"></script>
                        <script src="js/main.js"></script>
                        <!-- The javascript plugin to display page loading on top-->
                        <script src="js/plugins/pace.min.js"></script>
                        <!-- Page specific javascripts-->
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
                        <!-- Data table plugin-->
                        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
                        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
                        <script type="text/javascript">$('#sampleTable').DataTable();</script>
                        <script>
                            function deleteRow(r) {
                                var i = r.parentNode.parentNode.rowIndex;
                                document.getElementById("myTable").deleteRow(i);
                            }
                            jQuery(function () {
                                jQuery(".trash").click(function () {
                                    swal({
                                        title: "Cảnh báo",

                                        text: "Bạn có chắc chắn là muốn xóa đơn hàng này?",
                                        buttons: ["Hủy bỏ", "Đồng ý"],
                                    })
                                            .then((willDelete) => {
                                                if (willDelete) {
                                                    swal("Đã xóa thành công.!", {

                                                    });
                                                }
                                            });
                                });
                            });
                            oTable = $('#sampleTable').dataTable();
                            $('#all').click(function (e) {
                                $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
                                e.stopImmediatePropagation();
                            });

                            //EXCEL
                            // $(document).ready(function () {
                            //   $('#').DataTable({

                            //     dom: 'Bfrtip',
                            //     "buttons": [
                            //       'excel'
                            //     ]
                            //   });
                            // });


                            //Thời Gian
                            function time() {
                                var today = new Date();
                                var weekday = new Array(7);
                                weekday[0] = "Chủ Nhật";
                                weekday[1] = "Thứ Hai";
                                weekday[2] = "Thứ Ba";
                                weekday[3] = "Thứ Tư";
                                weekday[4] = "Thứ Năm";
                                weekday[5] = "Thứ Sáu";
                                weekday[6] = "Thứ Bảy";
                                var day = weekday[today.getDay()];
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1;
                                var yyyy = today.getFullYear();
                                var h = today.getHours();
                                var m = today.getMinutes();
                                var s = today.getSeconds();
                                m = checkTime(m);
                                s = checkTime(s);
                                nowTime = h + " giờ " + m + " phút " + s + " giây";
                                if (dd < 10) {
                                    dd = '0' + dd
                                }
                                if (mm < 10) {
                                    mm = '0' + mm
                                }
                                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                                        '</span>';
                                document.getElementById("clock").innerHTML = tmp;
                                clocktime = setTimeout("time()", "1000", "Javascript");

                                function checkTime(i) {
                                    if (i < 10) {
                                        i = "0" + i;
                                    }
                                    return i;
                                }
                            }
                            //In dữ liệu
                            var myApp = new function () {
                                this.printTable = function () {
                                    var tab = document.getElementById('sampleTable');
                                    var win = window.open('', '', 'height=700,width=700');
                                    win.document.write(tab.outerHTML);
                                    win.document.close();
                                    win.print();
                                }
                            }
                            //     //Sao chép dữ liệu
                            //     var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

                            // copyTextareaBtn.addEventListener('click', function(event) {
                            //   var copyTextarea = document.querySelector('.js-copytextarea');
                            //   copyTextarea.focus();
                            //   copyTextarea.select();

                            //   try {
                            //     var successful = document.execCommand('copy');
                            //     var msg = successful ? 'successful' : 'unsuccessful';
                            //     console.log('Copying text command was ' + msg);
                            //   } catch (err) {
                            //     console.log('Oops, unable to copy');
                            //   }
                            // });


                            //Modal
                            $("#show-emp").on("click", function () {
                                $("#ModalUP").modal({backdrop: false, keyboard: false})
                            });

                            function submitForm() {
                                document.getElementById("myForm").submit();
                            }

                            function submitForm1() {
                                document.getElementById("myForm1").submit();
                            }
                        </script>
                        </body>

                        </html>

