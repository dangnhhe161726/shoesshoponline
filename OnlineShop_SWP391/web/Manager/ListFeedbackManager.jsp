<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Danh sách phản hồi | Quản trị Marketing</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    </head>
    <body onload="time()" class="app sidebar-mini rtl">

        <main class="app-content">
            <jsp:include page="../Manager/Dashbroad.jsp"/>       
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Danh sách phản hồi</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            
                                                 <!--Search Customer-->

                            
                            <!--Lọc Trạng Thái-->

                            <form action="listfeedbackmanager" method="post">
                                <label for="sort" class="filter-label">Lọc:</label>
                                <select name="op">
                                    <option ${option == "2" ? "selected" : ""} value="2">Tất Cả</option>
                                    <option ${option == "1" ? "selected" : ""} value="1">Hoạt Động</option>
                                    <option ${option == "0" ? "selected" : ""} value="0">Không Hoạt Động</option>
                                </select>  

                                <!--    <br>
                                    <label for="sort1">Filter:</label>-->
                                <select name="op1">
                                    <option ${option1 == "6" ? "selected" : ""} value="6">Tất Cả</option>
                                    <option ${option1 == "5" ? "selected" : ""} value="5">5 sao</option>
                                    <option ${option1 == "4" ? "selected" : ""} value="4">4 sao</option>
                                    <option ${option1 == "3" ? "selected" : ""} value="3">3 sao</option>
                                    <option ${option1 == "2" ? "selected" : ""} value="2">2 sao</option>
                                    <option ${option1 == "1" ? "selected" : ""} value="1">1 sao</option>
                                    <option ${option1 == "0" ? "selected" : ""} value="0">0 sao</option>
                                </select>  
                                <button type="submit" class="filter-button">Lọc</button>
                                <table class="table table-hover table-bordered js-copytextarea" cellpadding="0" cellspacing="0" border="0" id="sampleTable">       
                                    <thead>
                                        <tr>
                                            <th>ID </th>
                                            <th width="150">Họ và tên</th>
                                            <th width="20">Email</th>
                                            <th width="150">mobile</th>
                                            <th>Tên Sản Phẩm</th>
                                            <th>Phản Hồi</th>
                                            <th>Trạng Thái</th>
                                            <th>Sao</th>
                                            <th width="100">Tính năng</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${alist}" var="c">   
                                            <tr>
                                                <td>${c.userId}</td>
                                                <td>${c.fullName}</td> 
                                                <td>${c.email}</td>
                                                <td>${c.mobile}</td>
                                                <td>${c.getProduct().getProductname()}</td>

                                                <td>${c.getFeedback().getFeedback()}</td>
                                                <td>
                                                    <span class="badge ${c.feedback.status == 'true' ? 'bg-success' : 'bg-danger'}">
                                                        ${c.feedback.status== 'true' ? 'Active' : 'Inactive'}
                                                    </span>
                                                </td> 


                                                <td>
                                                    <div class="star-rating">
                                                        <c:set var="rating" value="${c.getFeedback().getRatedStar()}" />
                                                        <div class="stars">
                                                            <c:forEach begin="1" end="5" varStatus="loop">
                                                                <c:choose>
                                                                    <c:when test="${loop.index <= rating}">
                                                                        <span class="star">&#9733;</span> <!-- Ký tự ngôi sao Unicode -->
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <span class="star">&#9734;</span> <!-- Ký tự ngôi sao rỗng Unicode -->
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>   
                                                    <button class="btn btn-primary btn-sm edit" type="button" title="Sửa">
                                                        <a href="detailfeedbackmanager?id=${c.userId}&idf=${c.feedbackId}"  class="btn-link"><i class="fa fa-edit"></i></a>
                                                    </button>                    
                                                </td>
                                            </tr>                    
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <!--MODAL-->


        <style>
            .star-rating {
                display: flex;
                align-items: center;
            }

            .stars {
                display: inline-flex;
            }

            .star {
                color: #FFD700; /* Màu sắc của ngôi sao */
                font-size: 20px; /* Kích thước của ngôi sao */
            }
            .filter-button {
                background-color: #4CAF50;
                color: white;
                padding: 5px 10px; /* Adjust the padding values to make the button smaller */
                border: none;
                cursor: pointer;
                font-size: 14px; /* Adjust the font size to make the button text smaller */
            }

            .filter-button:hover {
                background-color: #45a049;
            }
            .filter-label {
                color: #333; /* Text color */
                font-weight: bold; /* Add font weight to make the label stand out */
                font-size: 16px; /* Font size */
                margin-right: 10px; /* Add some right margin for spacing */
            }
        </style>


        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <script src="js/plugins/pace.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>
            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }

            //In dữ liệu
            var myApp = new function () {
                this.printTable = function () {
                    var tab = document.getElementById('sampleTable');
                    var win = window.open('', '', 'height=700,width=700');
                    win.document.write(tab.outerHTML);
                    win.document.close();
                    win.print();
                }
            }

            //Modal
            $("#show-emp").on("click", function () {
                $("#ModalUP").modal({backdrop: false, keyboard: false})
            });
        </script>
    </body>

</html>