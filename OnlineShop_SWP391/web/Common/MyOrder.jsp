<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <style>
        .dash{
            padding:7px 15px;
            background-color: #4fbfa8;
            margin-top: -5px;
            border-radius: 5px ;
        }

        .dash a{
            color: white;
        }
        h5 a:hover{
            text-decoration: none;
            color: white; /* Màu chữ khi di chuột */
            font-weight: bold; /* In đậm khi di chuột */
            background-color: #4fbfa8;
            padding:7px 15px;
            border-radius: 5px ;
            margin-top: -5px;
        }



    </style>
    <body>
        <!-- navbar-->
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang Chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Đơn Mua</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-3">
                            <!--
                            *** CUSTOMER MENU ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Lựa Chọn Của Tôi</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-pills flex-column"><a href="myorder" class="nav-link active"><i class="fa fa-list"></i> Đơn Mua</a><a href="editprofile" class="nav-link"><i class="fa fa-user"></i> Tài Khoản Của Tôi</a><a href="logout" class="nav-link"><i class="fa fa-sign-out"></i> Đăng Xuất</a></ul>
                                </div>

                            </div>

                            <!-- /.col-lg-3-->
                            <!-- *** CUSTOMER MENU END ***-->
                        </div>
                        <div id="customer-orders" class="col-lg-9">
                            <div class="box">

                                <div class="row">
                                    <h1 class="col-md-6">Đơn Hàng Của Tôi</h1>
                                    <div class="col-md-6">
                                        <form  role="search" action="myorder" method="get">
                                            <input type="hidden" value="1" name="check"/>
                                            <div style="display: flex">
                                                <input type="text" class="col-md-10 form-control" name="txt">
                                                <input data-toggle="collapse" class="btn navbar-btn btn-primary d-none d-lg-inline-block" type="submit" value="Search" class="fa fa-search"/>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <br>

                                <p class="text-muted">Nếu bạn có bất kỳ câu hỏi nào, xin vui lòng<a href="contact"> liên hệ</a> với chúng tôi, trung tâm dịch vụ khách hàng của chúng tôi làm việc cho bạn 24/7.</p>
                                <hr>

                                <!-- breadcrumb-->

                                <nav aria-label="breadcrumb">

                                    <ol class="breadcrumb" style="justify-content: space-around ">

                                        <h5 class="${requestScope.Id == null ?"dash":""}" style="font-size:15.5px"><a  href="myorder?Id=" class="breadcrumb-item">Tất cả</a></h5>

                                        <c:forEach items="${slist}" var="s">
                                            <h5 class="${requestScope.Id == s.getStatusOrderId() ?"dash":""}" style="font-size:15.5px;">

                                                <a  href="myorder?Id=${s.getStatusOrderId()}" class="breadcrumb-item" >${s.getStatusName()}</a>

                                            </h5>

                                        </c:forEach>
                                </nav> 
                                <c:if test="${hiddenSearch}">
                                    <img style="margin-left: 100px;" src="Icon/no-search-product.jpg" alt="Không Tìm Thấy Sản Phẩm"/>
                                </c:if>
                                <c:if test="${!hiddenSearch}">
                                    <div class="table-responsive">

                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Tên</th>
                                                    <th>Ngày Đặt</th>
                                                    <th>Giá</th>
                                                    <th>Tình Trạng</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="o" items="${list1}">
                                                    <tr>
                                                        <td>${o.orderid}</td>
                                                        <td>${o.getStatusOrder().getProduct().getProductname()} 
                                                        </td>
                                                        <td>${o.orderdate}</td>
                                                        <td><fmt:formatNumber pattern="##.#" value="${o.getTotalCost()}"/>đ</td>
                                                        <td>${o.getStatusOrder().getStatusName()}</td>
                                                        <td><a href="orderdetail?Id=${o.orderid}" class="btn btn-primary btn-sm">Chi Tiết</a></td>

                                                    </tr>
                                                </c:forEach>
                                            </c:if>
                                        </tbody>
                                    </table>
                                </div>                     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        *** FOOTER ***
        _________________________________________________________
        -->
        <jsp:include page="../Common/Footer.jsp"/>    
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>    
    </body>
</html>