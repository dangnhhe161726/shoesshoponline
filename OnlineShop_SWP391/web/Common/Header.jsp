<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<header class="header mb-5">
    <!--
    * TOPBAR *
    _______________________________________________________
    -->
    <div id="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offer mb-3 mb-lg-0"><a class="btn btn-success btn-sm">Cảm ơn bạn đã lựa chọn chúng tôi</a><a class="ml-1">Chúc bạn có giây phút mua sắm vui vẻ</a></div>

                <c:if test="${sessionScope.id == null}">
                    <div class="col-lg-6 text-center text-lg-right ">
                        <ul class="menu list-inline mb-0 des1">
                            <li class="list-inline-item"><a href="login">Đăng Nhập</a></li>
                            <li class="list-inline-item"><a href="register">Đăng Ký</a></li>
                        </ul>
                    </div>
                </c:if>
            </div>
        </div>
    </div>


    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a href="home" class="navbar-brand home">
                <img style="width: 90x;height: 90px;" src="Icon/logo_3.png" alt="My logo"/>
            </a>
            <div id="navigation" class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a href="home" class="nav-link" >TRANG CHỦ</a></li>
                    <li class="nav-item dropdown menu-large"><a  data-toggle="dropdown" data-hover="dropdown" data-delay="200" class="dropdown-toggle nav-link">Sản phẩm<b class="caret"></b></a>
                        <ul class="dropdown-menu megamenu">
                            <li>
                                <div class="row">
                                    <div class="col-md-6 col-lg-3">
                                        <h5>Loại</h5>
                                        <ul class="list-unstyled mb-3">
                                            <li><a href="product?page=1" class="nav-link">Tất cả sản phẩm</a></li>

                                            <c:forEach items="${sessionScope.list}" var="c">
                                                <li><a href="product?page=1&cateId=${c.categoryId}&pageSize=3" class="nav-link ${c.categoryId==cateId? 'active':''}">${c.categoryName}</a></li>
                                                </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item"><a href="categoryblog" class="nav-link">CHIA SẺ</a></li>
                    <li class="nav-item"><a href="contact" class="nav-link">PHẢN HỒI CHUNG</a></li>
                </ul>
                <div class="navbar-buttons d-flex justify-content-end">
                    <!-- /.nav-collapse-->

                    <div id="basket-overview" class="navbar-collapse collapse d-none d-lg-block ">
                        <c:if test="${sessionScope.sizeCart != null && sessionScope.sizeCart != 0}">
                            <a href="cartdetail" class="btn btn-primary navbar-btn des"><i class="fa fa-shopping-cart"></i>(${sessionScope.sizeCart})</a>
                        </c:if>
                        <c:if test="${sessionScope.sizeCart == null || sessionScope.sizeCart == 0}">
                            <a href="cartdetail" class="btn btn-primary navbar-btn des"><i class="fa fa-shopping-cart"></i></a>
                            </c:if>
                    </div>
                    <c:if test="${sessionScope.id != null}">
                        <div class="dropdown">
                            <button
                                class="dropdown-toggle d-flex align-items-center hidden-arrow"
                                type="button"
                                id="navbarDropdownMenuAvatar"
                                data-mdb-toggle="dropdown"
                                aria-expanded="false"
                                >
                                <img
                                    src="https://www.pngfind.com/pngs/m/34-349693_circled-user-icon-transparent-background-username-icon-hd.png"
                                    class="rounded-circle"
                                    height="30"
                                    alt="Black and White Portrait of a Man"
                                    loading="lazy"
                                    />
                            </button>
                            <ul
                                class="dropdown-menu dropdown-menu-end"
                                aria-labelledby="navbarDropdownMenuAvatar"
                                >
                                <li>
                                    <a class="dropdown-item" href="editprofile">Thông tin cá nhân</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="myorder">Đơn Hàng Của Tôi</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="changepassword">Đổi mật khẩu</a>
                                </li>
                                <c:if test="${role_id != 4}">
                                    <c:choose>
                                        <c:when test="${role_id == 1}">
                                            <li>
                                                <a class="dropdown-item" href="admindashboard">Đến trang quản lý</a>
                                            </li> 
                                        </c:when>
                                        <c:when test="${role_id == 2 || role_id == 5}">
                                            <li>
                                                <a class="dropdown-item" href="orderdashboard">Đến trang quản lý</a>
                                            </li> 
                                        </c:when>
                                        <c:when test="${role_id == 3}">
                                            <li>
                                                <a class="dropdown-item" href="marketingdashboard">Đến trang quản lý</a>
                                            </li> 
                                        </c:when>
                                    </c:choose>
                                </c:if>
                                <li>
                                    <a class="dropdown-item" href="logout">Đăng xuất</a>
                                </li> 
                            </ul>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </nav>

    <script
        type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.js"
    ></script>
</header>