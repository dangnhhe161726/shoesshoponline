<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/js/bootstrap.min.js" integrity="sha512-8Y8eGK92dzouwpROIppwr+0kPauu0qqtnzZZNEF8Pat5tuRNJxJXCkbQfJ0HlUG3y1HB3z18CSKmUo7i2zcPpg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <style>
            .product-image {
                width: 250px; /* Định nghĩa kích thước của phần tử chứa hình ảnh */
                height: 200px;
                overflow: hidden; /* Để hình ảnh không tràn ra ngoài phần tử */
            }

            .product-image img {
                width: 250px; /* Đảm bảo hình ảnh sẽ lấp đầy phần tử chứa */
                height:200px;
                object-fit: cover; /* Phông hình ảnh bằng cách co dãn hoặc thu nhỏ hình ảnh để nó lấp đầy phần tử */
            }

            .product{
                height: 500px;
                width: 250px;
            }

            .text{
                height: 150px;
                text-align: center;
            }
            .price{
                height: 50px;
                text-align: center;
            }
            .pay{
                height: 100px;
                text-align: center;
                margin-top: 20px;
            }
            .buttons a:first-child{
                margin-bottom: 10px;
            }

            .pages{
                margin-top: 20px;
            }
            @keyframes blink {
                0% {
                    opacity: 1;
                }
                50% {
                    opacity: 0;
                }
                100% {
                    opacity: 1;
                }
            }

            .blink {
                animation: blink 2s infinite;
            }
        </style>
    </head>
    <body>
        <%@include file="Header.jsp" %>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Loại giày</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-3">
                            <!--
                            *** MENUS AND FILTERS ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Loại giày</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-pills flex-column category-menu">
                                        <li><a href="product?page=1" class="nav-link ">Tất cả sản phẩm</a></li>

                                        <c:forEach items="${sessionScope.list}" var="c">
                                            <li><a href="product?page=1&cateId=${c.categoryId}&pageSize=6" class="nav-link ${c.categoryId==cateId? 'active':''}">${c.categoryName}</a></li>
                                            </c:forEach>

                                    </ul>
                                </div>
                            </div>
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Bộ lọc tìm kiếm sản phẩm</h3>
                                </div>
                            </div>
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Theo giá</h3>
                                </div>
                                <div class="card-body">
                                    <form method="post" action="product">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="0" onclick="uncheckOther(this)"> Dưới 500.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="1" onclick="uncheckOther(this)"> 500.000 - 1.000.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="2" onclick="uncheckOther(this)"> 1.000.000 - 1.500.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="3" onclick="uncheckOther(this)"> 1.500.000 - 2.000.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="4" onclick="uncheckOther(this)"> Trên 2.000.000
                                                </label>
                                            </div>
                                        </div>
                                        <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>
                                    </form>
                                </div>
                            </div>
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Kích thước</h3>
                                </div>
                                <div class="card-body">
                                    <form method="post" action="product">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="36" onclick="uncheckOther(this)"> Size 36
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="37" onclick="uncheckOther(this)"> Size 37
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="38" onclick="uncheckOther(this)"> Size 38
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="39" onclick="uncheckOther(this)"> Size 39
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="40" onclick="uncheckOther(this)"> Size 40
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="41" onclick="uncheckOther(this)"> Size 41
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="42" onclick="uncheckOther(this)"> Size 42
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="43" onclick="uncheckOther(this)"> Size 43
                                                </label>
                                            </div>
                                        </div>
                                        <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>
                                    </form>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-9">
                            <div class="box">
                                <h1 style="color:#03A9F4; font-weight:bold; font-style:italic;" class="blink">Sản Phẩm!!!</h1>
                                <p style="color: #4fbfa8; font-weight: bold;">Chúng tôi cung cấp nhiều lựa chọn các sản phẩm tốt nhất mà chúng tôi đã tìm thấy và lựa chọn cẩn thận trên toàn thế giới.</p>
                            </div>
                            <div class="box info-bar">

                                <div class="row">

                                    <div class=" products-number-sort">
                                        <div class="row">
                                            <div class=" col-md-12 col-lg-6">
                                                <form role="search" class="ml-auto" action="searchproduct" method="post">
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                        </div>
                                                        <input type="text" placeholder="Search" class="form-control" name="search">

                                                    </div>
                                                </form>
                                            </div>
                                            <c:if test="${!hidden}">
                                                <div style="text-align: right" class=" col-md-12 col-lg-6"><strong>Hiển thị </strong><a href="product?page=1&cateId=${cateId}&pageSize=9" class="btn btn-sm ${pageSize==9?'btn-primary':'btn-outline-secondary'}">9</a><a href="?page=1&cateId=${cateId}&pageSize=15" class="btn ${pageSize==15?'btn-primary':'btn-outline-secondary'} btn-sm">15</a><a href="product?page=1&cateId=${cateId}&pageSize=30" class="btn btn-sm ${pageSize==30?'btn-primary':'btn-outline-secondary'}">30</a><a href="?page=1&cateId=${cateId}&pageSize=1000" class="btn ${pageSize==1000?'btn-primary':'btn-outline-secondary'} btn-sm">Tất cả</a></div>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <c:if test="${hidden}">
                                <img style="width: 100%;" src="Icon/no-search-product.jpg" alt="alt"/>
                            </c:if>
                            <c:if test="${!hidden}">
                                <c:choose>
                                    <c:when test="${plist != null}">
                                        <div  class="products">
                                            <div id="search" class="row">
                                                <c:forEach items="${plist}" var="p">
                                                    <c:if test="${p.status}">
                                                        <div  class="col-lg-4 col-md-6">
                                                            <div class="product">
                                                                <div class="product-image">
                                                                    <a href="productdetail?id=${p.productid}"><img src="img/${p.productImg.images}" class="img-fluid"></a>
                                                                </div>
                                                                <div class="text">
                                                                    <h3><a href="productdetail?id=${p.productid}">${p.productname}</a></h3>
                                                                    <h5 style="text-align: center; color: transparent;
                                                                        text-shadow: 0 0 1px rgba(1,0,0,0.5);">${p.briefInfor}</h5>
                                                                </div>
                                                                <div class="price">
                                                                    <p > 
                                                                        <del></del><div style="text-decoration: line-through; text-align: center"><fmt:formatNumber pattern="##.#" value="${p.originalPrices}"/>đ</div>
                                                                    <del></del> <div style=" text-align: center; color: red;">Sale: <fmt:formatNumber pattern="##.#" value="${p.salePrices}"/>đ</div>
                                                                    </p>
                                                                </div>
                                                                <div class="pay">
                                                                    <p class="buttons">
                                                                        <a href="productdetail?id=${p.productid}&page=1" class="btn btn-outline-secondary">Mua ngay</a>
                                                                    </p>
                                                                </div>
                                                                <!-- /.text-->
                                                            </div>
                                                            <!-- /.product -->
                                                        </div>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                            <!-- /.products-->
                                        </div>
                                        <div class="pages">
                                            <nav aria-label="Page navigation example" class="d-flex justify-content-center">
                                                <ul class="pagination">
                                                    <li class="page-item <c:if test='${currentPage == 1}'>disabled</c:if>">
                                                        <a href="?page=${currentPage - 1}&cateId=${cateId}&pageSize=${pageSize}" aria-label="Previous" class="page-link">
                                                            <span aria-hidden="true">«</span><span class="sr-only">Previous</span>
                                                        </a>
                                                    </li>
                                                    <c:forEach var="i" begin="1" end="${endPage}">
                                                        <li class="page-item">
                                                            <a href="product?page=${i}&cateId=${cateId}&pageSize=${pageSize}" class="page-link ${i == page_raw? 'active':''}">${i}</a>
                                                        </li>
                                                    </c:forEach>
                                                    <li class="page-item <c:if test='${currentPage == endPage}'>disabled</c:if>">
                                                        <a href="?page=${currentPage + 1}&cateId=${cateId}&pageSize=${pageSize}" aria-label="Next" class="page-link">
                                                            <span aria-hidden="true">»</span><span class="sr-only">Next</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </c:when>
                                    <c:when test="${plist == null}">
                                        <img src="Icon/no-search-product.jpg" alt="alt"/>  
                                    </c:when>
                                </c:choose>

                            </c:if>

                        </div>
                        <!-- /.col-lg-9-->
                    </div>
                </div>
            </div>
        </div>
        <!--
        *** FOOTER ***
        _________________________________________________________
        -->
        <jsp:include page="../Common/Footer.jsp"/>  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script>
                                                        document.getElementById('sortBy').value = "?cateId=${cateId}&pFrom=${pFrom}&pTo=${pTo}&search=${search}&pageSize=9&sortBy=${sortBy}";

                                                        function uncheckOther(currentCheckbox) {
                                                            var checkboxes = document.getElementsByName("option");
                                                            checkboxes.forEach(function (checkbox) {
                                                                if (checkbox !== currentCheckbox) {
                                                                    checkbox.checked = false;
                                                                }
                                                            });
                                                        }

                                                        function searchByName(param) {
                                                            var txtSearch = param.values;
                                                            $.ajax({
                                                                url: "/OnlineShop_SWP391/searchAjax",
                                                                method: "get",
                                                                data: {
                                                                    txt: txtSearch
                                                                },
                                                                success: function (data) {
                                                                    var row = document.getElementById("search");
                                                                    row.innerHTML = data;
                                                                },
                                                                error: function (xhr) {

                                                                }
                                                            });
                                                        }
        </script>
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>    
    </body>
</html>