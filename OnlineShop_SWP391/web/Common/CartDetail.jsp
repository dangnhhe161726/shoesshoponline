<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : CartDetail
    Created on : Jun 21, 2023, 9:20:15 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cart Detail</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <style>
            .qua-cart{
                display: flex;
            }

            .qua-cart button{
                border: 1px solid #ececec;
                height: 30px;
                width: 30px;
            }
            .qua-cart a{
                text-decoration: none;
            }
            .delete-item{
                border: 1px solid #ececec;
            }
            .right {
                text-align: right;
            }
            .ed-img{
                height: 300px;
                width: 300px;
            }
            .price-product{
                font-style: italic;
                text-align: center;
                margin-bottom: 10px;
            }
            @keyframes blink {
                0% {
                    opacity: 1;
                }
                50% {
                    opacity: 0;
                }
                100% {
                    opacity: 1;
                }
            }

            .blink {
                animation: blink 1s infinite;
            }
        </style>
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Giỏ hàng</li>
                                </ol>
                            </nav>
                        </div>

                        <div id="basket" class="col-lg-9">
                            <div class="box">
                                <div style="display: flex;">
                                    <div class="left" style="margin-bottom: 30px;"><a href="product?page=1" class="btn btn-outline-secondary"><i class="fa fa-chevron-left"></i>Tiếp Tục Mua Hàng</a></div>
                                    <form role="search" class="ml-auto" action="searchproduct" method="post">
                                        <div class="input-group">
                                            <input type="text" placeholder="Search" class="form-control" name="search">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                            </div>                                                 
                                        </div>
                                    </form>
                                </div>

                                <c:if test="${sessionScope.sizeCart != null && sessionScope.sizeCart != 0}">
                                    <h3 class="text-muted">Bạn đang có ${sessionScope.sizeCart} sản phẩm trong giỏ hàng</3>
                                    </c:if>
                                    <c:if test="${sessionScope.sizeCart == null || sessionScope.sizeCart == 0}">
                                        <h3 class="text-muted">Bạn không có sản phẩm nào trong giỏ hàng</3>
                                        </c:if>
                                        <hr>
                                        <c:set var="c" value="${sessionScope.cart}"/>
                                        <c:if test="${c.items != null && sessionScope.sizeCart != 0}">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th colspan="2">Sản Phẩm</th>
                                                            <th>Kích Thước</th>
                                                            <th>Đơn Giá</th>
                                                            <th>Số Lượng</th>
                                                            <th>Số Tiền</th>
                                                            <th>Thao Tác</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody> 
                                                        <c:forEach var="i" items="${c.items}">
                                                            <c:set var="id" value="${id +1}"/>
                                                            <tr>
                                                                <td>${id}</td>
                                                                <td><a href="productdetail?id=${i.product.productid}"><img src="img/${i.product.productImg.images}" alt="White Blouse Armani"></a></td>
                                                                <td><a href="productdetail?id=${i.product.productid}">${i.product.productname}</a></td>
                                                                <td>${i.size.sizeName}</td>
                                                                <td><fmt:formatNumber pattern="##.#" value="${i.price}"/>đ</td>
                                                                <td>
                                                                    <div class="qua-cart">
                                                                        <button><a style="font-size: 20px;" href="editcartdetail?quantity=-1&id=${i.product.productid}&size=${i.size.sizeName}" target="target" onclick="confirmDelete(event, ${i.quantity})">-</a></button>
                                                                        <input style="width: 30px; height: 30px; text-align: center; border: 1px solid #ececec;" type="text"  readonly value="${i.quantity}">
                                                                        <button><a style="font-size: 20px;" href="editcartdetail?quantity=1&id=${i.product.productid}&size=${i.size.sizeName}" target="target">+</a></button>
                                                                    </div>
                                                                </td>
                                                                <td><fmt:formatNumber pattern="##.#" value="${i.price * i.quantity}"/>đ</td>
                                                                <td>
                                                                    <form name="delete" method="post">
                                                                        <input type="hidden" name="pid" value="${i.product.productid}">
                                                                        <input type="hidden" name="size" value="${i.size.sizeName}">
                                                                        <button class="delete-item" onclick="deleteToCart(this, event)"><i class="fa fa-trash-o"></i></button>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <c:if test="${sessionScope.id != null}">
                                                <form  action="checkoutcartdetail" method="post">
                                                    <div class="box-footer">
                                                        <div>
                                                            <p style="font-size: 15px;">Ghi chú đơn hàng:</p>
                                                            <textarea id="noteCart" name="noteCart" class="form-control" placeholder="Ghi chú đơn hàng tại đây....">${sessionScope.noteCart}</textarea>
                                                        </div>
                                                        <hr>
                                                        <div class="right">
                                                            <button type="submit" class="btn btn-primary">
                                                                Đặt Hàng Ngay
                                                                <i class="fa fa-chevron-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </c:if>
                                            <c:if test="${sessionScope.id == null}">
                                                <form  action="checkoutcartdetail" method="get">
                                                    <div class="box-footer">
                                                        <div>
                                                            <p style="font-size: 15px;">Ghi chú đơn hàng:</p>
                                                            <textarea id="noteCart" name="noteCart" class="form-control">${sessionScope.noteCart}</textarea>
                                                        </div>
                                                        <hr>
                                                        <div class="right">
                                                            <button type="submit" class="btn btn-primary">
                                                                Đăng Nhập Để Mua Hàng
                                                                <i class="fa fa-chevron-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </c:if>
                                        </c:if>
                                        </div>
                                        </div>
                                        <div class="col-lg-3">

                                            <div id="order-summary" class="box">

                                                <div class="box-header">
                                                    <h3 class="mb-0">Thông Tin Đơn Hàng</h3>
                                                </div>
                                                <p class="text-muted">Phí vận chuyển sẽ được tính ở trang thanh toán.</p>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr class="total">
                                                                <td>Tổng Tiền</td>
                                                                <th><fmt:formatNumber pattern="##.#" value="${c.getTotalMoney()}"/>đ</th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <!-- /.box-->
                                            <div class="row same-height-row">

                                                <div class="col-lg-3 col-md-6">
                                                    <div style="background-color: #4fbfa8;" class="box same-height">
                                                        <h3 style="text-align: center; margin-top: 50%; color: white;">Sản Phẩm Mới Nhất</h3>
                                                    </div>
                                                </div>

                                                <!-- /.product-->

                                                <c:forEach var="i" items="${sessionScope.listP}">
                                                    <c:if test="${i.status}">
                                                        <div class="col-md-3 col-sm-6">
                                                            <div class="product same-height">
                                                                <div class="flip-container">
                                                                    <div class="flipper">
                                                                        <div class="front"><a href="productdetail?id=${i.productid}"><img src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a></div>
                                                                        <div class="back"><a href="productdetail?id=${i.productid}"><img src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a></div>
                                                                    </div>
                                                                </div><a href="productdetail?id=${i.productid}" class="invisible"><img src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a>
                                                                <div class="text">
                                                                    <h3><a href="productdetail?id=${i.productid}">${i.productname}</a></h3>
                                                                    <div class="price-product blink">
                                                                        Sale:
                                                                        <span class="price text-danger ">${i.salePrices}VND</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </c:forEach>     
                                            </div>
                                        </div>

                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        <jsp:include page="../Common/Footer.jsp"/>        <!-- *** COPYRIGHT END ***-->
                                        <!-- JavaScript files-->
                                        <script>
                                            function deleteToCart(button, event) {
                                                var confirmation = confirm("Bạn có chắc chắn muốn xoá không?");
                                                if (!confirmation) {
                                                    event.preventDefault(); // Ngăn chặn điều hướng đến đường dẫn URL
                                                } else {
                                                    var form = button.parentNode;
                                                    var pid = form.querySelector('input[name="pid"]').value;
                                                    var size = form.querySelector('input[name="size"]').value;
                                                    form.action = "editcartdetail";
                                                    form.submit();
                                                }
                                            }

                                            function confirmDelete(event, quantity) {
                                                if (quantity === 1) {
                                                    var confirmation = confirm("Bạn có chắc chắn muốn xoá không?");
                                                    if (!confirmation) {
                                                        event.preventDefault(); // Ngăn chặn điều hướng đến đường dẫn URL
                                                    }
                                                }
                                            }
                                        </script>
                                        <script src="vendor/jquery/jquery.min.js"></script>
                                        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                                        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
                                        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
                                        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
                                        <script src="js/front.js"></script> 
                                        </body>
                                        </html>