<%-- 
    Document   : Orderdetail
    Created on : Jun 12, 2023, 9:39:28 AM
    Author     : pc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <body>
        <!-- navbar-->
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="Home">Trang Chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item"><a href="myorder">Đơn Hàng Của Tôi</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Chi Tiết Đơn Hàng</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-3">
                            <!--
                            *** CUSTOMER MENU ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Lựa Chọn Của Tôi</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-pills flex-column"><a href="customer-orders.html" class="nav-link active"><i class="fa fa-list"></i> Đơn Mua</a><a href="customer-wishlist.html" class="nav-link"><i class="fa fa-heart"></i> Danh Sách Yêu Thích</a><a href="editProfile" class="nav-link"><i class="fa fa-user"></i> Tài Khoản Của Tôi</a><a href="logout" class="nav-link"><i class="fa fa-sign-out"></i> Đăng Xuất</a></ul>
                                </div>
                            </div>




                            <!-- /.col-lg-3-->
                            <!-- *** CUSTOMER MENU END ***-->
                        </div>
                        <div id="customer-order" class="col-lg-9">
                            <div class="box">
                                <div class="row">
                                    <div class="centered" style="text-align: center;">
                                        <h1 class="col-md-12">VIẾT ĐÁNH GIÁ</h1>
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-md-12">
                                            <div class="p-3 py-5">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label class="labels">Họ và tên</label>
                                                        <input type="text" class="form-control" name="name"  value="${profile.fullName}" readonly>
                                                    </div>
                                                </div>
                                                <br><!-- comment -->
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label class="labels">Địa chỉ</label>
                                                        <input type="text" name="address" class="form-control" value="${profile.address}" readonly>
                                                    </div>
                                                </div>
                                                <br><!-- comment -->
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label class="labels">Số điện thoại</label>
                                                        <input type="text" class="form-control" name="phone" value="${profile.mobile}" readonly>
                                                    </div>
                                                </div>
                                                <br><!-- comment -->
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label class="exampleSelect1 control-label">Giới Tính</label>
                                                        <div class="toggle-switch" style="display: flex">
                                                            <div style="width: 100px;">
                                                                <input type="radio" name="gender" value="1" id="male" ${profile.gender ? "checked" : ""} disabled>
                                                                <label for="male"><strong>Nam</strong></label>
                                                            </div>
                                                            <div style="width: 100px; margin-right: 100px;">
                                                                <input type="radio" name="gender" value="0" id="female" ${!profile.gender ? "checked" : ""} disabled>
                                                                <label for="female"><strong>Nữ</strong></label>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>  
                                                <form action="feedback" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="oid" value="<%=request.getParameter("oid")%>">
                                                    <div class="mb-4">                                                
                                                        <!--                                                    <div class="mb-4">-->
                                                        <hr>
                                                        <label>Ðánh giá</label>
                                                        <div class="stars">
                                                            <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
                                                            <label class="star star-5" for="star-5"></label>
                                                            <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
                                                            <label class="star star-4" for="star-4"></label>
                                                            <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
                                                            <label class="star star-3" for="star-3"></label>
                                                            <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>
                                                            <label class="star star-2" for="star-2"></label>
                                                            <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>
                                                            <label class="star star-1" for="star-1"></label>
                                                        </div>

                                                        <input type="hidden" id="ratingValue" name="ratingValue" required>
                                                        <div class="form-group">
                                                            <label>Tiêu đề phản hồi</label>
                                                            <textarea name="titleFePro" class="form-control" required></textarea>
                                                            <hr>
                                                            <label>Phản Hồi Của Bạn</label>
                                                            <textarea name="feedbackProduct" class="form-control" required></textarea>
                                                        </div>

                                                        <div style="text-align: center;"  class="form-group col-md-12">
                                                            <label class="control-label">Thêm ảnh phản hồi</label>
                                                                <div class="image-container">
                                                                    <img alt="Chưa có ảnh nào, vui lòng thêm ảnh phản hồi của bạn" style="width: 200px; height: 250px; margin-bottom: 20px;" class="preview-image" id="imagePreview" src="img/${detail.img}" >
                                                                </div>
                                                            <input name="image"  class="form-control" type="file" accept="image/*" onchange="loadFile(event)" required >
                                                        </div>

                                                        <div class="form-group">
                                                            <hr>
                                                            <button class="btn btn-primary btn-sm" type="submit">Gửi Phản Hồi</button>
                                                            <a class="btn btn-danger btn-sm" href="home">Hủy bỏ</a>
                                                        </div>
                                                    </div>
                                                </form>                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--     </div> -->
                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function () {
// Handle the button click event
                    $("#submitButton").click(function () {
// Get the selected image file
                        var imageFile = $("#imageInput")[0].files[0];
                        var reader = new FileReader();

// When the file is loaded, handle the result
                        reader.onload = function () {
// Get the base64 encoded image data
                            var base64Image = reader.result.split(",")[1];

// Make an AJAX POST request to the Servlet
                            $.ajax({
                                type: "POST", // Use "GET" if your Servlet handles GET requests
                                url: "YourServletURL", // Replace with the actual URL of your Servlet
                                data: {imageData: base64Image}, // Data to be sent to the Servlet
                                success: function (response) {
// Handle the response from the Servlet
                                    $("#result").text(response);
                                },
                                error: function () {
                                    alert("Error occurred while making the AJAX request.");
                                }
                            });
                        };

// Read the selected file as a data URL (base64 encoded)
                        if (imageFile) {
                            reader.readAsDataURL(imageFile);
                        }
                    });
                });
            </script>
            <script>
                $(document).ready(function () {
                    // Listen for changes in the file input field
                    $("#imageInput").change(function (event) {
                        var input = event.target;
                        var reader = new FileReader();

                        // When the file is loaded, handle the result
                        reader.onload = function () {
                            // Get the base64 encoded image data
                            var base64Image = reader.result;

                            // Set the base64 encoded image data as the source of the preview image
                            $("#previewImage").attr("src", base64Image);
                        };

                        // Read the selected file as a data URL (base64 encoded)
                        if (input.files && input.files[0]) {
                            reader.readAsDataURL(input.files[0]);
                        }
                    });
                });
            </script>
            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    const stars = document.querySelectorAll('.stars input[type="radio"]');
                    const ratingValueInput = document.getElementById('ratingValue');

                    stars.forEach(function (star) {
                        star.addEventListener('change', function () {
                            ratingValueInput.value = this.value;
                        });
                    });
                });
                var loadFile = function (event) {
                    var output = document.getElementById('imagePreview');
                    output.src = URL.createObjectURL(event.target.files[0]);
                    output.onload = function () {
                        URL.revokeObjectURL(output.src) // free memory
                    }
                };
            </script>


            <style>
                .toggle-switch input[type="radio"] {
                    display: none;
                }

                .toggle-switch label {
                    display: inline-block;
                    width: 50%;
                    padding: 10px 0;
                    text-align: center;
                    cursor: not-allowed;
                    background-color: lightgray;
                }

                .toggle-switch input[type="radio"]:checked + label {
                    background-color: dodgerblue;
                    color: white;
                }

                div.stars {
                    width: 270px;
                    display: inline-block;
                }

                input.star {
                    display: none;
                }

                label.star {
                    float: right;
                    padding: 10px;
                    font-size: 36px;
                    color: #444;
                    transition: all .2s;
                }

                input.star:checked ~ label.star:before {
                    content: '\f005';
                    color: #FD4;
                    transition: all .25s;
                }

                input.star-5:checked ~ label.star:before {
                    color: #FE7;
                    text-shadow: 0 0 20px #952;
                }

                input.star-1:checked ~ label.star:before {
                    color: #F62;
                }

                label.star:hover {
                    transform: rotate(-15deg) scale(1.3);
                }

                label.star:before {
                    content: '\f006';
                    font-family: FontAwesome;
                }

            </style>
            <!--
            *** FOOTER ***
            _________________________________________________________
            -->
            <jsp:include page="../Common/Footer.jsp"/>            
            <!-- JavaScript files-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
            <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
            <script src="js/front.js"></script>   
    </body>
</html> 