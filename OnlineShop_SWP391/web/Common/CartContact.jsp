<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="model.User" %>
<%-- 
    Document   : CartContact
    Created on : Jun 24, 2023, 12:22:26 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cart Contact</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <style>
            .pay-method{
                padding: 10px;
                border: 2px solid #4fbfa8;
                border-radius: 5px;
            }
            .pay-method:hover{
                border: 2.5px solid #03A9F4;
            }

        </style>
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <form action="cartcontact" method="post">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-12">
                                <!-- breadcrumb-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="home">Trang Chủ</a></li>
                                        <li class="breadcrumb-item"><a href="cartdetail">Giỏ Hàng</a></li>
                                        <li aria-current="page" class="breadcrumb-item active">Liên Hệ Giỏ Hàng</li>
                                    </ol>
                                </nav>
                            </div>
                            <div id="basket" class="col-lg-7">
                                <div class="box">
                                    <div style="display: flex;">
                                        <div class="left" style="margin-bottom: 30px;"><a href="product?page=1" class="btn btn-outline-secondary"><i class="fa fa-chevron-left"></i>Tiếp Tục Mua Hàng</a></div>
                                    </div>
                                    <c:set var="c" value="${sessionScope.cart}"/>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th colspan="2">Sản Phẩm</th>
                                                    <th>Kích Thước</th>
                                                    <th>Đơn Giá</th>
                                                    <th>Số Lượng</th>
                                                    <th>Số Tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <c:forEach var="i" items="${c.items}">
                                                    <c:set var="id" value="${id +1}"/>
                                                    <tr>
                                                        <td>${id}</td>
                                                        <td><a href="productdetail?id=${i.product.productid}"><img src="img/${i.product.productImg.images}" alt="White Blouse Armani"></a></td>
                                                        <td><a href="productdetail?id=${i.product.productid}">${i.product.productname}</a></td>
                                                        <td>${i.size.sizeName}</td>
                                                        <td><fmt:formatNumber pattern="##.#" value="${i.price}"/>đ</td>
                                                        <td>
                                                            <div class="qua-cart"> 
                                                                <input style="width: 30px; height: 30px; text-align: center; border: 1px solid white;" type="text"  readonly value="${i.quantity}">
                                                            </div>
                                                        </td>
                                                        <td><fmt:formatNumber pattern="##.#" value="${i.price * i.quantity}"/>đ</td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="5"></th>
                                                    <th colspan="2" style="text-align: right;"><fmt:formatNumber pattern="##.#" value="${c.getTotalMoney()}"/>đ</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="5">Phí Vận Chuyển</th>
                                                    <th colspan="2" style="text-align: right;">50000đ</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="5">Tổng Tiền</th>
                                                    <th colspan="2" style="text-align: right;"><fmt:formatNumber pattern="##.#" value="${c.getTotalMoney() + 50000}"/>đ</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="box-footer">
                                        <div>
                                            <p style="font-size: 15px;">Ghi chú đơn hàng:</p>
                                            <textarea id="noteCart" name="noteCart" class="form-control" placeholder="Ghi chú đơn hàng tại đây....">${sessionScope.noteCart}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <h2>Phương Thức Thanh Toán</h2>
                                    <hr>
                                    <div id="click-pay-1" class="pay-method">
                                        <input type="radio" name="pay" id="pay-1" value="1" required/>
                                        <button style="border: 1px solid white; background-color: white;" id="truck-icon">
                                            <img src="Icon/icon_pay_shipCOD.jpg" width="35" height="25" alt="alt"/> Ship COD (Thanh toán khi nhận hàng)
                                        </button>
                                    </div>
                                    <hr>
                                    <div id="click-pay-2" class="pay-method">
                                        <input type="radio" name="pay" id="pay-2" value="2" required/>
                                        <img src="img/Logo-VNPAY-QR.png" width="65" height="25" alt="logo VNpay" style="margin-left: 5px;margin-right: 10px;"/> Sử dụng thẻ ATM
                                    </div>
                                    <div class="box-footer">
                                        <div class="payment-product">
                                            <button style="margin-left: 400px;" type="submit" class="btn btn-primary">
                                                Đặt Hàng Ngay
                                                <i class="fa fa-chevron-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-lg-7-->
                            <div id="checkout" class="col-lg-5">
                                <div class="box">
                                    <c:set var="u" value="${sessionScope.userAddress}" />
                                    <h2>Thông Tin Vận Chuyển  </h2>
                                    <div class="content py-3">
                                        <!-- /.row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="street">Email</label>
                                                    <input  class="form-control"  type="text" id="email" name="email" value="${sessionScope.userAddress.email}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="lastname">Họ Và Tên</label>
                                                    <input name="fullName" id="fullName" type="text" class="form-control" value="${sessionScope.userAddress.fullName}" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="street">Địa Chỉ Giao Hàng</label>
                                                    <div style="display: flex;">
                                                        <select class="form-select form-select-sm mb-3" id="city" aria-label=".form-select-sm" name="city" required>
                                                            <option value="" selected>Thành Phố</option>           
                                                        </select>
                                                        <select style="margin: 0 10px;" class="form-select form-select-sm mb-3" id="district" aria-label=".form-select-sm" name="district" required>
                                                            <option value="" selected>Quận/Huyện</option>
                                                        </select>
                                                        <select class="form-select form-select-sm mb-3" id="ward" aria-label=".form-select-sm" name="ward" required>
                                                            <option value="" selected>Xã</option>
                                                        </select>
                                                    </div>
                                                    <small>Địa Chỉ Chi Tiết: (Ví dụ: số nhà 69, đường Trần Duy Hưng)</small>
                                                    <input  class="form-control"  type="text" id="address" name="address" value="${sessionScope.userAddress.address}" required>     
                                                </div>
                                            </div> 
                                        </div>
                                        <!-- /.row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="city">Số Điện Thoại</label>
                                                    <input name="phoneNumber" id="phoneNumber" type="text" class="form-control" pattern="[0-9]{10}" required title="Vui lòng nhập số điện thoại gồm 10 chữ số." value="${sessionScope.userAddress.mobile}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.row-->
                                    </div>
                                </div>
                                <!-- /.box-->
                            </div>
                            <!-- /.col-lg-5-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <jsp:include page="../Common/Footer.jsp"/>
        <!-- JavaScript files-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
        <script>
            src = "https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
            ></script>
        <script>
                    var citis = document.getElementById("city");
            var districts = document.getElementById("district");
            var wards = document.getElementById("ward");
            var Parameter = {
                url: "https://raw.githubusercontent.com/kenzouno1/DiaGioiHanhChinhVN/master/data.json",
                method: "GET",
                responseType: "application/json",
            };
            var promise = axios(Parameter);
            promise.then(function (result) {
                renderCity(result.data);
            });

            function renderCity(data) {
                for (const x of data) {
                    var opt = document.createElement('option');
                    opt.value = x.Name;
                    opt.text = x.Name;
                    opt.setAttribute('data-id', x.Id);
                    citis.options.add(opt);
                }
                citis.onchange = function () {
                    district.length = 1;
                    ward.length = 1;
                    if (this.options[this.selectedIndex].dataset.id != "") {
                        const result = data.filter(n => n.Id === this.options[this.selectedIndex].dataset.id);

                        for (const k of result[0].Districts) {
                            var opt = document.createElement('option');
                            opt.value = k.Name;
                            opt.text = k.Name;
                            opt.setAttribute('data-id', k.Id);
                            district.options.add(opt);
                        }
                    }
                };
                district.onchange = function () {
                    ward.length = 1;
                    const dataCity = data.filter((n) => n.Id === citis.options[citis.selectedIndex].dataset.id);
                    if (this.options[this.selectedIndex].dataset.id != "") {
                        const dataWards = dataCity[0].Districts.filter(n => n.Id === this.options[this.selectedIndex].dataset.id)[0].Wards;

                        for (const w of dataWards) {
                            var opt = document.createElement('option');
                            opt.value = w.Name;
                            opt.text = w.Name;
                            opt.setAttribute('data-id', w.Id);
                            wards.options.add(opt);
                        }
                    }
                };
            }

            function handleDivClick() {
                var truckIcon = document.getElementById("truck-icon");
                truckIcon.addEventListener("click", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                });
                const div1 = document.getElementById('click-pay-1');
                const radio1 = document.getElementById('pay-1');
                div1.addEventListener('click', function () {
                    radio1.checked = !radio1.checked;
                });
                const div2 = document.getElementById('click-pay-2');
                const radio2 = document.getElementById('pay-2');
                div2.addEventListener('click', function () {
                    radio2.checked = !radio2.checked;
                });
            }

            handleDivClick();
        </script>  
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script> 
    </body>
</html>