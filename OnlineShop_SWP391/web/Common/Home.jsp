<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <style>

            h4{
                height: 40px;
            }
            .post{
                height: 400px;
                width: auto;
            }
            .post p:nth-last-child(1){

                height: 20%;
            }
            .post p:nth-last-child(2){

                height: 50%;
            }
            .post p:nth-last-child(1){
                font-weight: bold;
                height: 10%;
            }
            .price-product{
                font-style: italic;
                text-align: center;
                margin-bottom: 10px;
            }
            @keyframes blink {
                0% {
                    opacity: 1;
                }
                50% {
                    opacity: 0;
                }
                100% {
                    opacity: 1;
                }
            }

            .blink {
                animation: blink 1s infinite;
            }
        </style>
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="main-slider" class="owl-carousel owl-theme">
                                <c:forEach var="o" items="${lists}">
                                    <c:if test="${o.status}">
                                        <a href="${o.backlink}">
                                            <div class="item">
                                                <img style="max-width: 1110px;max-height: 800px;" src="img/${o.img}" alt="" class="img-fluid">
                                            </div>
                                        </a>
                                    </c:if>
                                </c:forEach>
                            </div>
                            <!-- /#main-slider-->
                        </div>
                    </div>
                </div>
                <!--
                *** ADVANTAGES HOMEPAGE ***
                _________________________________________________________
                -->
                <div id="advantages">
                    <div class="container">
                        <div class="row mb-4">
                            <div class="col-md-4">
                                <div class="box d-flex flex-column justify-content-center mb-0 h-100">
                                    <div class="icon"><i class="fa fa-heart"></i></div>
                                    <h3>We love our customers</h3>
                                    <p class="mb-0">Chúng tôi luôn cung cấp những dịch vụ tốt nhất cho khách hàng</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box d-flex flex-column justify-content-center mb-0 h-100">
                                    <div class="icon"><i class="fa fa-tags"></i></div>
                                    <h3>Best prices</h3>
                                    <p class="mb-0">Sản phẩm luôn có giá tốt nhất</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box d-flex flex-column justify-content-center mb-0 h-100">
                                    <div class="icon"><i class="fa fa-thumbs-up"></i></div>
                                    <h3>100% satisfaction guaranteed</h3>
                                    <p class="mb-0">Miễn phí hoàn đổi trong vòng 3 tháng</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row-->
                    </div>
                    <!-- /.container-->
                </div>
                <!-- /#advantages-->
                <!-- *** ADVANTAGES END ***-->
                <!--
                *** HOT PRODUCT SLIDESHOW ***
                _________________________________________________________
                -->
                <div id="hot">
                    <div class="box py-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 style="font-weight: bold;">Sản phẩm hot tuần này</h2>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="container">
                        <div class="product-slider owl-carousel owl-theme">
                            <c:forEach var="i" items="${sessionScope.listP}">
                                <c:if test="${i.status}">
                                    <div class="item">
                                        <div class="product">
                                            <div class="flip-container">
                                                <div class="flipper">
                                                    <div class="front"><a href="productdetail?id=${i.productid}&page=1"><img style="height: 293.667px" src="img/${i.productImg.images}" alt="" class="img-fluid"></a></div>
                                                    <div class="back"><a href="productdetail?id=${i.productid}"><img style="height: 293.667px" src="img/${i.productImg.images}" alt="" class="img-fluid"></a></div>
                                                </div>
                                            </div><a href="productdetail?id=${i.productid}" class="invisible"><img style="height: 293.667px" src="img/${i.productImg.images}" alt="" class="img-fluid"></a>
                                            <div class="text">
                                                <h3><a href="productdetail?id=${i.productid}">${i.productname}</a></h3>
                                                <div class="price-product blink">
                                                    Sale:
                                                    <span class="price text-danger ">${i.salePrices}đ</span>
                                                </div>
                                            </div> 
                                            <!-- /.ribbon-->
                                        </div>
                                        <!-- /.product-->
                                    </div>
                                </c:if>
                            </c:forEach>

                        </div>
                        <!-- /#hot-->


                        <!-- *** HOT END ***-->
                    </div>
                    <!--
                    *** GET INSPIRED ***
                    _________________________________________________________
                    -->
                    <div class="container">
                        <div class="col-md-12">
                            <div class="box slideshow">
                                <div id="get-inspired" class="owl-carousel owl-theme">
                                    <iframe width="100%" height="590px" src="https://www.youtube.com/embed/GdlSWFyYA8s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- *** GET INSPIRED END ***-->
                    <!--
                    *** BLOG HOMEPAGE ***
                    _________________________________________________________
                    -->
                    <div class="box text-center">
                        <div class="container">
                            <div class="col-md-12">
                                <h2  style="font-weight: bold;">CHIA SẺ TIN TỨC</h2>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="col-md-12">
                            <div id="blog-homepage" class="row blog-home">
                                <c:forEach var="i" items="${listB}">
                                    <div class="col-sm-6">
                                        <div class="post">
                                            <h4>Mẹo vặt thời trang</h4>
                                            <p class="intro">${i.title}</p>
                                            <p>${i.briefInfor}</p>
                                            <p class="read-more"><a href="blogdetailcontrol?bid=${i.blogId}" class="btn btn-primary">Đọc tiếp</a></p>
                                        </div>
                                    </div>
                                </c:forEach>

                            </div>
                            <!-- /#blog-homepage-->
                        </div>
                    </div>


                    <!-- /.container-->
                    <!-- *** BLOG HOMEPAGE END ***-->
                </div>
            </div>
            <!--
            *** FOOTER ***
            _________________________________________________________
            -->
            <jsp:include page="../Common/Footer.jsp"/>        <!-- *** COPYRIGHT END ***-->
            <!-- JavaScript files-->
            <script>
                function changeMoney() {
                    //format price VietNamDong
                    const priceElements = document.querySelectorAll('.price');
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND'
                    });
                    priceElements.forEach(element => {
                        const price = parseFloat(element.textContent);
                        element.textContent = formatter.format(price);
                    });
                }
                changeMoney();
            </script>
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
            <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
            <script src="js/front.js"></script>    
    </body>
</html>