<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : CartCompletion
    Created on : Jul 8, 2023, 1:58:01 PM
    Author     : Admin
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cart completion</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="favicon.png">
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Loại giày</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-3">
                            <!--
                            *** MENUS AND FILTERS ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Loại giày</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-pills flex-column category-menu">
                                        <li><a href="product?page=1" class="nav-link ">Tất cả sản phẩm</a></li>

                                        <c:forEach items="${sessionScope.list}" var="c">
                                            <li><a href="product?page=1&cateId=${c.categoryId}&pageSize=6" class="nav-link ${c.categoryId==cateId? 'active':''}">${c.categoryName}</a></li>
                                            </c:forEach>

                                    </ul>
                                </div>
                            </div>
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Bộ lọc tìm kiếm sản phẩm</h3>
                                </div>
                            </div>
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Theo giá</h3>
                                </div>
                                <div class="card-body">
                                    <form method="post" action="product">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="0" onclick="uncheckOther(this)"> Dưới 500.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="1" onclick="uncheckOther(this)"> 500.000 - 1.000.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="2" onclick="uncheckOther(this)"> 1.000.000 - 1.500.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="3" onclick="uncheckOther(this)"> 1.500.000 - 2.000.000
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="4" onclick="uncheckOther(this)"> Trên 2.000.000
                                                </label>
                                            </div>
                                        </div>
                                        <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>
                                    </form>
                                </div>
                            </div>
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Kích thước</h3>
                                </div>
                                <div class="card-body">
                                    <form method="post" action="product">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="35" onclick="uncheckOther(this)"> Size 35
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="36" onclick="uncheckOther(this)"> Size 36
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="37" onclick="uncheckOther(this)"> Size 37
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="38" onclick="uncheckOther(this)"> Size 38
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="39" onclick="uncheckOther(this)"> Size 39
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="40" onclick="uncheckOther(this)"> Size 40
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="41" onclick="uncheckOther(this)"> Size 41
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="option" value="42" onclick="uncheckOther(this)"> Size 42
                                                </label>
                                            </div>
                                        </div>
                                        <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="box info-bar">

                                <div class="row">

                                    <div class=" products-number-sort">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form role="search" class="ml-auto" action="searchproduct" method="post">
                                                    <div class="input-group">  
                                                        <div class="input-group-append">
                                                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                        </div>
                                                        <input style="width: 500px;" type="text" placeholder="Search" class="form-control" name="search">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="box info-bar">
                                <div id="error-page" class="row">
                                    <div class="col-md-12 mx-auto">
                                        <div class="box text-center py-5">
                                            <p class="text-center"><img src="Icon/logo_3.png" alt="Obaju template"></p>
                                            <h3>Xác thực mua hàng thành công</h3>
                                            <h4 class="text-muted">Cảm ơn bạn đã mua hàng ở cửa hàng chúng tôi</h4>
                                            <h4 class="text-muted">Đơn hàng của bạn đã được lưu tại <a href="myorder?id=1" style="color: blue; text-decoration: none;">Đơn hàng của tôi</a></h4>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.col-lg-9-->
                            <div class="row">
                                <c:forEach var="i" items="${sessionScope.listP}">
                                    <c:if test="${i.status}">
                                        <div class="col-md-3 col-sm-3">
                                            <div class="product">
                                                <div class="flip-container">
                                                    <div class="flipper">
                                                        <div class="front"><a href="productdetail?id=${i.productid}"><img style="height: 150px; width: 200px;" src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a></div>
                                                        <div class="back"><a href="productdetail?id=${i.productid}"><img style="height: 150px; width: 200px;" src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a></div>
                                                    </div>
                                                </div><a href="productdetail?id=${i.productid}" class="invisible"><img style="height: 150px; width: 200px;" src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a>
                                                <div class="text">
                                                    <h3><a href="productdetail?id=${i.productid}">${i.productname}</a></h3>
                                                    <div class="price-product blink">
                                                        Sale:
                                                        <span class="price text-danger ">${i.salePrices}VND</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            *** FOOTER ***
            _________________________________________________________
            -->
            <jsp:include page="../Common/Footer.jsp"/>  
            <!-- *** COPYRIGHT END ***-->
            <!-- JavaScript files-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
            <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
            <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
            <script src="js/front.js"></script>
    </body>
</html>
