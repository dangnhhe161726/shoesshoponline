<%-- 
    Document   : ForgotPassword
    Created on : Jun 5, 2023, 8:30:48 AM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div class="container padding-bottom-3x mb-2 mt-5">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-10">
                    <div class="forgot">
                        <h1>Quên mật khẩu?</h1>
                        <p>Thay đổi mật khẩu của bạn chỉ trong ba bước đơn giản. Điều này sẽ giúp bạn bảo mật tài khoản của mình!</p>
                        <ol class="list-unstyled">
                            <li><span class="text-primary text-medium">1. </span>Nhập địa chỉ email của bạn.</li>
                            <li><span class="text-primary text-medium">2. </span>Hệ thống của chúng tôi sẽ gửi mã OTP vào email của bạn.</li>
                            <li><span class="text-primary text-medium">3. </span>Nhập OTP để xác minh.</li>
                        </ol>
                    </div>

                    <form class="card mt-4" action="forgotpassword" method="POST">

                        <div class="card-body">
                            <div class="form-group">
                                 <% if(request.getAttribute("error")!=null){%>
                                                        <p  h6" style=" color: red" ><b><% out.print((String)request.getAttribute("error"));  }
                                                            %>  </b></p> 
                                <label for="email-for-pass">Nhập địa chỉ email của bạn:</label> <input
                                    class="form-control" type="text" name="email"><p
                                    class="form-text text-muted">Nhập địa chỉ email bạn đã đăng ký. Sau đó, chúng tôi sẽ gửi email OTP đến địa chỉ này.</small>

                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-success" type="submit">Tạo mật khẩu mới</button>
                            <button class="btn btn-danger" ><a href="Login">Trở lại ĐĂNG NHẬP</a>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <jsp:include page="../Common/Footer.jsp"/>   
        <!-- *** COPYRIGHT END ***-->
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>
        <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.js"
        ></script>
    </body>
</html>

