<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : Contact
    Created on : Jun 6, 2023, 3:35:33 PM
    Author     : ADMIN
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">

    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang Chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Phản Hồi Chung</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-3">
                            <!--
                            *** PAGES MENU ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Phản Hồi Chung</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-pills flex-column">
                                        <li><a href="home" class="nav-link">Trang chủ</a></li>
                                        <li><a href="contact" class="nav-link">Phản Hồi Chung</a></li>
                                        <li><a href="categoryblog" class="nav-link">Tin Tức</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- *** PAGES MENU END ***-->
                        </div>
                        <div class="col-lg-9">
                            <div id="contact" class="box">
                                <h1>Phản Hồi Với Chúng Tôi</h1>
                                <p class="lead">
                                    Bạn có tò mò về một cái gì đó? Bạn có một số loại vấn đề với các sản phẩm của chúng tôi?</p>
                                <p>
                                    Xin vui lòng liên hệ với chúng tôi, trung tâm dịch vụ khách hàng của chúng tôi làm việc cho bạn 24/7.</p>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3><i class="fa fa-map-marker"></i>Địa chỉ</h3>
                                        <p>2G7G+7R Thạch Thất<br>Hoà Lạc<br>Đại Học Fpt<br>Hà Nội<br><strong>Việt Nam</strong></p>
                                    </div>
                                    <!-- /.col-sm-4-->
                                    <div class="col-md-4">
                                        <h3><i class="fa fa-phone"></i>Trung tâm cuộc gọi</h3>
                                        <p class="text-muted">
                                            Số này miễn phí nếu gọi từ Hà NộI, nếu không chúng tôi khuyên bạn nên sử dụng hình thức liên lạc điện tử.</p>
                                        <p><strong>0857432886</strong></p>
                                    </div>
                                    <!-- /.col-sm-4-->
                                    <div class="col-md-4">
                                        <h3><i class="fa fa-envelope"></i>
                                            Hỗ trợ điện tử </h3>
                                        <p class="text-muted">Vui lòng viết email cho chúng tôi hoặc sử dụng hệ thống điện tử.</p>
                                        <ul>
                                            <li><strong><a>info@fakeemail.com</a></strong></li>
                                            <li><strong><a>Ticketio</a></strong> - our ticketing support platform</li>
                                        </ul>
                                    </div>
                                    <!-- /.col-sm-4-->
                                </div>
                                <!-- /.row-->
                                <hr>
                                <div>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.4855347082876!2d105.52448937498035!3d21.01324998063224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2zVHLGsOG7nW5nIMSQ4bqhaSBI4buNYyBGUFQ!5e0!3m2!1svi!2s!4v1686409766123!5m2!1svi!2s" width="750" height="600" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                </div>
                                <hr>
                                <hr>
                                <c:if test="${sessionScope.id == null}">
                                    <h2>Nếu bạn muốn phản hồi. Vui lòng <a href="login" style="text-decoration: none; color: red;">đăng nhập</a></h2>
                                </c:if>
                                <c:if test="${sessionScope.id != null}">
                                    <h2>Phản Hồi Của Bạn Dành Cho Chúng Tôi</h2>
                                    <form action="contact" method="post" id="form2">
                                        <input type="hidden" class="form-control-sm" name="id" value="${profile.userId}">
                                        <input type="hidden" class="form-control-sm" name="password" value="${profile.password}">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Họ Và Tên:</label>
                                                    <input id="name" type="text" class="form-control" name="name" value="${profile.fullName}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input id="email" type="email" class="form-control" name="email" value="${profile.email}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone">Địa Chỉ:</label>
                                                    <input id="address" type="text" class="form-control" name="phone" value="${profile.address}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="subject">Tiêu Đề Phản Hồi:</label>
                                                    <input id="title" name="fullname" type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="message">Nhập Phản Hồi Của Bạn:</label>
                                                    <textarea id="mess" name="feedback" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                <button  onclick="confirmSubmit1()"  type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send message</button>
                                            </div>
                                        </div>

                                    </form>
                                </c:if>
                                <script>
                                    function  confirmSubmit() {

                                        var username = document.getElementById("email").value;
                                        var password = document.getElementById("pass").value;
                                        var title = document.getElementById("title").value;
                                        var message = document.getElementById("mess").value;

                                        if (username === "" || password === "" || title === "" || message === "") {
                                            event.preventDefault();
                                            alert("Vui lòng điền đầy đủ thông tin.");
                                        } else {
                                            alert("Gửi Phản Hồi Thành Công");

                                            document.getElementById("form1").submit();
                                        }

                                    }

                                    function  confirmSubmit1() {
                                        var userName = document.getElementById("name").value;
                                        var email = document.getElementById("email").value;
                                        var address = document.getElementById("address").value;
                                        var title = document.getElementById("title").value;
                                        var message = document.getElementById("mess").value;

                                        if (userName === "" || email === "" || address === "" || title === "" || message === "") {
                                            event.preventDefault();
                                            alert("Vui lòng điền đầy đủ thông tin.");
                                        } else {
                                            alert("Gửi Phản Hồi Thành Công");
                                            document.getElementById("form2").submit();
                                        }

                                    }
                                </script>
                            </div>
                        </div>
                        <!-- /.col-md-9-->
                    </div>
                </div>
            </div>
        </div>

        <!--
        *** FOOTER ***
        _________________________________________________________
        -->
        <jsp:include page="../Common/Footer.jsp"/>   
        <!-- *** COPYRIGHT END ***-->
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js"></script>
        <script src="js/map.js"></script>
    </body>
</html>
