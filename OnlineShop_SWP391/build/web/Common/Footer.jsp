<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <h4 class="mb-3">Các trang</h4>
                <ul class="list-unstyled">
                    <li><a href="home">Trang chủ</a></li>
                    <li><a href="product?page=1">Sản phẩm</a></li>
                    <li><a href="categoryBlog">Chia sẻ</a></li>
                    <li><a href="contact">Phản hồi chung</a></li>
                </ul>
                <hr>
                <h4 class="mb-3">Thao tác người dùng</h4>
                <ul class="list-unstyled">
                    <li><a href="login">Đăng nhập</a></li>
                    <li><a href="register">Đăng ký</a></li>
                </ul>
            </div>
            <!-- /.col-lg-3-->
            <div class="col-lg-3 col-md-6">
                <h4 class="mb-3">Loại giày của Shop</h4>
                <ul class="list-unstyled">
                    <c:forEach items="${sessionScope.list}" var="c">
                        <li><a href="product?page=1&cateId=${c.categoryId}&pageSize=3" class="nav-link ${c.categoryId==cateId? 'active':''}">${c.categoryName}</a></li>
                    </c:forEach>
                </ul>
            </div>
            <!-- /.col-lg-3-->
            <div class="col-lg-3 col-md-6">
                <h4 class="mb-3">Có thể tìm chúng tôi ở đâu ?</h4>
                <p><strong>Địa chỉ:</strong><br>69 Thôn 3<br>Hoà Lạc<br>Thạch Thất<br>Hoà Lạc<br><strong>Việt Nam</strong></p><a href="contact">Đến trang liên hệ</a>
                <hr class="d-block d-md-none">
            </div>
            <!-- /.col-lg-3-->
            <div class="col-lg-3 col-md-6">
                <h4 class="mb-3">Châm ngôn của shop</h4>
                <p class="text-muted">Bảo vệ đôi chân của bạn, chúng tôi luôn luôn đầu tư về chất liệu, giúp bạn có một sản phẩm sạch và bền nhất</p>
                <hr>
                <h4 class="mb-3">Chúng tôi có mặt tại</h4>
                <p class="social"><a href="https://www.facebook.com/adidasVN/" class="facebook external"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/adidas" class="twitter external"><i class="fa fa-twitter"></i></a><a href="https://www.instagram.com/adidas/" class="instagram external"><i class="fa fa-instagram"></i></p>
            </div>
            <!-- /.col-lg-3-->
        </div>
        <!-- /.row-->
    </div>
    <!-- /.container-->
</div>
<!-- /#footer-->
<!-- *** FOOTER END ***-->


<!--
*** COPYRIGHT ***
_________________________________________________________
-->
<div id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-2 mb-lg-0">
                <p class="text-center text-lg-left">Được thành lập ©2023</p>
            </div>
        </div>
    </div>
</div>  

