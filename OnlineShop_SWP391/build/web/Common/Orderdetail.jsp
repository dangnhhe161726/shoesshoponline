<%-- 
    Document   : Orderdetail
    Created on : Jun 12, 2023, 9:39:28 AM
    Author     : pc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <body>
        <!-- navbar-->
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang Chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item"><a href="myorder">Đơn Hàng Của Tôi</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Chi Tiết Đơn Hàng</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-3">
                            <!--
                            *** CUSTOMER MENU ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu">
                                <div class="card-header">
                                    <h3 class="h4 card-title">Lựa Chọn Của Tôi</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-pills flex-column"><a href="myorder" class="nav-link active"><i class="fa fa-list"></i> Đơn Mua</a><a href="editprofile" class="nav-link"><i class="fa fa-user"></i> Tài Khoản Của Tôi</a><a href="logout" class="nav-link"><i class="fa fa-sign-out"></i> Đăng Xuất</a></ul>

                                </div>
                            </div>
                            <!-- /.col-lg-3-->
                            <!-- *** CUSTOMER MENU END ***-->
                        </div>
                        <div id="customer-order" class="col-lg-9">
                            <div class="box">
                                <div class="row">
                                    <h1 class="col-md-6">Chi Tiết Đơn Hàng</h1>
                                    <div class="col-md-6">
                                        <form  role="search" action="orderdetail" method="get">
                                            <div style="display: flex">
                                                <input type="hidden" class="col-md-10 form-control" value="${requestScope.Id}" name="Id">
                                                <input type="text" class="col-md-10 form-control" name="txt">
                                                <input data-toggle="collapse" class="btn navbar-btn btn-primary d-none d-lg-inline-block" type="submit" value="Search" class="fa fa-search"/>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <br>
                                <p class="text-muted">Nếu bạn có bất kỳ câu hỏi nào, xin vui lòng <a href="contact">liên hệ</a> với chúng tôi, trung tâm dịch vụ khách hàng của chúng tôi làm việc cho bạn 24/7.</p>
                                <hr>
                                <c:if test="${hiddenSearch}">
                                    <img style="margin-left: 100px;" src="Icon/no-search-product.jpg" alt="Không Tìm Thấy Sản Phẩm"/>
                                </c:if>
                                <c:if test="${!hiddenSearch}">
                                    <div class="table-responsive mb-4">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Sản Phẩm</th>
                                                    <th style="width:100px">Số lượng</th>
                                                    <th>Kích thước</th>
                                                    <th>Ngày Đặt</th>
                                                    <th>Giá Tiền</th>
                                                    <th>Tình Trạng</th>
                                                    <th>Tổng</th>
                                                        <c:if test ="${listOrder.getStatusOrder().getStatusOrderId() == 5 || listOrder.getStatusOrder().getStatusOrderId() == 4}">
                                                        <th style="width:100px">Thao Tác</th>
                                                        </c:if>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="o" items="${list1}">   
                                                    <tr>
                                                        <td><img src="img/${o.getOrderDetail().getStatusOrder().getPi().getImages()}" alt="alt"/></td>
                                                        <td>${o.getOrderDetail().getStatusOrder().getPi().getProduct().getProductname()}</td>
                                                        <td>${o.getOrderDetail().getQuantity()}</td>
                                                        <td>${o.size.getSizeName()}</td>
                                                        <td style="width:120px">${o.getOrderdate()}</td>
                                                        <td> <fmt:formatNumber pattern="##.#" value="${o.getOrderDetail().getStatusOrder().getPi().getProduct().getSalePrices()}"/>đ</td>
                                                        <td style="width:150px">${o.getOrderDetail().getStatusOrder().getStatusName()}</td>
                                                        <td> <fmt:formatNumber pattern="##.#" value="${o.getOrderDetail().getStatusOrder().getPi().getProduct().getSalePrices() * o.getOrderDetail().getQuantity()}"/>đ</td>
                                                        <td>
                                                            <c:if test ="${o.orderDetail.getStatusOrder().getStatusOrderId()  == 4}">
                                                                <a style="width: 75px; height: 25px; margin-bottom: 10px; margin-left: 5px;" href="feedback?oid=${o.orderDetail.statusOrder.getPi().product.getProductid()}" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Đánh giá</a> 
                                                            </c:if>
                                                            <c:if test ="${o.orderDetail.getStatusOrder().getStatusOrderId()  == 4 || o.orderDetail.getStatusOrder().getStatusOrderId()  == 5}">
                                                                <a style="width: 75px; height:25px;" href="buybackproduct?productId=${o.orderDetail.statusOrder.getPi().product.getProductid()}&sizeProduct=${o.size.getSizeName()}&quantity=${o.getOrderDetail().getQuantity()}" class="btn btn-secondary btn-sm" role="button" aria-pressed="true">Mua lại</a>
                                                            </c:if>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                            <tfoot>
                                                <c:if test ="${listOrder.getStatusOrder().getStatusOrderId() == 1}">
                                                    <tr>
                                                        <th colspan="12" class="text-right"><a href="contact" class="btn btn-primary active" role="button" aria-pressed="true">Liên Hệ</a> <a href="orderdetail?Id=${listOrder.getOrderid()}&flag=1" class="btn btn-secondary active" role="button" aria-pressed="true">Hủy Đơn</a></th>
                                                    </tr>
                                                </c:if> 
                                                <tr>
                                                    <th colspan="12" class="text-right" style="font-weight: bold;">Tổng tiền Hóa Đơn</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="12" class="text-right"><fmt:formatNumber pattern="##.#" value="${listOrder.totalCost}"/>đ</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive-->
                                    <div class="row addresses">
                                        <div style="text-align: left" class="col-lg-6">
                                            <h2>Địa Chỉ Cửa Hàng</h2>
                                            <p><strong>Quán:</strong> Anh Em Cây Khế</p>
                                            <p><strong>Địa chỉ:</strong> Hoà Lạc, Thạch Thất, Hà Nội</p>
                                            <p><strong>Số Điện Thoại:</strong> 0987654321</p>
                                        </div>
                                        <div style="text-align: left" class="col-lg-6">
                                            <h2>Địa Chỉ Giao Hàng</h2>
                                            <p><strong>ID Đơn Hàng:</strong> ${listOrder.getOrderid()}</p>
                                            <p><strong>Người Nhận:</strong> ${listOrder.getFullname()}</p>
                                            <p><strong>Địa chỉ:</strong> ${listOrder.getAddress()}</p>
                                            <p><strong>Số Điện Thoại:</strong>  ${listOrder.getMobile()}</p>
                                        </div>
                                    </div>
                                </c:if>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--
        *** FOOTER ***
        _________________________________________________________
        -->
        <jsp:include page="../Common/Footer.jsp"/>            
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>   
    </body>
</html> 