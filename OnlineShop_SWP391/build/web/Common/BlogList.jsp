<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">

        <style>
            li a:hover span {
                background-color: #4fbfa8;
                color: white;
            }

            .a dash{
                color: white;
                background-color: #4fbfa8;
            }
            li span:hover{
                text-decoration: none;
                color: white; /* Màu chữ khi di chuột */
                font-weight: bold; /* In đậm khi di chuột */
                background-color: #4fbfa8;
                padding:7px 15px;
                border-radius: 5px ;
                margin-top: -5px;

                a.active{
                    color: pink;
                    font-weight: bold;
                }
            }
            .image{
                height: auto;
                width: 100%;
            }
            @keyframes blink {
                0% {
                    opacity: 1;
                }
                50% {
                    opacity: 0;
                }
                100% {
                    opacity: 1;
                }
            }

            .blinking-text {
                animation: blink 2s infinite;
            }

            .search-blog{
                display: flex;
            }
        </style>
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active"><a href="categoryblog">Danh sách tin tức</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <!--
                            *** BLOG MENU ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu mb-4">
                                <div class="card-header">
                                    <h3 class="h4 panel-title">Blog</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav flex-column nav-pills">
                                        <c:forEach var="o" items="${listCB}">
                                            <li><a href="categorycontrol?cateId=${o.categoryBlogId}" class="nav-link"${o.categoryBlogId==cateId?'active':'' }>${o.categoryBlogName}</a></li>
                                            </c:forEach>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.col-lg-3-->
                            <!-- *** BLOG MENU END ***-->
                            <div >
                            </div>
                        </div>
                        <!--
                        *** LEFT COLUMN ***
                        _________________________________________________________
                        -->
                        <div id="blog-listing" class="col-lg-9">

                            <div class="box">
                                <div class="row">
                                    <h1 class="col-md-6">Tìm kiếm tin tức:</h1>
                                    <div  class="col-md-6">

                                        <form role="search" class="ml-auto" action="searchblog" method="post">
                                            <div class="input-group">
                                                <input type="text" placeholder="Search" class="form-control" name="txt">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <c:if test="${hidden}">
                                <img style="width: 100%;margin-bottom: 20px;" src="Icon/no-search-product.jpg" alt="alt"/>
                            </c:if>
                            <c:if test="${!hidden}">

                                <div class="box">
                                    <h1 class="blinking-text" style="margin-bottom: 30px; color: #03A9F4; font-style: italic; font-weight: bold;">Bài viết mới nhất!</h1>
                                    <h2><a   href="blogdetailcontrol?bid=${lastB.blogId}">${lastB.title}</a></h2>
                                    <hr>
                                    <p class="date-comments"><a href="#"><i class="fa fa-calendar-o"></i>${lastB.u.fullName}  | ${lastB.updateDate}</a></p>
                                    <div><a href="blogdetailcontrol?bid=${lastB.blogId}"> <img class="image" src="img/${lastB.thumbnail}" alt="${lastB.thumbnail}"/></a></div>
                                    <p style="margin-top: 30px;" class="intro">${lastB.briefInfor}</p>
                                    <p class="read-more"><a href="blogdetailcontrol?bid=${lastB.blogId}" class="btn btn-primary">Đọc tiếp</a></p>

                                </div>
                                <!-- post-->
                                <c:forEach var="o" items="${listB}">
                                    <c:if test="${o.status}">
                                        <div class="post">
                                            <h2><a href="blogdetailcontrol?bid=${o.blogId}">${o.title}</a></h2>
                                            <hr>
                                            <p class="date-comments"><a href="#"><i class="fa fa-calendar-o"></i> <a href="#"> ${o.u.fullName}  | </a>  ${o.updateDate}</a></p>
                                            <div><a href="blogdetailcontrol?bid=${o.blogId}"> <img  class="image" src="img/${o.thumbnail}" alt="${o.thumbnail}"/></a></div>
                                            <p style="margin-top: 30px;" class="intro">${o.briefInfor}</p>
                                            <p class="read-more"><a href="blogdetailcontrol?bid=${o.blogId}" class="btn btn-primary">Đọc tiếp</a></p>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <div class="pager d-flex justify-content-between">
                                </div>
                                <div class="pages">
                                    <nav aria-label="Page navigation example" class="d-flex justify-content-center">
                                        <ul class="pagination">
                                            <li class="page-item <c:if test='${currentPage == 1}'>disabled</c:if>">
                                                <a href="categoryblog?index=${currentPage - 1}" aria-label="Previous" class="page-link">
                                                    <span aria-hidden="true">«</span><span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            <c:forEach var="o" begin="1" end="${endPage}">
                                                <li class="page-item">
                                                    <a href="categoryblog?index=${o}" class="page-link ${o == index? 'active':''}">${o}</a>
                                                </li>
                                            </c:forEach>
                                            <li class="page-item <c:if test='${currentPage == endPage}'>disabled</c:if>">
                                                <a href="categoryblog?index=${currentPage + 1}" aria-label="Next" class="page-link">
                                                    <span aria-hidden="true">»</span><span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </c:if>
                        </div>
                        <!-- /.col-lg-9-->
                        <!-- *** LEFT COLUMN END ***-->
                    </div>
                </div>
            </div>
        </div>
        <!--
        *** FOOTER ***
        _________________________________________________________
        -->
        <jsp:include page="../Common/Footer.jsp"/>        <!-- *** COPYRIGHT END ***-->
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>    
    </body>
</html>

