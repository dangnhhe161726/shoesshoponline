<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Chỉnh sửa sản phẩm | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
        <script src="http://code.jquery.com/jquery.min.js" type="text/javascript"></script>
        <script>
            function readURL(input, thumbimage) {
                if (input.files && input.files[0]) { //Sử dụng  cho Firefox - chrome
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#thumbimage").attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else { // Sử dụng cho IE
                    $("#thumbimage").attr('src', input.value);
                }
                $("#thumbimage").show();
                $('.filename').text($("#uploadfile").val());
                $('.Choicefile').css('background', '#14142B');
                $('.Choicefile').css('cursor', 'default');
                $(".removeimg").show();
                $(".Choicefile").unbind('click');

            }
            $(document).ready(function () {
                $(".Choicefile").bind('click', function () {
                    $("#uploadfile").click();

                });
                $(".removeimg").click(function () {
                    $("#thumbimage").attr('src', '').hide();
                    $("#myfileupload").html('<input type="file" id="uploadfile"  onchange="readURL(this);" />');
                    $(".removeimg").hide();
                    $(".Choicefile").bind('click', function () {
                        $("#uploadfile").click();
                    });
                    $('.Choicefile').css('background', '#14142B');
                    $('.Choicefile').css('cursor', 'pointer');
                    $(".filename").text("");
                });
            })

            var loadFile = function (event, id) {
                var output = document.getElementById('subImg-' + id);
                output.src = URL.createObjectURL(event.target.files[0]);
                output.onload = function () {
                    URL.revokeObjectURL(output.src) // free memory
                }
            };

            function confirmCancel(event) {
                var confirmation = confirm("Bạn có chắc chắn muốn huỷ thêm sản phẩm mới?");
                if (!confirmation) {
                    event.preventDefault(); // Ngăn chặn điều hướng đến đường dẫn URL
                }
            }
            function changeSubImg() {
                document.getElementById("myForm").submit();
            }

        </script>
    </head>

    <body class="app sidebar-mini rtl">
        <style>
            .Choicefile {
                display: block;
                background: #14142B;
                border: 1px solid #fff;
                color: #fff;
                width: 150px;
                text-align: center;
                text-decoration: none;
                cursor: pointer;
                padding: 5px 0px;
                border-radius: 5px;
                font-weight: 500;
                align-items: center;
                justify-content: center;
            }

            .Choicefile:hover {
                text-decoration: none;
                color: white;
            }

            #uploadfile,
            .removeimg {
                display: none;
            }

            #thumbbox {
                position: relative;
                width: 100%;
                margin-bottom: 20px;
            }

            .removeimg {
                height: 25px;
                position: absolute;
                background-repeat: no-repeat;
                top: 5px;
                left: 5px;
                background-size: 25px;
                width: 25px;
                /* border: 3px solid red; */
                border-radius: 50%;

            }

            .removeimg::before {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                content: '';
                border: 1px solid red;
                background: red;
                text-align: center;
                display: block;
                margin-top: 11px;
                transform: rotate(45deg);
            }

            .removeimg::after {
                /* color: #FFF; */
                /* background-color: #DC403B; */
                content: '';
                background: red;
                border: 1px solid red;
                text-align: center;
                display: block;
                transform: rotate(-45deg);
                margin-top: -2px;
            }
            .swatch-element label{
                margin-right: 20px;
            }
            .swatch-element:hover {
                color: #0397d6;
            }

            .swatch-element input[type="radio"]:checked + label {
                font-weight: bold;
                font-size: 20px;
                color: #4fbfa8;
            }

            .input-number{
                border: 1px solid white;
                width: 80px;
            }
        </style>
        <jsp:include page="../Manager/Dashbroad.jsp"/> 
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="productlistmanager">Danh sách sản phẩm \ </a></li>
                    <li style="color: #138496"> Thêm sản phẩm</li>
                </ul>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <h3 class="tile-title">Thêm sản phẩm</h3>
                        <div class="tile-body">
                            <form id="myForm" action="addproductdetailmanager" method="post" enctype="multipart/form-data">                     
                                <div class="row">
                                    <div class="form-group col-md-9">
                                        <label class="control-label">Tên sản phẩm</label>
                                        <input name="productName" class="form-control" type="text" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="exampleSelect1" class="control-label">Danh mục</label>
                                        <select name="category" class="form-control">
                                            <c:forEach var="i" items="${listCa}">
                                                <option value="${i.categoryId}">${i.categoryName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Giá bán</label>
                                        <input name="salePrices" class="form-control" type="number" min="0" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Giá vốn</label>
                                        <input name="originalPrices" class="form-control" type="number" min="0" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Tiêu đề</label>
                                    <textarea class="form-control" name="brefInfo" rows="1" required></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Mô tả sản phẩm</label>
                                    <textarea class="form-control" name="productDetail" required></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label class="control-label">Ảnh chính cho sản phẩm</label>
                                    </div>
                                    <div class="col-md-4" style="margin-top: 20px;">
                                        <div id="myfileupload">
                                            <input type="file" id="uploadfile" name="ImageUpload" onchange="readURL(this);" />
                                        </div>
                                        <div id="thumbbox">
                                            <img height="200" width="200" alt="Thumb image" id="thumbimage" style="display: none" />
                                            <a class="removeimg" href="javascript:"></a>
                                        </div>
                                        <div id="boxchoice">
                                            <a href="javascript:" class="Choicefile"><i class="fas fa-cloud-upload-alt"></i> Chọn ảnh thay đổi</a>
                                            <p style="clear:both"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-right: 100px;">
                                        <table border="1">
                                            <thead>
                                                <tr>
                                                    <th>Ảnh phụ</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="10">Thêm ảnh phụ:<input name="subImg" type="file" accept="image/*"  onchange="addSubImg()"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <p>Thêm size:</p>
                                        <table>
                                            <thead>
                                            <th>Size</th>
                                            <th>Số lượng</th>
                                            </thead>
                                            <tbody>
                                            <td><input class="input-number" type="number" min="36" max="42" name="size" required></td>
                                            <td><input class="input-number" type="number" min="1" name="quantity" required></td>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" style="text-align: right;">
                                        <button class="btn btn-save" style="width: 200px; height: 50px;" type="submit">Thêm sản phẩm</button>
                                        <a style="width: 100px; height: 50px;" class="btn btn-cancel" href="addproductdetailmanager" onclick="confirmCancel(event);">Hủy bỏ</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script src="js/plugins/pace.min.js"></script>
    </body>

</html>