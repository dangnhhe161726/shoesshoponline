<%-- 
    Document   : userlist
    Created on : Jun 21, 2023, 4:09:39 PM
    Author     : asus
--%>
<%@page import ="dao.UserDAO"%>
<%@page import="java.util.List"%>
<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách người dùng | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <jsp:include page="../Manager/Dashbroad.jsp"/>        
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Danh sách người dùng</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">

                            <div class="row element-button">
                                <div class="col-sm-2">

                                    <a class="btn btn-add btn-sm" href="adduser" title="Thêm"><i class="fas fa-plus"></i>
                                        Tạo mới người dùng</a>
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-delete btn-sm nhap-tu-file" type="button" title="Nhập" onclick="myFunction(this)"><i
                                            class="fas fa-file-upload"></i> Tải từ file</a>
                                </div>

                                <div class="col-sm-2">
                                    <a class="btn btn-delete btn-sm print-file" type="button" title="In" onclick="myApp.printTable()"><i
                                            class="fas fa-print"></i> In dữ liệu</a>
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-delete btn-sm print-file js-textareacopybtn" type="button" title="Sao chép"><i
                                            class="fas fa-copy"></i> Sao chép</a>
                                </div>

                                <div class="col-sm-2">
                                    <a class="btn btn-excel btn-sm" href="" title="In"><i class="fas fa-file-excel"></i> Xuất Excel</a>
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-delete btn-sm pdf-file" type="button" title="In" onclick="myFunction(this)"><i
                                            class="fas fa-file-pdf"></i> Xuất PDF</a>
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-delete btn-sm" type="button" title="Xóa" onclick="myFunction(this)"><i
                                            class="fas fa-trash-alt"></i> Xóa tất cả </a>
                                </div>
                            </div>
                            <style>
                                .form-container {
                                    display: flex;
                                    align-items: center;
                                    margin-bottom: 20px;
                                }

                                .form-container form {
                                    display: flex;
                                    align-items: center;
                                }

                                .form-container label {
                                    margin-right: 10px;
                                    font-size: 16px;
                                }

                                .form-container select, .form-container input[type="text"] {
                                    padding: 8px;
                                    font-size: 14px;
                                    border: 1px solid #ccc;
                                    border-radius: 4px;
                                }

                                .form-container input[type="submit"] {
                                    padding: 8px 16px;
                                    font-size: 14px;
                                    border: none;
                                    background-color: #4CAF50;
                                    color: white;
                                    cursor: pointer;
                                    border-radius: 4px;
                                }

                                .form-container input[type="submit"]:hover {
                                    background-color: #45a049;
                                }
                            </style>

                            <div class="form-container">
                                <form action="usersearching" method="post">
                                    <label>Tìm kiếm theo:</label>
                                    <select name="option">
                                        <option value="Fullname">Họ và tên</option>
                                        <option value="email">Email</option>
                                        <option value="mobile">Số điện thoại</option>
                                    </select>
                                    <input type="text" name="value">
                                    <input type="submit" value="SEARCH">
                                </form>
                            </div>

                            <div class="form-container">
                                <form action="userfiltering" method="post">
                                    <label>Loc theo:</label>
                                    <select name="option" id="optionSelect">                                        
                                        <option value="gender" selected>Giới tính</option>
                                        <option value="role_id">Vai trò</option>
                                        <option value="status">Trạng thái</option>
                                    </select>
                                    <div id="valueContainer">
                                        <!-- Nội dung ô giá trị sẽ được thêm vào đây -->
                                    </div>
                                    <input type="submit" value="FILTER">
                                </form>
                            </div>


                            <script>
                                // Lắng nghe sự kiện thay đổi của select box
                                document.getElementById("optionSelect").addEventListener("change", function () {
                                    var selectedOption = this.value;
                                    var valueContainer = document.getElementById("valueContainer");

                                    // Xóa nội dung hiện tại của valueContainer
                                    valueContainer.innerHTML = "";

                                    // Tạo một ô chọn mới cho giá trị
                                    var valueSelect = document.createElement("select");
                                    valueSelect.name = "value";

                                    // Tạo các option cho giá trị dựa trên lựa chọn
                                    if (selectedOption === "gender") {
                                        var maleOption = document.createElement("option");
                                        maleOption.value = "true";
                                        maleOption.text = "Nam";
                                        maleOption.selected = true; // Chọn giới tính "Nam" mặc định
                                        valueSelect.appendChild(maleOption);

                                        var femaleOption = document.createElement("option");
                                        femaleOption.value = "false";
                                        femaleOption.text = "Nữ";
                                        valueSelect.appendChild(femaleOption);
                                    } else if (selectedOption === "role_id") {
                                        // Tạo các option cho vai trò
                                        var adminOption = document.createElement("option");
                                        adminOption.value = "1";
                                        adminOption.text = "Admin";
                                        adminOption.selected = true; // Chọn giới tính "Admin" mặc định
                                        valueSelect.appendChild(adminOption);

                                        var saleOption = document.createElement("option");
                                        saleOption.value = "2";
                                        saleOption.text = "Sale";
                                        valueSelect.appendChild(saleOption); 
                                        
                                        var marketingOption = document.createElement("option");
                                        marketingOption.value = "3";
                                        marketingOption.text = "Marketing";
                                        valueSelect.appendChild(marketingOption); 
                                        
                                        var customerOption = document.createElement("option");
                                        customerOption.value = "4";
                                        customerOption.text = "Khách hàng";
                                        valueSelect.appendChild(customerOption); 
                                        
                                        var saleManagerOption = document.createElement("option");
                                        saleManagerOption.value = "5";
                                        saleManagerOption.text = "Sale Manager";
                                        valueSelect.appendChild(saleManagerOption); 
                                        
                                    } else if (selectedOption === "status") {
                                        // Tạo các option cho trạng thái
                                        // ...
                                    }

                                    // Thêm ô chọn giá trị vào valueContainer
                                    valueContainer.appendChild(valueSelect);
                                });

// Mặc định chọn giới tính "Nam" khi trang được tải
                                var defaultOption = "gender";
                                var defaultGenderOption = "true";

// Lấy đối tượng select box
                                var optionSelect = document.getElementById("optionSelect");

// Thiết lập giá trị mặc định cho select box
                                optionSelect.value = defaultOption;

// Gọi sự kiện change để hiển thị ô giá trị tương ứng với giới tính mặc định
                                var changeEvent = new Event("change");
                                optionSelect.dispatchEvent(changeEvent);

                            </script>
                            <table class="table table-hover table-bordered js-copytextarea" cellpadding="0" cellspacing="0" border="0"
                                   id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th width="150">Họ và tên</th>
                                        <th>Giới tính</th>
                                        <th>Email</th>                     
                                        <th>Số điện thoại</th>
                                        <th>Vai trò</th>
                                        <th>Trạng thái</th>
                                        <th width="100">Tính năng</th>
                                    </tr>
                                </thead>
                                <tbody><% 
                                        List<User> li = (List<User>)request.getAttribute("userlist");
                                        if(li!=null){
                                        for(User s :li){ 
                                    %>
                                    <tr>
                                        <td><%= s.getUserId()%></td>
                                        <td><%= s.getFullName()%></td>
                                        <td>
                                            <% 
                                            boolean gender = s.isGender();
                                            String genderLabel = gender ? "Nam" : "Nữ";
                                            out.println(genderLabel);
                                            %>
                                        </td>
                                        <td><%= s.getEmail()%></td>
                                        <td><%= s.getMobile()%></td>
                                        <td>
                                            <% 
                                              String role = "";
                                              int roleId = s.getRoleId();
                                              if (roleId == 4) {
                                                role = "Khách hàng";
                                              } else if (roleId == 2) {
                                                role = "Sale";
                                              } else if (roleId == 5) {
                                                role = "Sale Manager";  
                                              } else if (roleId == 3) {
                                                role = "Marketing";
                                              } else if (roleId == 1) {
                                                role = "Admin";
                                              }
                                              out.print(role);
                                            %>
                                        </td>
                                        <td>
                                            <% 
                                              String status = "";
                                              int statusCode = s.getStatus();
                                              if (statusCode == 0) {
                                                status = "Vô hiệu hóa";
                                              } else if (statusCode == 1) {
                                                status = "Hoạt động";
                                              }
                                              out.print(status);
                                            %>
                                        </td>
                                        <td class="table-td-center">
                                            <!--                                            <button class="btn btn-primary btn-sm trash" type="button" title="Xóa"
                                                                                                onclick="myFunction(this)"><i class="fas fa-toggle-off"></i>-->
                                            <!--                                            </button>-->
                                            <form action="userdetail" method="post">
                                                <input type="hidden" name="id" value="<%= s.getUserId()%>">
                                                <button class="btn btn-primary btn-sm edit" type="submit" title="Sửa">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </form>
                                        </td>

                                        </td>
                                    </tr>             
                                    <%}}%>

                                    </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <!--
 

        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>
            function deleteRow(r) {
                var i = r.parentNode.parentNode.rowIndex;
                document.getElementById("myTable").deleteRow(i);
            }
            jQuery(function () {
                jQuery(".trash").click(function () {
                    swal({
                        title: "Cảnh báo",

                        text: "Bạn có chắc chắn là muốn xóa người dùng này?",
                        buttons: ["Hủy bỏ", "Đồng ý"],
                    })
                            .then((willDelete) => {
                                if (willDelete) {
                                    swal("Đã xóa thành công.!", {

                                    });
                                }
                            });
                });
            });
            oTable = $('#sampleTable').dataTable();
            $('#all').click(function (e) {
                $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
                e.stopImmediatePropagation();
            });

            //EXCEL
            // $(document).ready(function () {
            //   $('#').DataTable({

            //     dom: 'Bfrtip',
            //     "buttons": [
            //       'excel'
            //     ]
            //   });
            // });


            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }
            //In dữ liệu
            var myApp = new function () {
                this.printTable = function () {
                    var tab = document.getElementById('sampleTable');
                    var win = window.open('', '', 'height=700,width=700');
                    win.document.write(tab.outerHTML);
                    win.document.close();
                    win.print();
                }
            }
            //     //Sao chép dữ liệu
            //     var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

            // copyTextareaBtn.addEventListener('click', function(event) {
            //   var copyTextarea = document.querySelector('.js-copytextarea');
            //   copyTextarea.focus();
            //   copyTextarea.select();

            //   try {
            //     var successful = document.execCommand('copy');
            //     var msg = successful ? 'successful' : 'unsuccessful';
            //     console.log('Copying text command was ' + msg);
            //   } catch (err) {
            //     console.log('Oops, unable to copy');
            //   }
            // });


            //Modal
            $("#show-emp").on("click", function () {
                $("#ModalUP").modal({backdrop: false, keyboard: false})
            });
        </script>
    </body>

</html>