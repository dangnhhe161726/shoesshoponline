<%-- 
    Document   : Authorization.jsp
    Created on : Jul 4, 2023, 3:34:47 PM
    Author     : asus
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">

    <head>
        <title>Phân quyền mới</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <jsp:include page="../Manager/Dashbroad.jsp"/>        
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Phân quyền mới</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            <style>
                                .form-container {
                                    display: flex;
                                    align-items: center;
                                    margin-bottom: 20px;
                                }

                                .form-container form {
                                    display: flex;
                                    align-items: center;
                                }

                                .form-container label {
                                    margin-right: 10px;
                                    font-size: 16px;
                                }

                                .form-container select, .form-container input[type="text"] {
                                    padding: 8px;
                                    font-size: 14px;
                                    border: 1px solid #ccc;
                                    border-radius: 4px;
                                }

                                .form-container input[type="submit"] {
                                    padding: 8px 16px;
                                    font-size: 14px;
                                    border: none;
                                    background-color: #4CAF50;
                                    color: white;
                                    cursor: pointer;
                                    border-radius: 4px;
                                }

                                .form-container input[type="submit"]:hover {
                                    background-color: #45a049;
                                }
                            </style>
                            <div class="tile-body">
                                <p style="color: red">${error}</p>
                                <form action="authorizationinsert" method="post">
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Chức vụ</label>
                                        <select class="form-control" name="role">
                                            <option value="0" >Khách chưa đăng nhập</option>
                                            <option value="4" >Khách hàng</option>
                                            <option value="2" >Sale</option>
                                            <option value="5" >Sale Manager</option>
                                            <option value="3" >Marketing</option>
                                            <option value="1" >Admin</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">URL</label>
                                        <input class="form-control" name="url" type="text" id="urlInput">
                                        <small id="urlValidationMessage" class="form-text text-danger" style="display: none;">URL phải bắt đầu bằng '/'</small>
                                    </div>

                                    <script>
                                        // Lắng nghe sự kiện người dùng nhập liệu vào ô input "URL"
                                        document.getElementById("urlInput").addEventListener("input", function () {
                                            var inputValue = this.value;

                                            // Kiểm tra nếu giá trị không bắt đầu bằng "/"
                                            if (!inputValue.startsWith("/")) {
                                                // Hiển thị thông báo lỗi
                                                document.getElementById("urlValidationMessage").style.display = "block";
                                            } else {
                                                // Giá trị hợp lệ, ẩn thông báo lỗi (nếu có)
                                                document.getElementById("urlValidationMessage").style.display = "none";
                                            }
                                        });
                                    </script>

                                    <div class="form-group col-md-4">
                                        <label class="control-label">Tính năng</label>
                                        <input class="form-control" name="feature" type="text" >
                                    </div>
                            </div>
                            <button class="btn btn-save" type="submit">Lưu lại</button>
                            <a class="btn btn-cancel" href="authorizationmanagement">Hủy bỏ</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!--


    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="src/jquery.table2excel.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <!-- Data table plugin-->
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <script>
        function deleteRow(r) {
            var i = r.parentNode.parentNode.rowIndex;
            document.getElementById("myTable").deleteRow(i);
        }
        jQuery(function () {
            jQuery(".trash").click(function () {
                swal({
                    title: "Cảnh báo",

                    text: "Bạn có chắc chắn là muốn xóa người dùng này?",
                    buttons: ["Hủy bỏ", "Đồng ý"],
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                swal("Đã xóa thành công.!", {

                                });
                            }
                        });
            });
        });
        oTable = $('#sampleTable').dataTable();
        $('#all').click(function (e) {
            $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
            e.stopImmediatePropagation();
        });

        //EXCEL
        // $(document).ready(function () {
        //   $('#').DataTable({

        //     dom: 'Bfrtip',
        //     "buttons": [
        //       'excel'
        //     ]
        //   });
        // });


        //Thời Gian
        function time() {
            var today = new Date();
            var weekday = new Array(7);
            weekday[0] = "Chủ Nhật";
            weekday[1] = "Thứ Hai";
            weekday[2] = "Thứ Ba";
            weekday[3] = "Thứ Tư";
            weekday[4] = "Thứ Năm";
            weekday[5] = "Thứ Sáu";
            weekday[6] = "Thứ Bảy";
            var day = weekday[today.getDay()];
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            nowTime = h + " giờ " + m + " phút " + s + " giây";
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = day + ', ' + dd + '/' + mm + '/' + yyyy;
            tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                    '</span>';
            document.getElementById("clock").innerHTML = tmp;
            clocktime = setTimeout("time()", "1000", "Javascript");

            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i;
                }
                return i;
            }
        }
        //In dữ liệu
        var myApp = new function () {
            this.printTable = function () {
                var tab = document.getElementById('sampleTable');
                var win = window.open('', '', 'height=700,width=700');
                win.document.write(tab.outerHTML);
                win.document.close();
                win.print();
            }
        }
        //     //Sao chép dữ liệu
        //     var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

        // copyTextareaBtn.addEventListener('click', function(event) {
        //   var copyTextarea = document.querySelector('.js-copytextarea');
        //   copyTextarea.focus();
        //   copyTextarea.select();

        //   try {
        //     var successful = document.execCommand('copy');
        //     var msg = successful ? 'successful' : 'unsuccessful';
        //     console.log('Copying text command was ' + msg);
        //   } catch (err) {
        //     console.log('Oops, unable to copy');
        //   }
        // });


        //Modal
        $("#show-emp").on("click", function () {
            $("#ModalUP").modal({backdrop: false, keyboard: false})
        });
    </script>
</body>

</html>
