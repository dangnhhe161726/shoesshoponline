/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Authorization;
import model.Blog;
import model.CategoryBlog;

/**
 *
 * @author Admin
 */
public class AuthorizationDAO extends DBcontext {

    //Insert authorization
    public void authorizationInsert(int roleID, String url, String FeatureName) {
        try {
            String sql = "INSERT INTO shoes_shop_online.authorization (role_id,url,feature)VALUES (?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, roleID);
            statement.setString(2, url);
            statement.setString(3, FeatureName);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    //Delete authorization
    public void authorizationRemove(int ID) {
        try {
            String sql = "delete from authorization where id=" + ID;
            PreparedStatement statement = connection.prepareCall(sql);
            statement.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    //Get authorization list
    public ArrayList<Authorization> authorizationList() {
        ArrayList<Authorization> list = new ArrayList<>();
        String sql = "SELECT * FROM authorization;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Authorization u = new Authorization(rs.getInt("id"), rs.getInt("role_id"), rs.getString("url"), rs.getString("feature"));
                list.add(u);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
    //Check authorization
    public boolean authorizationCheck(int role_id, String url) {   
        System.out.println("DAO CHECK: URL: " +url+ " || ROLE ID: "+ role_id);
        if(url.contains("css") || url.contains("js") || url.contains(".jpg") || url.contains("/vendor") || url.contains("/img")|| url.contains(".png") || url.contains("LoadImg")){
           return true; 
        }
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT id FROM shoes_shop_online.authorization where role_id=? and url=?");
            ps.setInt(1, role_id);
            ps.setString(2, url);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                rs.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static void main(String[] args) {
        AuthorizationDAO dao = new AuthorizationDAO();
        System.out.println(dao.authorizationCheck(1,"/authorizationmanagement") );
    }
}
