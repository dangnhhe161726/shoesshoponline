/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Category;
import model.FeedBack;

import model.OrderDetail;
import model.Product;
import model.User;

/**
 *
 * @author quyde
 */
public class AdminDAO extends DBcontext {

    // total order status transport
    public int getTotalOrderTransport() {
        String sql = "SELECT count(*) FROM shoes_shop_online.order where status_order = 2;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    // total order success
    public int getTotalOrderSuccess() {
        String sql = "SELECT count(*) FROM shoes_shop_online.order where status_order = 4;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    //total order cancel

    public int getTotalOrderCancel() {
        String sql = "SELECT count(*) FROM shoes_shop_online.order where status_order = 5;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    // total order customer
    public int getTotalCustomer() {
        String sql = "SELECT count(*) FROM shoes_shop_online.user where role_id = 4 and status = 1;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    // total revenue

    public int getTotalRevenue() {
        String sql = "SELECT sum(od.total_cost) FROM shoes_shop_online.order_detail od,shoes_shop_online.order o\n"
                + "\n"
                + "where od.order_id = o.order_id and o.status_order = 4;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    // get newly register
    public User getNewLyRegisterUser() {

        String sql = "Select * from User where status = 1 and role_id=4 order by userId desc limit 1;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                User u = new User(rs.getInt("userId"), rs.getString("fullName"));

                return u;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // get new customer newly board
    public User getNewLyBoughtUser() {

        String sql = "SELECT * FROM shoes_shop_online.order where status_order = 4 order by orderDate desc limit 1 ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                User u = new User(rs.getInt("userId"), rs.getString("fullName"));

                return u;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public double getAvarageStarFeeback() {
        String sql = "SELECT FORMAT(ROUND(AVG(rated_star), 2), 2) FROM feedback";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                double avgRatedStar = rs.getDouble("FORMAT(ROUND(AVG(rated_star), 2), 2)");
                return avgRatedStar;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public ArrayList<OrderDetail> getTotalRevenueByCategory() {
        ArrayList<OrderDetail> list = new ArrayList<>();
        String sql = "SELECT c.category_name, SUM(od.total_cost) AS total_sum\n"
                + "FROM shoes_shop_online.order_detail od\n"
                + "INNER JOIN shoes_shop_online.order o ON od.order_id = o.order_id\n"
                + "INNER JOIN shoes_shop_online.product p ON od.product_id = p.product_id\n"
                + "INNER JOIN shoes_shop_online.category c ON p.category_id = c.category_id\n"
                + "where o.status_order =4 and c.status = 1\n"
                + "GROUP BY c.category_name;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Category c = new Category(rs.getString("category_name"));
                OrderDetail od = new OrderDetail(rs.getDouble("total_sum"), c);
                list.add(od);

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<FeedBack> getTotalFeedBackByCategory() {
        ArrayList<FeedBack> list = new ArrayList<>();
        String sql = "SELECT c.category_name, count(fb.feedBack_id) AS total_feedback\n"
                + "FROM shoes_shop_online.feedback fb\n"
                + "JOIN shoes_shop_online.product p ON fb.product_id = p.product_id\n"
                + "JOIN shoes_shop_online.category c ON p.category_id = c.category_id\n"
                + "where p.status = 1 and c.status = 1 \n"
                + "GROUP BY c.category_name;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Category c = new Category(rs.getString("category_name"));
                FeedBack fb = new FeedBack(rs.getInt("total_feedback"), c);

                list.add(fb);

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public int getTotalOrderLast7dayAll(String d1, String d2) {
        String sql = "SELECT COUNT(*) AS total_orders\n"
                + "FROM `order`\n"
                + "WHERE orderDate BETWEEN ? AND ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, d1);
            st.setString(2, d2);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_orders");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalOrderLast7daySuccess(String d1, String d2) {
        String sql = "SELECT COUNT(*) AS total_orders_success "
                + "FROM `order` "
                + "WHERE orderDate BETWEEN ? AND ? AND status_order = 4;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, d1);
            st.setString(2, d2);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_orders_success");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public ArrayList<Product> getTopSell() {
        ArrayList<Product> list = new ArrayList<>();
        try {
            String sql = "SELECT p.product_id, p.product_name, c.category_name, p.original_prices,p.sale_prices, SUM(od.quantity) AS total_sold\n"
                    + "FROM shoes_shop_online.order_detail od , shoes_shop_online.product p , shoes_shop_online.category c\n"
                    + "where c.category_id = p.category_id   and od.product_id = p.product_id and p.status =1\n"
                    + "GROUP BY p.product_id, p.product_name, c.category_name, p.original_prices,p.sale_prices\n"
                    + "ORDER BY total_sold DESC\n"
                    + "LIMIT 5;";

            PreparedStatement statement = connection.prepareCall(sql);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Category category = new Category(rs.getString("category_name"));
                //  OrderDetail o=new OrderDetail(rs.getInt(3));
                Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), category, rs.getInt("total_sold"));

                list.add(p);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        AdminDAO dao = new AdminDAO();
        int a = dao.getTotalOrderLast7dayAll("2022-01-21", "2022-01-28");
        System.out.println(a);

    }
}
