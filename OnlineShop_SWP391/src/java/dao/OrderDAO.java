/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import model.Cart;
import model.Item;
import model.Order;
import model.OrderDetail;
import model.Product;
import model.ProductImages;
import model.Size;
import model.StatusOrder;
import model.User;

/**
 *
 * @author pc
 */
public class OrderDAO extends DBcontext {

    public ArrayList<StatusOrder> getallStatusOrders() {
        ArrayList<StatusOrder> list = new ArrayList<>();
        String sql = "SELECT * FROM status_order;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                StatusOrder s = new StatusOrder(rs.getInt(1), rs.getString(2));
                list.add(s);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<Order> getStatusByID(int uid, int sid) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select o.order_id, p.product_name, o.orderDate,o.total_cost, s.status_name, s.status_order_id  from shoes_shop_online.order o ,user u,  status_order s , order_detail ot , product p\n"
                    + "where u.userId = o.userId and status_order_id = o.status_order \n"
                    + "and o.order_id =ot.order_id and p.product_id = ot.product_id\n"
                    + "and u.userId =? and s.status_order_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, uid);
            stm.setInt(2, sid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Product p = new Product(rs.getString(2), 0);
                StatusOrder s = new StatusOrder(rs.getInt(6), rs.getString(5), p);
                Order o = new Order(rs.getInt(1), rs.getDate(3), rs.getDouble(4), s);
                list.add(o);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Order> searchByName(int uid, String txtSearch) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            // Create SQL String
            String sql = "select o.order_id, p.product_name, o.orderDate,o.total_cost s.status_name  from shoes_shop_online.order o ,user u,  status_order s , order_detail ot , product p\n"
                    + "                    where u.userId = o.userId and status_order_id = o.status_order and o.order_id =ot.order_id \n"
                    + "                    and p.product_id = ot.product_id and u.userId =? and p.product_name like ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, uid);
            stm.setString(2, "%" + txtSearch + "%");

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getString(2), 0);
                StatusOrder s = new StatusOrder(1, rs.getString(5), p);
                Order o = new Order(rs.getInt(1), rs.getDate(3), rs.getDouble(4), s);
                list.add(o);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Order> getOrderByIdUser(int uid) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select o.order_id, p.product_name, o.orderDate,o.total_cost,s.status_name, s.status_order_id\n"
                    + "                    from shoes_shop_online.order o ,user u,  status_order s , order_detail ot , product p\n"
                    + "                    where u.userId = o.userId and status_order_id = o.status_order and o.order_id =ot.order_id\n"
                    + "                    and p.product_id = ot.product_id and u.userId =?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, uid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Product p = new Product(rs.getString(2), 0);
                StatusOrder s = new StatusOrder(rs.getInt(6), rs.getString(5), p);
                Order o = new Order(rs.getInt(1), rs.getDate(3), rs.getDouble(4), s);
                list.add(o);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public int getQuantityOfProduct(int size, int pid) {
        try {
            String sql = "select quantity from size where size = ? and product_id = ?";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, size);
            stm.setInt(2, pid);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public void addOrder(User user, Cart cart, String fullName, String mobile, String address, String note) {
        Date currentDate = new Date();
        String formattedDate = formatDate(currentDate, "yyyy-MM-dd HH:mm:ss");
        try {
            //add to table order on database
            String sql = "INSERT INTO `order` (`orderDate`, `total_cost`, `fullName`, `mobile`, `address`, `status_order`, `userId`,`note`) VALUES (?,?,?,?,?,?,?,?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, formattedDate);
            statement.setDouble(2, cart.getTotalMoney() + 50000);
            statement.setString(3, fullName);
            statement.setString(4, mobile);
            statement.setString(5, address);
            statement.setInt(6, 1);
            statement.setInt(7, user.getUserId());
            statement.setString(8, note);
            statement.executeUpdate();
            //Get the id of the newly added oder 
            String sql1 = "SELECT order_id FROM `order` ORDER BY order_id DESC LIMIT 1;";
            PreparedStatement statement1 = connection.prepareStatement(sql1);
            ResultSet rs = statement1.executeQuery();
            //add to Orer Detail
            if (rs.next()) {
                int oid = rs.getInt("order_id");
                for (Item item : cart.getItems()) {
                    String sql2 = "INSERT INTO `order_detail` (`quantity`, `order_id`, `product_id`, `total_cost`,`size`) VALUES (?,?,?,?,?);";
                    PreparedStatement statement2 = connection.prepareCall(sql2);
                    statement2.setInt(1, item.getQuantity());
                    statement2.setInt(2, oid);
                    statement2.setInt(3, item.getProduct().getProductid());
                    statement2.setDouble(4, item.getPrice() * item.getQuantity());
                    statement2.setInt(5, item.getSize().getSizeName());
                    statement2.executeUpdate();
                }
            }
            //update quantity of size product
            OrderDAO dao = new OrderDAO();
            ProductDAO pdao = new ProductDAO();
            for (Item item : cart.getItems()) {
                String sql3 = "UPDATE `size` s SET `quantity` = ? WHERE (s.size = ? and s.product_id = ?);";
                PreparedStatement statement3 = connection.prepareCall(sql3);
                statement3.setInt(1, dao.getQuantityOfProduct(item.getSize().getSizeName(), item.getProduct().getProductid()) - item.getQuantity());
                statement3.setInt(2, item.getSize().getSizeName());
                statement3.setInt(3, item.getProduct().getProductid());
                statement3.executeUpdate();

                //update status of size product
                ArrayList<Size> listSz = pdao.getSizeProductById(item.getProduct().getProductid() + "");
                for (Size size : listSz) {
                    if (size.getSizeName() == item.getSize().getSizeName()) {
                        if (size.getQuantity() == 0) {
                            pdao.updateStatusOfSizeProduct(size.getQuantity(), size.getSizeName(), item.getProduct().getProductid());
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public ArrayList<Order> getOrderDetailByUid(int uid, int oid) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select o.order_id, pi.images ,p.product_name, ot.quantity,o.orderDate, p.sale_prices,s.status_name, s.status_order_id, p.product_id, ot.size from shoes_shop_online.order o ,user u,  status_order s , order_detail ot , product p , products_images pi\n"
                    + "where u.userId = o.userId and status_order_id = o.status_order and o.order_id =ot.order_id \n"
                    + "and p.product_id = ot.product_id and pi.product_id = p.product_id and u.userId =? and o.order_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, uid);
            stm.setInt(2, oid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Product p = new Product(rs.getString("product_name"), rs.getInt("product_id"), rs.getDouble("sale_prices"));
                ProductImages pi = new ProductImages(rs.getString("images"), p);
                StatusOrder s = new StatusOrder(rs.getInt("status_order_id"), rs.getString("status_name"), pi);
                OrderDetail ot = new OrderDetail(rs.getInt("quantity"), s);
                Size size = new Size(rs.getInt("size"));
                Order o = new Order(rs.getInt("order_id"), rs.getDate("orderDate"), ot, size);
                list.add(o);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }

    public Order getUserInfor(int uid, int oid) {
        try {
            String sql = "SELECT st.status_order_id, st.status_name, o.order_id,o.fullName,o.mobile ,o.address,o.total_cost, o.status_order, o.orderDate, o.saler_id FROM user u , `order` o, status_order st where o.userId = u.userId and o.status_order = st.status_order_id and u.userId =? and o.order_id=?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, uid);
            stm.setInt(2, oid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                StatusOrder sa = new StatusOrder(rs.getInt("status_order_id"), rs.getString("status_name"));
                Order u = new Order(rs.getInt("order_id"), rs.getDate("orderDate"), rs.getDouble("total_cost"), rs.getString("fullName"), rs.getString("mobile"), rs.getString("address"), rs.getInt("saler_id"), null, sa, null);
                return u;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean UpdateStatus(int uid) {
        boolean check = true;
        try {
            String sql = "update shoes_shop_online.order\n"
                    + "set status_order = 5\n"
                    + "where order_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, uid);
            stm.executeUpdate();

        } catch (Exception e) {
            check = false;
        }

        return check;
    }

    public ArrayList<Order> searchOrderDetailByUid(int uid, int oid, String search) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select o.order_id, pi.images ,p.product_name, ot.quantity,o.orderDate, p.sale_prices,s.status_name from shoes_shop_online.order o ,user u,  status_order s , order_detail ot , product p , products_images pi\n"
                    + "         where u.userId = o.userId and status_order_id = o.status_order and o.order_id =ot.order_id \n"
                    + "         and p.product_id = ot.product_id and pi.product_id = p.product_id and u.userId =? and o.order_id =? and p.product_name like?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, uid);
            stm.setInt(2, oid);
            stm.setString(3, "%" + search + "%");
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Product p = new Product(rs.getString(3), rs.getFloat(6));
                ProductImages pi = new ProductImages(1, rs.getString(2), p);
                StatusOrder s = new StatusOrder(2, rs.getString(7), pi);
                OrderDetail ot = new OrderDetail(3, rs.getInt(4), s);
                Order o = new Order(rs.getInt(1), rs.getDate(5), ot);
                list.add(o);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static void main(String[] args) {
        OrderDAO dao = new OrderDAO();
        ArrayList<Order> list = dao.getOrderDetailByUid(12, 65);
        for (Order order : list) {
            System.out.println(order.size.getSizeName());
        }
    }
}
