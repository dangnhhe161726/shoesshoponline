/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Blog;
import model.Category;
import model.CategoryBlog;
import model.User;

/**
 *
 * @author quyde
 */
public class PostDAO extends DBcontext {
// l?y tât c? post

    public ArrayList<Blog> getAllPostandAuthor() {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "select b.blog_id, b.thumbnail, b.title, cb.categoryBlog_name, u.fullName, b.status, u.userId from blog b , user u , category_blog cb \n"
                + "where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getString("categoryBlog_name"));
                Blog b = new Blog(rs.getInt("blog_id"), rs.getString("title"), rs.getString("thumbnail"), rs.getBoolean("status"), u, cb);

                list.add(b);

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    // lay post theo id : detail

    public Blog getPostByID(String pid) {

        String sql = "select b.blog_id, b.title, u.userId, b.updated_date, b.content, b.thumbnail, b.brief_infor, b.status, u.fullName, cb.categoryBlog_name, cb.categoryBlog_id from blog b , user u , category_blog cb \n"
                + "                where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and b.blog_id =?;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, pid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getInt("categoryBlog_id"), rs.getString("categoryBlog_name"));
                Blog b = new Blog(rs.getInt("blog_id"), rs.getString("title"), rs.getDate("updated_date"), rs.getString("content"), rs.getString("thumbnail"), rs.getString("brief_infor"), rs.getBoolean("status"), u, cb);

                return b;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }
    //deletet post

    public void deletePost(String bid) {

        try {
            String sql = "delete from  shoes_shop_online.blog where blog_id=" + bid;
            PreparedStatement statement = connection.prepareCall(sql);
            statement.executeUpdate();

        } catch (Exception e) {
        }
    }

    public void updatePost(String title, String author_id, String content, String thumbnail, String brief_infor, String categoryBlog_id, String status, String bid) {
        String sql = "UPDATE `shoes_shop_online`.`blog` SET `title` = ?, `author_id` = ?, `updated_date` = NOW(), `content` = ?, `thumbnail` = ?, `brief_infor` = ?, `categoryBlog_id` = ?, `status` = ? WHERE (`blog_id` = ?);";

        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, title);
            statement.setString(2, author_id);
            statement.setString(3, content);
            statement.setString(4, thumbnail);
            statement.setString(5, brief_infor);
            statement.setString(6, categoryBlog_id);
            statement.setString(7, status);
            statement.setString(8, bid);

            statement.executeUpdate();
        } catch (Exception e) {
        }

    }

    public void addPost(String title, String author_id, String content, String thumbnail, String brief_infor, String categoryBlog_id, String status) {

        try {
            String sql = "INSERT INTO `shoes_shop_online`.`blog` ( `title`, `author_id`, `updated_date`, `content`, `thumbnail`, `brief_infor`, `categoryBlog_id`, `status`) VALUES (?, ?, now(), ?, ?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, title);
            statement.setString(2, author_id);
            statement.setString(3, content);
            statement.setString(4, thumbnail);
            statement.setString(5, brief_infor);
            statement.setString(6, categoryBlog_id);
            statement.setString(7, status);
            statement.executeUpdate();
        } catch (Exception e) {
        }

    }

    public ArrayList<CategoryBlog> getAllCategoryBlog() {
        ArrayList<CategoryBlog> categoryblogs = new ArrayList<>();
        String sql = " SELECT * FROM shoes_shop_online.category_blog;";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
//            
            while (rs.next()) {
                CategoryBlog cb = new CategoryBlog();
                cb.setCategoryBlogId(rs.getInt("categoryBlog_id"));
                cb.setCategoryBlogName(rs.getString("categoryBlog_name"));
                cb.setStatus(rs.getBoolean("status"));
                categoryblogs.add(cb);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categoryblogs;
    }

    public ArrayList<Blog> getAllPostByCateId(String id) {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "select b.blog_id, b.thumbnail, b.title, cb.categoryBlog_name, u.fullName, b.status, u.userId,cb.categoryBlog_id  from blog b , user u , category_blog cb \n"
                + "                where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and cb.categoryBlog_id = ?;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getString("categoryBlog_name"));
                Blog b = new Blog(rs.getInt("blog_id"), rs.getString("title"), rs.getString("thumbnail"), rs.getBoolean("status"), u, cb);
                list.add(b);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<Blog> getAllPostByAuthorId(int id) {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "select b.blog_id, b.thumbnail, b.title, cb.categoryBlog_name, u.fullName, b.status, u.userId,cb.categoryBlog_id  from blog b , user u , category_blog cb \n"
                + "                where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and u.userId = ?;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getString("categoryBlog_name"));
                Blog b = new Blog(rs.getInt("blog_id"), rs.getString("title"), rs.getString("thumbnail"), rs.getBoolean("status"), u, cb);
                list.add(b);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<User> getallUserRoleMarketing() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM shoes_shop_online.user where role_id = 3;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                list.add(u);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
