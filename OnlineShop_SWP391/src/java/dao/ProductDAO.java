/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.Category;
import model.FeedBack;
import model.Product;
import model.ProductImages;
import model.RatedStar;
import model.Size;
import model.User;
import java.time.LocalDate;

/**
 *
 * @author Admin
 */
public class ProductDAO extends DBcontext {

    public ArrayList<Category> getallCategory() {
        ArrayList<Category> list = new ArrayList<>();
        String sql = "SELECT * FROM category;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Category ca = new Category(rs.getInt(1), rs.getString(2), rs.getBoolean(3));
                list.add(ca);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<Product> getProduct() {
        ArrayList<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM product p\n"
                + "JOIN products_images pI ON p.product_id = pI.product_id\n"
                + "ORDER BY p.update_date DESC LIMIT 12;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi);
                list.add(p);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Product> getAllProduct() {
        ArrayList<Product> list = new ArrayList<>();
        String sql = "SELECT p.*, c.category_name, pi.products_images_id, pi.images, SUM(s.quantity) as quantity\n"
                + "FROM product p\n"
                + "JOIN category c ON p.category_id = c.category_id\n"
                + "JOIN products_images pi ON p.product_id = pi.product_id\n"
                + "JOIN size s ON p.product_id = s.product_id\n"
                + "GROUP BY p.product_id, pi.products_images_id;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                Category ca = new Category(rs.getInt("category_id"), rs.getString("category_name"));
                Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi, ca, rs.getInt("quantity"));
                list.add(p);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //search and sort product by price
    public ArrayList<Product> getProductByPrice(int check) {
        try {
            String sql = "";
            String start = "";
            String end = "";
            ArrayList<Product> list = new ArrayList<>();

            if (check == 4) {
                start = "2000000";
                sql = "SELECT * FROM product p, products_images pi  WHERE p.product_id = pi.product_id AND p.sale_prices >= ? ORDER BY  p.sale_prices ASC;";
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, start);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                    Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi);
                    list.add(p);
                }
                return list;
            } else {
                sql = "SELECT * FROM product p, products_images pi  WHERE p.product_id = pi.product_id AND p.sale_prices >= ? AND p.sale_prices <= ? ORDER BY  p.sale_prices ASC;";
                PreparedStatement ps = connection.prepareStatement(sql);
                if (check == 0) {
                    end = " 500000";
                }
                if (check == 1) {
                    start = "500000";
                    end = " 1000000";
                }
                if (check == 2) {
                    start = "1000000";
                    end = " 1500000";
                }
                if (check == 3) {
                    start = "1500000";
                    end = " 2000000";
                }
                ps.setString(1, start);
                ps.setString(2, end);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                    Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi);
                    list.add(p);
                }
                return list;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //search and sort product by size
    public ArrayList<Product> getProductBySize(int check) {
        try {

            ArrayList<Product> list = new ArrayList<>();

            String sql = "SELECT * FROM product p, products_images pi, size s WHERE p.product_id = pi.product_id AND s.product_id = p.product_id AND s.size = ?  ORDER BY  p.sale_prices ASC;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, check);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi);
                list.add(p);
            }
            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Product getProductById(String id) {
        try {
            // Create SQL String
            String sql = "SELECT * FROM Product, Category WHERE Product.category_id = Category.category_id AND Product.category_id= ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Category c = new Category(rs.getInt("category_id"), rs.getString("category_name"), rs.getBoolean("status"));
                Product product = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), c);
                return product;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getTotalProduct(String cateId, String search) {
        String sql = "";
        try {
            if (cateId != null && search == null) {
                sql = "SELECT count(*)\n"
                        + "FROM Product p \n"
                        + "JOIN category c ON p.category_id = c.category_id \n"
                        + "WHERE c.category_id = ? and p.status = 1";
                PreparedStatement stm = connection.prepareStatement(sql);
                stm.setString(1, cateId);
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    return rs.getInt(1);
                }
            }
            if (cateId == null && search == null) {
                sql = "SELECT count(*)\n"
                        + "FROM Product p \n"
                        + "JOIN category c ON p.category_id = c.category_id WHERE p.status = 1 ";
                PreparedStatement stm = connection.prepareStatement(sql);
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    return rs.getInt(1);
                }
            }
            if (cateId == null && search != null) {
                sql = "SELECT COUNT(*)\n"
                        + "FROM Product p \n"
                        + "JOIN products_images pI ON pI.product_id = p.product_id\n"
                        + "WHERE p.product_name like ? and p.status = 1;";
                PreparedStatement stm = connection.prepareStatement(sql);
                stm.setString(1, "%" + search + "%");
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalFeedBackProduct(String idP) {
        try {
            String sql = "SELECT count(*)\n"
                    + "FROM Product p, feedback f\n"
                    + "WHERE p.product_id = f.product_id and p.product_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, idP);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public ArrayList<Product> getallProductbyCateId(String id) {
        ArrayList<Product> list = new ArrayList<>();
        try {
            // Create SQL String
            String sql = "SELECT p.*, c.category_name, pi.products_images_id, pi.images, SUM(s.quantity) as quantity\n"
                    + "FROM product p\n"
                    + "JOIN category c ON p.category_id = c.category_id\n"
                    + "JOIN products_images pi ON p.product_id = pi.product_id\n"
                    + "JOIN size s ON p.product_id = s.product_id\n"
                    + "WHERE c.category_id = ?\n"
                    + "GROUP BY p.product_id, pi.products_images_id;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                Category ca = new Category(rs.getInt("category_id"), rs.getString("category_name"));
                Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi, ca, rs.getInt("quantity"));
                list.add(p);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Product getProductDetail(String id) {

        try {
            // Create SQL String
            String sql = "SELECT * FROM product p\n"
                    + "join products_images pI on p.product_id = pI.product_id\n"
                    + "join category c on c.category_id = p.category_id\n"
                    + "where p.product_id = ? ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                Category ca = new Category(rs.getInt("category_id"), rs.getString("category_name"), rs.getBoolean("status"));
                Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi, ca);
                return p;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public ArrayList<Product> searchByName(String txtSearch, int pageS) {
        ArrayList<Product> list = new ArrayList<>();
        try {
            // Create SQL String
            String sql = "SELECT *\n"
                    + "FROM Product p \n"
                    + "JOIN products_images pI ON pI.product_id = p.product_id\n"
                    + "WHERE p.product_name like ?\n"
                    + "LIMIT ?,6;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + txtSearch + "%");
            stm.setInt(2, (pageS - 1) * 6);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi);
                list.add(p);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Product> pagingForProduct(int pageS, String pageSize, String cateId) {
        ArrayList<Product> list = new ArrayList<>();
        String sql = "";
        try {
            if (pageSize == null && cateId == null) {
                sql = "SELECT *\n"
                        + "FROM Product p \n"
                        + "JOIN products_images pI ON pI.product_id = p.product_id\n"
                        + "LIMIT ?,6";

                PreparedStatement stm = connection.prepareStatement(sql);
                stm.setInt(1, (pageS - 1) * 6);
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                    Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi);
                    list.add(p);
                }
                return list;
            }
            if (pageSize != null && cateId == null) {
                int a = Integer.parseInt(pageSize);
                sql = "SELECT *\n"
                        + "FROM Product p \n"
                        + "JOIN products_images pI ON pI.product_id = p.product_id\n"
                        + "LIMIT ?,?;";
                PreparedStatement stm = connection.prepareStatement(sql);
                stm.setInt(1, (pageS - 1) * a);
                stm.setInt(2, a);
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                    Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi);
                    list.add(p);
                }
                return list;
            }
            if (pageSize != null && cateId != null) {
                int a = Integer.parseInt(pageSize);
                sql = "SELECT *\n"
                        + "FROM Product p \n"
                        + "JOIN category c ON p.category_id = c.category_id \n"
                        + "JOIN products_images pI ON pI.product_id = p.product_id\n"
                        + "WHERE c.category_id = ?\n"
                        + "LIMIT ?,?;";
                PreparedStatement stm = connection.prepareStatement(sql);
                stm.setString(1, cateId);
                stm.setInt(2, (pageS - 1) * Integer.parseInt(pageSize));
                stm.setInt(3, a);
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    ProductImages pi = new ProductImages(rs.getInt("products_images_id"), rs.getString("images"));
                    Category ca = new Category(rs.getInt("category_id"), rs.getString("category_name"), rs.getBoolean("status"));
                    Product p = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getDouble("original_prices"), rs.getDouble("sale_prices"), rs.getString("product_details"), rs.getString("brief_infor"), rs.getBoolean("status"), rs.getDate("update_date"), pi, ca);
                    list.add(p);
                }
                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<ProductImages> getAllImageProductbyID(String id) {
        ArrayList<ProductImages> list = new ArrayList<>();
        try {
            // Create SQL String
            String sql = "SELECT pmi.products_many_images_id, pmi.images FROM product_many_img pmi, products_images pi\n"
                    + "where pmi.products_images_id = pi.products_images_id\n"
                    + "and pi.product_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                ProductImages pi = new ProductImages(rs.getInt("products_many_images_id"), rs.getString("images"));
                list.add(pi);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Size> getSizeProductById(String id) {
        ArrayList<Size> list = new ArrayList<>();
        try {
            // Create SQL String
            String sql = "select * from size s\n"
                    + "where s.product_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Size sz = new Size(rs.getInt("size_id"), rs.getInt("size"), rs.getBoolean("status"), rs.getInt("quantity"));
                list.add(sz);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Size getOneSizeProductById(String pid, String size) {
        try {
            // Create SQL String
            String sql = "select * from size s\n"
                    + "where s.product_id = ? and s.size = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, pid);
            stm.setString(2, size);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                Size sz = new Size(rs.getInt("size_id"), rs.getInt("size"), rs.getBoolean("status"), rs.getInt("quantity"));
                return sz;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<FeedBack> getFeedbackProductById(String id, int page) {
        ArrayList<FeedBack> list = new ArrayList<>();
        try {
            // Create SQL String
            String sql = "SELECT f.feedBack_id, f.fullName as fullNameFeedback, f.rated_star, f.feedback,f.date , u.fullName, u.userId, u.avatar, f.status, f.image FROM feedback f, user u , product p\n"
                    + "where f.product_id = p.product_id and f.userId = u.userId and p.product_id = ?\n"
                    + "limit ?,5;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            stm.setInt(2, (page - 1) * 5);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("avatar"), rs.getInt("status"));
                FeedBack fe = new FeedBack(rs.getInt("feedBack_id"), rs.getString("fullNameFeedback"), rs.getInt("rated_star"), rs.getString("feedback"), rs.getString("image"), rs.getBoolean("status"), rs.getDate("date"), user);
                list.add(fe);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<RatedStar> getPercentRatedStar(String id) {
        ArrayList<RatedStar> list = new ArrayList<>();
        try {
            // Create SQL String
            String sql = "SELECT rated_star,(COUNT(*) / (SELECT COUNT(rated_star) FROM feedback WHERE rated_star IS NOT NULL and product_id = ?)) * 100 AS vote_percentage\n"
                    + "FROM feedback\n"
                    + "WHERE rated_star IS NOT NULL and product_id = ?\n"
                    + "GROUP BY rated_star\n"
                    + "ORDER BY rated_star desc;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, id);
            stm.setString(2, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                RatedStar ra = new RatedStar(rs.getInt("rated_star"), rs.getInt("vote_percentage"));
                list.add(ra);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insertFeedbackProduct(String img, String title, String rating, String feedback, String productId, String userId, String date) {
        String sql = "INSERT INTO `feedback` (`fullName`, `rated_star`, `feedback`, `status`, `product_id`, `userId`, `date`, `image`) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement statement = connection.prepareCall(sql);

            statement.setString(1, title);
            statement.setString(2, rating);
            statement.setString(3, feedback);
            statement.setString(4, "1");
            statement.setString(5, productId);
            statement.setString(6, userId);
            statement.setString(7, date);
            statement.setString(8, img);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateSubImgOfProduct(String subImg, String subImgId) {
        String sql = "UPDATE `product_many_img` SET `images` = ? WHERE (`products_many_images_id` = ?);";

        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, subImg);
            statement.setString(2, subImgId);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void insertSubImgOfProduct(String imgId, String img) {
        String sql = "INSERT INTO `product_many_img` (`products_images_id`, `images`) VALUES (?, ?);";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, imgId);
            statement.setString(2, img);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateSizeOfProduct(String quantity, String sizeName, String pid) {
        String sql = "UPDATE `size` SET `quantity` = ? WHERE (size = ? and product_id = ?);";

        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, quantity);
            statement.setString(2, sizeName);
            statement.setString(3, pid);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertSizeOfProduct(String sizeName, String sizeQuantity, String pid) {
        String sql = "INSERT INTO `size` (`size`, `status`, `quantity`, `product_id`) VALUES (?, '1', ?, ?);";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, sizeName);
            statement.setString(2, sizeQuantity);
            statement.setString(3, pid);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateStatusOfSizeProduct(int quantity, int sizeName, int pid) {
        String sql = "";
        try {
            if (quantity == 0) {
                sql = "UPDATE `size` SET `status` = 0 WHERE (size = ? and product_id = ? and quantity = ?);";

            } else {
                sql = "UPDATE shoes_shop_online.`size` SET `status` = 1 WHERE `size` = ? AND `product_id` = ? AND ? > 0;";
            }

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setInt(1, sizeName);
            statement.setInt(2, pid);
            statement.setInt(3, quantity);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProductDetail(String pName, String oprice, String sprice, String pdetail, String binfor, String status, String ca, String date, String pid) {
        String sql = "UPDATE `product` SET `product_name` = ?, `original_prices` = ?, `sale_prices` = ?, `product_details` = ?, `brief_infor` = ?, `status` = ?, `category_id` = ?, `update_date` = ? WHERE (`product_id` = ?);";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, pName);
            statement.setString(2, oprice);
            statement.setString(3, sprice);
            statement.setString(4, pdetail);
            statement.setString(5, binfor);
            statement.setString(6, status);
            statement.setString(7, ca);
            statement.setString(8, date);
            statement.setString(9, pid);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateImgProductDetail(String pid, String img) {
        String sql = "UPDATE `products_images` SET `images` = ? WHERE (product_id= ?);";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, img);
            statement.setString(2, pid);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int addProductDetail(String pname, double oprice, double sprice, String pdetail, String binfor, int ca, int size, int quantity, String mainImg, String subImg) {
        Date currentDate = new Date();
        String formattedDate = formatDate(currentDate, "yyyy-MM-dd HH:mm:ss");
        int pid = 0;
        try {
            //add product to table product on database
            String sql = "INSERT INTO `product` (`product_name`, `original_prices`, `sale_prices`, `product_details`, `brief_infor`, `status`, `category_id`, `update_date`) VALUES (?, ?, ?, ?, ?, '1', ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, pname);
            statement.setDouble(2, oprice);
            statement.setDouble(3, sprice);
            statement.setString(4, pdetail);
            statement.setString(5, binfor);
            statement.setInt(6, ca);
            statement.setString(7, formattedDate);
            statement.executeUpdate();
            //Get the id of the newly added product 
            String sql1 = "SELECT product_id FROM `product` ORDER BY product_id DESC LIMIT 1;";
            PreparedStatement statement1 = connection.prepareStatement(sql1);
            ResultSet rs1 = statement1.executeQuery();
            //add size and main image of product
            if (rs1.next()) {
                pid = rs1.getInt("product_id");
                //add size of product
                String sql2 = "INSERT INTO `size` (`size`, `status`, `quantity`, `product_id`) VALUES (?, '1', ?, ?);";
                PreparedStatement statement2 = connection.prepareCall(sql2);
                statement2.setInt(1, size);
                statement2.setInt(2, quantity);
                statement2.setInt(3, pid);
                statement2.executeUpdate();
                //add main image of product
                String sql3 = "INSERT INTO `products_images` (`product_id`, `images`) VALUES (?, ?);";
                PreparedStatement statement3 = connection.prepareCall(sql3);
                statement3.setInt(1, pid);
                statement3.setString(2, mainImg);
                statement3.executeUpdate();
            }
            //Get the id of the newly added product image 
            String sql4 = "SELECT products_images_id FROM `products_images` ORDER BY products_images_id DESC LIMIT 1;";
            PreparedStatement statement4 = connection.prepareStatement(sql4);
            ResultSet rs4 = statement4.executeQuery();
            if (rs4.next()) {
                // add sub image of product
                int pImgId = rs4.getInt("products_images_id");
                String sql5 = "INSERT INTO `product_many_img` (`products_images_id`, `images`) VALUES (?, ?);";
                PreparedStatement statement5 = connection.prepareCall(sql5);
                statement5.setInt(1, pImgId);
                statement5.setString(2, subImg);
                statement5.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return pid;
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static void main(String[] args) {
        ProductDAO dao = new ProductDAO();
        ArrayList<FeedBack> list = dao.getFeedbackProductById("55", 1);
        for (FeedBack feedBack : list) {
            System.out.println(feedBack.getUser().getAvatar());
        }
    }
}
