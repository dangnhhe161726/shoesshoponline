/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.CategoryBlog;
import model.Slider;
import model.User;

/**
 *
 * @author quyde
 */
public class SliderDAO extends DBcontext {

    public ArrayList<Slider> getAllSlider() {

        ArrayList<Slider> list = new ArrayList<>();
        String sql = "SELECT  slider_id, slider_title, slider_image, backlink, status FROM slider ";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Slider s = new Slider(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5));
                list.add(s);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public Slider getSliderByID(String sid) {

        String sql = "select u.userId, u.fullName, s.slider_id, s.slider_title, s.slider_image, s.backlink,s.note,s.status from slider s, user u\n"
                + " where s.updated_by = u.userId and s.slider_id = ?;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, sid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                Slider s = new Slider(rs.getInt("slider_id"), rs.getString("slider_title"), rs.getString("slider_image"), rs.getString("backlink"), rs.getString("note"), rs.getBoolean("status"), u);
                return s;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public void deleteSlider(String sid) {

        try {
            String sql = "delete from slider where slider_id=" + sid;
            PreparedStatement statement = connection.prepareCall(sql);
            statement.executeUpdate();

        } catch (Exception e) {
        }
    }

    public void updateSlider(String title, String image, String backlink, String note, String status, String userId, String sid) {
        String sql = "UPDATE `slider` SET `slider_title` = ?, `slider_image` = ?, `backlink` = ?, `note` = ?, `status` = ?, `updated_by` = ? WHERE (`slider_id` = ?);";

        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, title);
            statement.setString(2, image);
            statement.setString(3, backlink);
            statement.setString(4, note);
            statement.setString(5, status);
            statement.setString(6, userId);
            statement.setString(7, sid);
            statement.executeUpdate();
        } catch (Exception e) {
        }

    }

    public void addSlider(String title, String img, String backlink, String note, String status, String updatedby) {

        String sql = "INSERT INTO `shoes_shop_online`.`slider` (`slider_title`, `slider_image`, `backlink`, `note`, `status`, `updated_by`) \n"
                + "VALUES (?,?,?,?,?,?);";

        try {

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, title);
            statement.setString(2, img);
            statement.setString(3, backlink);
            statement.setString(4, note);
            statement.setString(5, status);
            statement.setString(6, updatedby);
            statement.executeUpdate();
        } catch (Exception e) {
        }

    }

    public ArrayList<Slider> getSliderByStatus(String status) {
        ArrayList<Slider> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM shoes_shop_online.slider WHERE status = ?";

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, status);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                Slider s = new Slider(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(6));

                list.add(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        SliderDAO dao = new SliderDAO();
        ArrayList<Slider> list = dao.getAllSlider();
        for (Slider slider : list) {
            System.out.println(slider.getBacklink() + " ?" + slider.getImg() + "/" + slider.isStatus());
        }

    }
}
