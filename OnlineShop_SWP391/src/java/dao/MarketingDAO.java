/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import com.oracle.wls.shaded.org.apache.bcel.generic.AALOAD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.FeedBack;
import model.Order;
import model.OrderDetail;
import model.Product;
import model.ProductImages;
import model.User;

/**
 *
 * @author ADMIN
 */
public class MarketingDAO extends DBcontext {

    public ArrayList<User> getAllCustomer() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM shoes_shop_online.user where role_id=4";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                User u = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("email"), rs.getString("password"), rs.getString("avatar"), rs.getBoolean("gender"), rs.getString("mobile"), rs.getString("address"), rs.getInt("status"));
                //  new User(rs.getInt("userId"),rs.getString("fullName"),rs.getString("password"),rs.getString("avatar"), rs.getBoolean("gender"), rs.getString("email"), rs.getString("mobile"), rs.getString("address"), rs.getDate("DateOfBirth"),rs.getInt("status"));
                list.add(u);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void insertCustomer(String name, String email, String password, String address, String mobile, String gender) {
        try {
            String sql = "INSERT INTO shoes_shop_online.user (fullName, email, password,address,mobile,gender,status,role_id )VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, password);
            statement.setString(4, address);
            statement.setString(5, mobile);
            statement.setString(6, gender);
            statement.setInt(7, 1);
            statement.setInt(8, 4);
            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    public User GetCustomer(String id) {
        try {
            String sql = "SELECT * FROM shoes_shop_online.user WHERE userId=? and role_id = 4 ";
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("email"), rs.getString("password"), rs.getString("avatar"), rs.getBoolean("gender"), rs.getString("mobile"), rs.getString("address"), rs.getInt("status"));
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void EditCustomer(String status, String uid) {
        try {
            String sql = "UPDATE `user` SET `status` = ? where userId=? and  role_id = 4 ";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, uid);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<User> getCustomerbyid(String search) {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = " SELECT *\n"
                    + "FROM shoes_shop_online.user\n"
                    + "WHERE role_id = 4\n"
                    + "  AND (fullName like ? OR email like ? OR mobile LIKE ? OR status like ?)";

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, "%" + search + "%");
            statement.setString(2, "%" + search + "%");
            statement.setString(3, "%" + search + "%");
            statement.setString(4, "%" + search + "%");

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("email"), rs.getString("password"), rs.getString("avatar"), rs.getBoolean("gender"), rs.getInt("status"));
                list.add(user);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<User> pagingBlog(int index) {
        ArrayList<User> list = new ArrayList<>();
        String sql = " SELECT * FROM user\n"
                + "WHERE role_id = 4\n"
                + "ORDER BY userId\n"
                + "LIMIT 2 OFFSET ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 2);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("email"), rs.getString("password"), rs.getString("avatar"), rs.getBoolean("gender"), rs.getString("mobile"), rs.getString("address"), rs.getInt("status"));
                list.add(user);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<User> getCustomerbystatus(String search) {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM shoes_shop_online.user WHERE role_id = 4 AND status LIKE ?";

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, "%" + search + "%");

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = new User(
                        rs.getInt("userId"),
                        rs.getString("fullName"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("avatar"),
                        rs.getBoolean("gender"),
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getInt("status")
                );
                list.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<User> sort(String p) {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM shoes_shop_online.user WHERE role_id = 4 ORDER BY " + p + " ASC  ";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("email"), rs.getString("password"),
                        rs.getString("avatar"), rs.getBoolean("gender"), rs.getString("mobile"), rs.getString("address"), rs.getInt("status"));
                list.add(u);
            }
            return list;
        } catch (Exception e) {
             e.printStackTrace(); 
        }
        return null;
    }

    public boolean checkPhone(String phone) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM shoes_shop_online.user where mobile like ?");
            ps.setString(1, phone);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean checkEmail(String email) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM shoes_shop_online.user where email=?");
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public ArrayList<User> getCustomerbyFeedBackAndProduct1() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT u.userId, u.fullName, u.email, u.mobile,p.product_name, f.feedback, f.status, f.rated_star, f.feedBack_id FROM shoes_shop_online.user u"
                + " JOIN shoes_shop_online.feedback f ON u.userId = f.userId "
                + "JOIN shoes_shop_online.product p ON f.product_id = p.product_id;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Product product = new Product(rs.getString(5));
                FeedBack feedback = new FeedBack(rs.getString(6), rs.getBoolean(7), rs.getInt(8));
                User u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), String.valueOf(rs.getString(9)), product, feedback);
                list.add(u);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<User> getCustomerFeedBackAndProductbysearch(String search) {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = "SELECT u.userId, u.fullName, u.email, u.mobile, p.product_name, f.feedback, f.status, f.rated_star FROM shoes_shop_online.user u JOIN shoes_shop_online.feedback f ON u.userId = f.userId JOIN shoes_shop_online.product p "
                    + "ON f.product_id = p.product_id WHERE u.fullName LIKE ? OR p.product_name LIKE ? OR f.feedback LIKE ?";

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, "%" + search + "%");
            statement.setString(2, "%" + search + "%");
            statement.setString(3, "%" + search + "%");

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Product product = new Product(rs.getString(5));
                FeedBack feedback = new FeedBack(rs.getString(6), rs.getBoolean(7), rs.getInt(8));
                User u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), product, feedback);
                list.add(u);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<User> getCustomerFeedBackAndProductbyStatus(String status) {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = "     SELECT u.userId, u.fullName, u.email, u.mobile, p.product_name, f.feedback, f.status, f.rated_star FROM shoes_shop_online.user u JOIN shoes_shop_online.feedback f ON u.userId = f.userId JOIN shoes_shop_online.product p \n"
                    + "                ON f.product_id = p.product_id WHERE f.status=?";

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, status);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                Product product = new Product(rs.getString(5));
                FeedBack feedback = new FeedBack(rs.getString(6), rs.getBoolean(7), rs.getInt(8));
                User u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), product, feedback);
                list.add(u);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<User> getCustomerFeedBackAndProductbyStar(String Star) {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = "	SELECT u.userId, u.fullName, u.email, u.mobile, p.product_name, f.feedback, f.status, f.rated_star\n"
                    + "	 FROM shoes_shop_online.user u JOIN shoes_shop_online.feedback f ON u.userId = f.userId\n"
                    + "	 JOIN shoes_shop_online.product p ON f.product_id = p.product_id where f.rated_star=?";

            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, Star);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                Product product = new Product(rs.getString(5));
                FeedBack feedback = new FeedBack(rs.getString(6), rs.getBoolean(7), rs.getInt(8));
                User u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), product, feedback);
                list.add(u);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<User> getCustomerFeedBackAndProductbyStatusAndStar(String status, String star) {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = "SELECT u.userId, u.fullName, u.email, u.mobile, p.product_name, f.feedback, f.status, f.rated_star\n"
                    + "FROM shoes_shop_online.user u\n"
                    + "JOIN shoes_shop_online.feedback f ON u.userId = f.userId\n"
                    + "JOIN shoes_shop_online.product p ON f.product_id = p.product_id\n"
                    + "WHERE f.status = ? and f.rated_star = ?";

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, status);
            statement.setString(2, star);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Product product = new Product(rs.getString(5));
                FeedBack feedback = new FeedBack(rs.getString(6), rs.getBoolean(7), rs.getInt(8));
                User u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), product, feedback);
                list.add(u);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void EditStatusFeedBack(String status, int uid) {
        try {
            String sql = "UPDATE shoes_shop_online.feedback  SET status = ? where feedBack_id=? ";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, status);
            preparedStatement.setInt(2, uid);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User GetCustomerByFeedback(String id, String idf) {
        try {
            String sql = "SELECT u.userId, u.fullName, u.email, u.mobile,p.product_name, f.feedback, f.status, f.rated_star, f.feedBack_id FROM shoes_shop_online.user u"
                    + " JOIN shoes_shop_online.feedback f ON u.userId = f.userId "
                    + "JOIN shoes_shop_online.product p ON f.product_id = p.product_id where u.userId='" + id + "' and f.feedBack_id='" + idf + "'";
            PreparedStatement statement = connection.prepareCall(sql);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Product product = new Product(rs.getString(5));
                FeedBack feedback = new FeedBack(rs.getString(6), rs.getBoolean(7), rs.getInt(8));
                User u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), String.valueOf(rs.getString(9)), product, feedback);

                return u;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getTotalCustomer() {
        String sql = "select count(*) from user where role_id=4";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalProduct() {
        String sql = "select count(*) from product";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalorder() {
        String sql = "select count(*) from  shoes_shop_online.order;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalfeedback() {
        String sql = "select count(*) from feedback";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalProfit() {
        String sql = "select sum(total_cost) from  shoes_shop_online.order";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public ArrayList<Product> getTopSelling() {
        ArrayList<Product> list = new ArrayList<>();
        try {
            String sql = "SELECT p.product_id, p.product_name, SUM(o.quantity) AS total_quantity\n"
                    + "FROM order_detail o\n"
                    + "JOIN product p ON o.product_id = p.product_id\n"
                    + "GROUP BY p.product_id,p.product_name\n"
                    + "ORDER BY total_quantity DESC\n"
                    + "LIMIT 5;";

            PreparedStatement statement = connection.prepareCall(sql);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                //  OrderDetail o=new OrderDetail(rs.getInt(3));
                Product P = new Product(rs.getInt(1), rs.getString(2), Integer.valueOf(rs.getInt(3)));

                list.add(P);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int getTotalOrderLast7day(String d1, String d2) {
        String sql = "SELECT  COUNT(*) AS total_orders\n"
                + "FROM shoes_shop_online.order\n"
                + "WHERE orderDate BETWEEN ? - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, d1);
            st.setString(2, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalUserLast7day(String d1, String d2) {
        String sql = "SELECT COUNT(DISTINCT o.userId) AS total_users\n"
                + "FROM shoes_shop_online.order o JOIN shoes_shop_online.user u ON o.userId = u.userId\n"
                + "WHERE orderDate BETWEEN ? - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY AND u.role_id = 4;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, d1);
            st.setString(2, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalfeedbackLast7day(String d1, String d2) {
        String sql = " SELECT COUNT(*) AS total_feedbacks\n"
                + "FROM shoes_shop_online.user c\n"
                + "JOIN shoes_shop_online.feedback u ON u.userId = c.userId\n"
                + "JOIN shoes_shop_online.product p ON u.product_id = p.product_id\n"
                + "WHERE date BETWEEN ?- INTERVAL 7 DAY AND ? + INTERVAL 7 DAY\n"
                + "  AND c.role_id = 4\n"
                + "  AND p.product_id IS NOT NULL\n"
                + "  AND c.userId IS NOT NULL;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, d1);
            st.setString(2, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalcostlast7day() {
        String sql = "SELECT SUM(total_cost) AS total_cost\n"
                + "  FROM shoes_shop_online.order\n"
                + "   WHERE orderDate BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() + INTERVAL 7 DAY ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
