/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import model.Category;
import model.Order;
import model.OrderDetail;
import model.Product;
import model.ProductImages;
import model.StatusOrder;
import model.User;

/**
 *
 * @author pc
 */
public class OrderSaleDAO extends DBcontext {

    public ArrayList<Order> getallOrders() {
        ArrayList<Order> list = new ArrayList<>();
        String sql = "SELECT distinct o.order_id,o.orderDate,o.fullName , p.product_name ,o.total_cost, s.status_name FROM shoes_shop_online.order o , product p, user u, status_order s, order_detail ot  where u.userId = o.userId and status_order_id = o.status_order and o.order_id =ot.order_id \n"
                + "     and p.product_id = ot.product_id ";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getString(4), 0);
                StatusOrder s = new StatusOrder(3, rs.getString(6), p);
                Order o = new Order( rs.getInt(1),rs.getDate(2),rs.getDouble(5),rs.getString(3), s);
                list.add(o);

            }
            ArrayList<Order> allorder = checkDuplicateOrder(list);
            return allorder;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<User> getallSale() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "Select * from user u , role r where r.role_id =u.role_id and r.role_id =2";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt(1), rs.getString(2));
                list.add(u);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getTotalUser() {
        String sql = "select count(*) from user where status = 1;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalorder() {
        String sql = "select count(*) from  shoes_shop_online.order;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public ArrayList<Product> getTopSelling() {
        ArrayList<Product> list = new ArrayList<>();
        try {
            String sql = "SELECT p.product_id, p.product_name, SUM(o.quantity) AS total_quantity\n"
                    + "FROM order_detail o\n"
                    + "JOIN product p ON o.product_id = p.product_id\n"
                    + "GROUP BY p.product_id,p.product_name\n"
                    + "ORDER BY total_quantity DESC\n"
                    + "LIMIT 5;";

            PreparedStatement statement = connection.prepareCall(sql);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                //  OrderDetail o=new OrderDetail(rs.getInt(3));
                Product P = new Product(rs.getInt(1), rs.getString(2), Integer.valueOf(rs.getInt(3)));

                list.add(P);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<StatusOrder> getallStatus() {
        ArrayList<StatusOrder> list = new ArrayList<>();
        String sql = "SELECT * FROM status_order;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                StatusOrder s = new StatusOrder(rs.getInt(1), rs.getString(2));
                list.add(s);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<Order> getStatusByID(int sid) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select o.order_id, o.orderDate,o.fullName,p.product_name, o.total_cost,s.status_name,s.status_order_id from shoes_shop_online.order o ,user u,  status_order s , order_detail ot , product p\n"
                    + "                    where u.userId = o.userId and status_order_id = o.status_order \n"
                    + "                    and o.order_id =ot.order_id and p.product_id = ot.product_id\n"
                    + "                    and s.status_order_id =?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, sid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Product p = new Product(rs.getString(4), 0);
                StatusOrder s = new StatusOrder(rs.getInt(7), rs.getString(6), p);
                Order o = new Order( rs.getInt(1),rs.getDate(2),rs.getDouble(5),rs.getString(3), s);
                list.add(o);
            }
            ArrayList<Order> listStatusbyid = checkDuplicateOrder(list);
            return listStatusbyid;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Order> getSaleOrderByID(int sid) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select o.order_id,o.orderDate,o.fullName,p.product_name, o.total_cost,s.status_name"
                    + ",s.status_order_id from shoes_shop_online.order o ,user u,  status_order s , order_detail ot , product p\n"
                    + "                    where u.userId = o.userId and status_order_id = o.status_order \n"
                    + "                    and o.order_id =ot.order_id and p.product_id = ot.product_id\n"
                    + "                    and o.saler_id =?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, sid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Product p = new Product(rs.getString(4), 0);
                StatusOrder s = new StatusOrder(rs.getInt(7), rs.getString(6), p);
                Order o = new Order( rs.getInt(1),rs.getDate(2),rs.getDouble(5),rs.getString(3), s);
                list.add(o);

            }
            ArrayList<Order> listSalebyid = checkDuplicateOrder(list);
            return listSalebyid;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Order> getOrderDetailByOid(int oid) {
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select o.order_id, pi.images ,p.product_name,ot.size,ot.quantity,o.orderDate, p.sale_prices,s.status_name,"
                    + "(select u.fullName from `order` o, `user` u where u.userId = o.saler_id and o.order_id = ?) as saler \n"
                    + "from `order` o, order_detail ot, product p , products_images pi, status_order s, `user` u\n"
                    + "where o.order_id = ot.order_id \n"
                    + "and s.status_order_id = o.status_order\n"
                    + "and u.userId = o.userId\n"
                    + "and p.product_id = ot.product_id \n"
                    + "and p.product_id = pi.product_id\n"
                    + "and o.order_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, oid);
            stm.setInt(2, oid);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                User u = new User(1, rs.getString(9));
                Product p = new Product(rs.getString(3), rs.getFloat(7), u);
                ProductImages pi = new ProductImages(1, rs.getString(2), p);
                StatusOrder s = new StatusOrder(1, rs.getString(8), pi);
                OrderDetail ot = new OrderDetail(rs.getInt(5), rs.getInt(4), s, null);
                Order o = new Order(rs.getInt(1), rs.getDate(6), ot);
                list.add(o);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }

    public Order getUserInforOfSale(int oid) {
        try {
            String sql = "SELECT st.status_order_id, st.status_name, o.order_id,o.fullName,o.mobile ,o.address,o.total_cost, o.status_order, o.orderDate, o.saler_id FROM user u , `order` o, status_order st where o.userId = u.userId and o.status_order = st.status_order_id and o.order_id=?;";
            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setInt(1, oid);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                StatusOrder sa = new StatusOrder(rs.getInt("status_order_id"), rs.getString("status_name"));
                Order u = new Order(rs.getInt("order_id"), rs.getDate("orderDate"), rs.getDouble("total_cost"), rs.getString("fullName"), rs.getString("mobile"), rs.getString("address"), rs.getInt("saler_id"), null, sa, null);
                return u;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean UpdateStatus(int sid, int oid) {
        boolean check = true;
        try {
            String sql = "update shoes_shop_online.order\n"
                    + "set status_order = ?\n"
                    + "where order_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, sid);
            stm.setInt(2, oid);
            stm.executeUpdate();

        } catch (Exception e) {
            check = false;
        }

        return check;
    }

    public boolean UpdateSaler(int slid, int oid) {
        boolean check = true;
        try {
            String sql = "update shoes_shop_online.order\n"
                    + "set saler_id = ?\n"
                    + "where order_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, slid);
            stm.setInt(2, oid);
            stm.executeUpdate();

        } catch (Exception e) {
            check = false;
        }

        return check;
    }

    public int getTotalorderbyStattus1(String slid, String d1, String d2) {
        String sql = " SELECT count( *)  FROM shoes_shop_online.order  where saler_id=? and status_order=1 and\n"
                + " orderDate BETWEEN ?  - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, slid);
            st.setString(2, d1);
            st.setString(3, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalorderbyStattus2(String slid, String d1, String d2) {
        String sql = " SELECT count( *)  FROM shoes_shop_online.order  where saler_id=? and status_order=2 and\n"
                + "orderDate BETWEEN ?  - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, slid);
            st.setString(2, d1);
            st.setString(3, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalorderbyStattus3(String slid, String d1, String d2) {
        String sql = " SELECT count( *)  FROM shoes_shop_online.order  where saler_id=? and status_order=3 and\n"
                + " orderDate BETWEEN ?  - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, slid);
            st.setString(2, d1);
            st.setString(3, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalorderbyStattus4(String slid, String d1, String d2) {
        String sql = " SELECT count( *)  FROM shoes_shop_online.order  where saler_id=? and status_order=4 and\n"
                + " orderDate BETWEEN ?  - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, slid);
            st.setString(2, d1);
            st.setString(3, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalorderbyStattus5(String slid, String d1, String d2) {
        String sql = " SELECT count( *)  FROM shoes_shop_online.order  where saler_id=? and status_order=5 and\n"
                + "orderDate BETWEEN ?  - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, slid);
            st.setString(2, d1);
            st.setString(3, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTotalorderbyStattus6(String slid, String d1, String d2) {
        String sql = " SELECT count( *)  FROM shoes_shop_online.order  where saler_id=? and status_order=6 and\n"
                + "orderDate BETWEEN ?  - INTERVAL 7 DAY AND ? + INTERVAL 7 DAY ; ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, slid);
            st.setString(2, d1);
            st.setString(3, d2);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static void main(String[] args) {
        OrderSaleDAO dao = new OrderSaleDAO();
        ArrayList<User> list = dao.getallSale();
        
        for (User order : list) {
            System.out.println();
        }

    }

    public static ArrayList<Order> checkDuplicateOrder(ArrayList<Order> olist) {
        OrderDAO dao = new OrderDAO();
        ArrayList<Order> oListNoDup = new ArrayList<>();
        HashSet<Integer> set = new HashSet<>();
        for (Order order : olist) {
            if (!set.contains(order.getOrderid())) {
                oListNoDup.add(order);
                set.add(order.getOrderid());
            }
        }
        return oListNoDup;
    }

}
