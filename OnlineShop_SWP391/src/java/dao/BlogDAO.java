/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Blog;
import model.CategoryBlog;
import model.User;

/**
 *
 * @author Admin
 */
public class BlogDAO extends DBcontext {

    // lay 2 blog o home
    public ArrayList<Blog> getBlog() {
        int count = 0;
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "select b.blog_id, b.title, u.userId, b.updated_date, b.content, b.thumbnail, b.brief_infor, b.status, u.fullName, cb.categoryBlog_name, cb.categoryBlog_id from blog b , user u , category_blog cb \n" +
"                               where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and b.status =1 order by updated_date desc;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getInt("categoryBlog_id"), rs.getString("categoryBlog_name"));
                Blog b = new Blog( rs.getInt("blog_id"),rs.getString("title"),rs.getDate("updated_date"), rs.getString("content"),rs.getString("thumbnail"), rs.getString("brief_infor"), rs.getBoolean("status"), u, cb);
                list.add(b);
                count++;
                if (count == 2) {
                    break;
                }
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    // lay all blog
    public ArrayList<Blog> getAllBlog() {
        
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "select b.blog_id, b.title, u.userId, b.updated_date, b.content, b.thumbnail, b.brief_infor, b.status, u.fullName, cb.categoryBlog_name, cb.categoryBlog_id from blog b , user u , category_blog cb \n" +
"                               where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and b.status =1 order by updated_date desc;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getInt("categoryBlog_id"), rs.getString("categoryBlog_name"));
                Blog b = new Blog( rs.getInt("blog_id"),rs.getString("title"),rs.getDate("updated_date"), rs.getString("content"),rs.getString("thumbnail"), rs.getString("brief_infor"), rs.getBoolean("status"), u, cb);
                list.add(b);

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    // lay ra tat ca cac categoryblog
    public ArrayList<CategoryBlog> getAllCategoryBlog() {
        ArrayList<CategoryBlog> categoryblogs = new ArrayList<>();
        String sql = " SELECT * FROM shoes_shop_online.category_blog;";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
//            UserAccountDAO dao = new UserAccountDAO();
            while (rs.next()) {
                CategoryBlog cb = new CategoryBlog();
                cb.setCategoryBlogId(rs.getInt("categoryBlog_id"));
                cb.setCategoryBlogName(rs.getString("categoryBlog_name"));
                cb.setStatus(rs.getBoolean("status"));
                categoryblogs.add(cb);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categoryblogs;
    }

    public Blog getBlogByID(String id) {

         String sql = "select b.blog_id, b.title, u.userId, b.updated_date, b.content, b.thumbnail, b.brief_infor, b.status, u.fullName, cb.categoryBlog_name, cb.categoryBlog_id from blog b , user u , category_blog cb \n"
                + "                where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and b.blog_id =?;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getInt("categoryBlog_id"), rs.getString("categoryBlog_name"));
                Blog b = new Blog( rs.getInt("blog_id"),rs.getString("title"),rs.getDate("updated_date"), rs.getString("content"),rs.getString("thumbnail"), rs.getString("brief_infor"), rs.getBoolean("status"), u, cb);
             
                return b;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public CategoryBlog getCategoryBlogByID(String id) {

        String sql = " select* from category_blog where categoryBlog_id = ?;";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                CategoryBlog cb = new CategoryBlog();
                cb.setCategoryBlogId(rs.getInt("categoryBlog_id"));
                cb.setCategoryBlogName(rs.getString("categoryBlog_name"));
                cb.setStatus(rs.getBoolean("status"));

                return cb;

            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
  // lây blog rheo categoryblog
    public ArrayList<Blog> getAllBlogByCateId(String id) {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "select b.blog_id, b.title, u.userId, b.updated_date, b.content, b.thumbnail, b.brief_infor, b.status, u.fullName, cb.categoryBlog_name, cb.categoryBlog_id from blog b , user u , category_blog cb\n" +
"               where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and cb.categoryBlog_id = ?;               ";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                 User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getInt("categoryBlog_id"), rs.getString("categoryBlog_name"));
                Blog b = new Blog( rs.getInt("blog_id"),rs.getString("title"),rs.getDate("updated_date"), rs.getString("content"),rs.getString("thumbnail"), rs.getString("brief_infor"), rs.getBoolean("status"), u, cb);
                list.add(b);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    // lay blog ms nhat
    public Blog getLastBlog() {

        String sql = "select b.blog_id, b.title, u.userId, b.updated_date, b.content, b.thumbnail, b.brief_infor, b.status, u.fullName, cb.categoryBlog_name, cb.categoryBlog_id from blog b , user u , category_blog cb \n" +
                        "where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and b.status =1 order by updated_date desc limit 1; ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getInt("categoryBlog_id"), rs.getString("categoryBlog_name"));
                Blog b = new Blog( rs.getInt("blog_id"),rs.getString("title"),rs.getDate("updated_date"), rs.getString("content"),rs.getString("thumbnail"), rs.getString("brief_infor"), rs.getBoolean("status"), u, cb);

                return b;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //dem so blog co trong dtb
    public int getTotalBlog() {
        String sql = "select count(*) from blog where status = 1 ;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
   // phân trang blog
    public ArrayList<Blog> pagingBlog(int index) {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "SELECT b.blog_id, b.title,u.userId, b.updated_date, b.content, b.thumbnail, b.brief_infor, b.status, u.fullName, cb.categoryBlog_name, cb.categoryBlog_id from blog b , user u , category_blog cb \n" +
"          where b.categoryBlog_id = cb.categoryBlog_id and u.userId = b.author_id and b.status =1  ORDER BY blog_id ASC\n" +
"                 limit 2 offset ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 2);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
           User u = new User(rs.getInt("userId"), rs.getString("fullName"));
                CategoryBlog cb = new CategoryBlog(rs.getInt("categoryBlog_id"), rs.getString("categoryBlog_name"));
                Blog b = new Blog( rs.getInt("blog_id"),rs.getString("title"),rs.getDate("updated_date"), rs.getString("content"),rs.getString("thumbnail"), rs.getString("brief_infor"), rs.getBoolean("status"), u, cb);
                list.add(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    // search blog by title
    public ArrayList<Blog> searchBlogbyTitle(String search) {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "select * from blog where title like ?;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, "%"+search+"%");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Blog b = new Blog(rs.getInt(1), rs.getString(2), rs.getDate(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getBoolean("status"));
                list.add(b);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
         
    }
    
    

    public static void main(String[] args) {
        BlogDAO dao = new BlogDAO();

//        int count = dao.getTotalBlog();
//        System.out.println(count);

       ArrayList<Blog> list = dao.pagingBlog(2);
        System.out.println(list);
        }

    }


