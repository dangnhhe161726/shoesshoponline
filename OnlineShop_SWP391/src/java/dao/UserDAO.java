/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * @author asus
 */
public class UserDAO extends DBcontext {

    //Get user's ID by Email
    public int getIDByEmail(String email) {
        String sql = "select userId from User u where u.email = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }
    //Get Login Information: ID, Role, Status
    public User getLoginInfo(String email, String password) {

        String sql = "select userId, role_id, status, fullName from User u where u.email = ? and u.password = ? ";

        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, email);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1), rs.getInt(2),rs.getInt(3), rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    
    //Get Login Information: ID, Role, Status
    public User getLoginInfo(String email) {

        String sql = "select userId, role_id, status, fullName from User u where u.email = ? ";

        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1), rs.getInt(2),rs.getInt(3), rs.getString(4));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
    
    //Get an account by ID
    public User getAccountByID(int id) {
        String sql = "select * from User u where u.userId = ?";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getString(6), rs.getString(3));
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    //Change user Password
    public void chagePassword(int id, String newPassword) {
        String sql = "UPDATE user\n"
                + "SET password = ?"
                + "WHERE userId = ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newPassword);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    //get Account List
    public ArrayList<User> getallAccount() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM user;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt(1), rs.getString(6), rs.getString(3));
                list.add(u);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    //Get user's account by Email
    public User getAccountByEmail(String email) {
        String sql = "select userId, role_id, status from User u where u.email = ? ";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt(1), rs.getInt(2),rs.getInt(3));
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
    


    //Check an email Exist
    public boolean checkEmail(String email) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM shoes_shop_online.user where email=?");
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    //Get user Profile by ID
    public User getProfile(String id) {

        try {

            String sql = "select * from User u where u.userId = ?";
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("email"), rs.getString("password"), rs.getString("avatar"), rs.getBoolean("gender"), rs.getString("mobile"), rs.getString("address"), rs.getInt("status"));
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Edit user's profile
    public void editProfile(String fullname, String address, String phone, String gender, String id) {
        try {
            String sql = "update shoes_shop_online.user set fullName=?,address=?,mobile=?,gender=? where userId=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, fullname);
            preparedStatement.setString(2, address);
            preparedStatement.setString(3, phone);
            preparedStatement.setString(4, gender);
            preparedStatement.setString(5, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Insert a feedback
    public void insertfeedback(String fullname, String feedback, int userid) {
        try {
            String sql = "INSERT INTO shoes_shop_online.feedback (fullName,feedback,status,userId) values(?,? ,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, fullname);
            statement.setString(2, feedback);

            statement.setFloat(3, 1);
            statement.setInt(4, userid);
            //0 = inactive : 1 = active

            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Edit user's profile
    public void editProfile(String fullname, String avatar, String address, String phone, String gender, String id) {
        try {
            String sql = "update shoes_shop_online.user set fullName=?,avatar=?,address=?,mobile=?,gender=? where userId=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, fullname);
            preparedStatement.setString(2, avatar);
            preparedStatement.setString(3, address);
            preparedStatement.setString(4, phone);
            preparedStatement.setString(5, gender);
            preparedStatement.setString(6, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Insert a new User
    public void insertUser(String name, String email, String password, String address, String mobile, String gender, String role, String status) {
        try {
            String sql = "INSERT INTO shoes_shop_online.user (fullName, email, password,address,mobile,gender,role_id,status)VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, password);
            statement.setString(4, address);
            statement.setString(5, mobile);
            statement.setString(6, gender);
            statement.setString(7, role);
            statement.setString(8, status);
            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
    //Get all user's account
    public ArrayList<User> getallUser() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT * FROM user;";
        try {
            PreparedStatement statement = connection.prepareCall(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getBoolean("gender"), rs.getString("email"), rs.getString("mobile"), rs.getInt("role_id"), rs.getInt("status"));
                list.add(u);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    //Get User Information
    public User getUserInfomation(String id) {

        try {

            String sql = "select * from User u where u.userId = ?";
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getString("email"), rs.getString("avatar"), rs.getBoolean("gender"), rs.getString("mobile"), rs.getString("address"), rs.getInt("role_id"), rs.getInt("status"));
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //update User Detail
    public void updateUserDetail(String id, String role, String status) {
        try {
            String sql = "update shoes_shop_online.user set role_id=?,status=? where userId=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, role);
            preparedStatement.setString(2, status);
            preparedStatement.setString(3, id);
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Search user
    public List<User> findUserBy(String option, String value) {
        List<User> list = new ArrayList<>();
        String sql = "SELECT * FROM User WHERE " + option + " LIKE ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + value + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userId"), rs.getString("fullName"), rs.getBoolean("gender"), rs.getString("email"), rs.getString("mobile"), rs.getInt("role_id"), rs.getInt("status"));
                list.add(u);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

   
    public void insertUser(String name, String email, String password, String address, String mobile, String gender) {
        try {
            String sql = "INSERT INTO shoes_shop_online.user (fullName, email, password,address,mobile,gender,role_id,status)VALUES (?,?,?,?,?,?,4,1)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, password);
            statement.setString(4, address);
            statement.setString(5, mobile);
            statement.setString(6, gender);
            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User getAccountID(String email, String password) {

        String sql = "select * from User u where u.email = ? and u.password = ? ";

        try {
            PreparedStatement statement = connection.prepareCall(sql);
            statement.setString(1, email);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    

  

    

    public void updateUserInformationbyAdmin(String id, String role, String status) {
        try {
            String sql = "update shoes_shop_online.user set role_id=?,status=? where userId=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, role);
            preparedStatement.setString(2, status);
            preparedStatement.setString(3, id);
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        UserDAO dao = new UserDAO();
        User u = dao.getProfile("12");
        System.out.println(u.getEmail());
    }
}
