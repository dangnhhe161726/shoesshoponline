/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package LocationFile;

import jakarta.servlet.ServletContext;

/**
 *
 * @author Admin
 */
public class Location {

    //Ảnh chính cho web gồm(ảnh sản phẩm, ảnh blog, ảnh slider)
    public final String LOADIMAGETOFILEIMG = "img/";
    //Ảnh linh tinh, ảnh người dùng( sử dụng trong quản lý người dùng, profile...)
    public final String LOADIMAGETOFILELOADIMG = "LoadImg/";

    public static StringBuilder getRelativePath(ServletContext context, String filePath) {
        String appPath = context.getRealPath("/");
        String link = convertPath(appPath + filePath);
        StringBuilder linkPath = editLocation(link);
        return linkPath;
    }

    public static String convertPath(String path) {
        return path.replace("\\", "/");
    }

    public static StringBuilder editLocation(String path) {
        String[] splits = path.split("build/");
        StringBuilder stringBuilder = new StringBuilder();
        for (String item : splits) {
            stringBuilder.append(item);
        }
        return stringBuilder;
    }

}
