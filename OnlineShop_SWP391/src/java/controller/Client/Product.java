/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Client;

import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Category;

/**
 *
 * @author Admin
 */
public class Product extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProductDAO dao = new ProductDAO();
        int endPage = 0;

        //get product by category id
        String cateId = request.getParameter("cateId");

        //get quantity product
        String pageSize = request.getParameter("pageSize");

        //get number page of Product 
        String page_raw = request.getParameter("page");

        int page = Integer.parseInt(page_raw);
        
        int currentPage = page;
        
        ArrayList<model.Product> plist = null;

        if (cateId != null && cateId.isEmpty()) {
            cateId = null;
        }

        if (pageSize != null && pageSize.isEmpty()) {
            pageSize = null;
        }

        //get all product
        if (cateId == null && pageSize == null) {
            plist = dao.pagingForProduct(page, pageSize, cateId);
        }

        // get product by Category ID
        if (cateId != null && pageSize == null) {
            plist = dao.pagingForProduct(page, pageSize, cateId);

        }

        //get all product with quantity
        if (cateId == null && pageSize != null) {
            plist = dao.pagingForProduct(page, pageSize, cateId);

        }
        // get product by Category id with quantity
        if (cateId != null && pageSize != null) {
            plist = dao.pagingForProduct(page, pageSize, cateId);

        }

        if (pageSize == null) {
            int pageS = 6;
            int count = dao.getTotalProduct(cateId, null);
            endPage = count / pageS;
            if (count % pageS != 0) {
                endPage++;
            }
        } else {
            int count = dao.getTotalProduct(cateId, null);
            endPage = count / Integer.parseInt(pageSize);
            if (count % Integer.parseInt(pageSize) != 0) {
                endPage++;
            }
        }
        request.setAttribute("page_raw", page_raw);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("plist", plist);
        request.setAttribute("endPage", endPage);
        request.setAttribute("plist", plist);
        request.setAttribute("pageSize", pageSize);
        request.setAttribute("cateId", cateId);

        request.getRequestDispatcher("./Common/ProductList.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO dao = new ProductDAO();

        String option_raw = request.getParameter("option");

        ArrayList<model.Product> plist = null;

        if (option_raw != null) {
            int option = Integer.parseInt(option_raw);

            if (option < 5) {

                plist = dao.getProductByPrice(option);

            } else {

                plist = dao.getProductBySize(option);

            }
        }
        boolean hidden = false;
        request.setAttribute("hidden", hidden);
        request.setAttribute("plist", plist);
        request.getRequestDispatcher("./Common/ProductList.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
