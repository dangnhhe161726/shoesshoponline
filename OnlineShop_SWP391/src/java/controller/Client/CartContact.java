/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Client;

import Service.SendMail;
import dao.OrderDAO;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Cart;
import model.Order;
import model.User;

/**
 *
 * @author Admin
 */
public class CartContact extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CartContact</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CartContact at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderDAO dao = new OrderDAO();
        UserDAO udao = new UserDAO();
        //get information of user from CartContact.jsp
        HttpSession session = request.getSession();
        String note = request.getParameter("noteCart");
        String pay = request.getParameter("pay");
        String fullName = request.getParameter("fullName");
        String city = request.getParameter("city").trim();
        String district = request.getParameter("district").trim();
        String ward = request.getParameter("ward").trim();
        String address = request.getParameter("address");
        String phoneNumber = request.getParameter("phoneNumber");
        String email = request.getParameter("email");
        String addressDetail = address + "," + ward + "," + district + "," + city;
        int payment = Integer.parseInt(pay);

        // get cart session
        Cart cart = null;
        Object ob = session.getAttribute("cart");
        if (ob != null) {
            cart = (Cart) ob;
        } else {
            cart = new Cart();
        }

        //get id user session
        String uid = session.getAttribute("id") + "";
        if (uid != null && uid.isEmpty()) {
            uid = null;
        }

        if (uid == null) {
            request.getRequestDispatcher("./Common/Page404.jsp").forward(request, response);
        } else {
            if (payment == 1) {
                User user = udao.getProfile(uid);
                dao.addOrder(user, cart, fullName, phoneNumber, addressDetail, note);
                String htmlContent = "<html>\n"
                        + "    <head>\n"
                        + "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                        + "    </head>\n"
                        + "    <body>\n"
                        + "        <h1 style=\"color: #1877F2;\">Cảm ơn quý khách đã mua hàng</h1>\n"
                        + "        <h2>Xin chào" + fullName + "cảm ơn bạn đã mua hàng tại Shoes shop của chúng tôi</h2>\n"
                        + "        <p>Chúng tôi hy vọng quý khách hài lòng với trải nghiệm mua sắm và các sản phẩm đã chọn.</p>\n"
                        + "        <p>Đơn hàng của quý khách hiện đã được tiếp nhận. Bạn có thể xem đơn bạn đã mua tại phần đơn hàng của tôi.</p>\n"
                        + "        <br>\n"
                        + "        <p>Địa chỉ giao hàng:" + addressDetail + "</p>\n"
                        + "        <p>Người nhận:" + fullName + "</p>\n"
                        + "        <p>Số điện thoại:" + phoneNumber + "</p>\n"
                        + "        <br>\n"
                        + "        <p>Hình thức thanh toán: Thanh toán khi nhận hàng</p>\n"
                        + "        <p>Thời gian giao hàng dự kiến</p>\n"
                        + "        <p style=\"color: red;\">Lưu ý:</p>\n"
                        + "        <p>- Thời gian giao hàng trên được tính kể từ ngày đơn vị vận chuyển bắt đầu phát hàng đi, chưa bao gồm thời gian xử lý đơn hàng và không giao hàng vào ngày Chủ nhật và ngày Lễ.</p>\n"
                        + "        <p>- Trong thời gian đơn hàng được phát đi, quý khách có nhu cầu thay đổi thông tin địa chỉ nhận hàng, nếu trái tuyến đường đang vận chuyển trước đó sẽ phải thanh toán thêm phí vận chuyển phát sinh.</p>\n"
                        + "    </body>\n"
                        + "</html>";

                SendMail.send("Shoes shop", htmlContent, email);
                session.removeAttribute("cart");
                session.removeAttribute("noteCart");
                session.removeAttribute("userAddress");
                session.setAttribute("sizeCart", 0);
                request.getRequestDispatcher("./Common/CartCompletion.jsp").forward(request, response);
            } else {
                Order order = new Order(fullName, phoneNumber, addressDetail, note);
                session.setAttribute("OrderInfo", order);
                response.sendRedirect("payment");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
