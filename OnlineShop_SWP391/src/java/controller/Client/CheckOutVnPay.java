/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Client;

import Service.SendMail;
import dao.OrderDAO;
import dao.ProductDAO;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Cart;
import model.Order;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CheckOutVnPay", urlPatterns = {"/checkoutvnpay"})
public class CheckOutVnPay extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CheckOutVnPay</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CheckOutVnPay at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        String vnp_Amount = request.getParameter("vnp_Amount");
        String vnp_BankCode = request.getParameter("vnp_BankCode");
        String vnp_CardType = request.getParameter("vnp_CardType");
        if (vnp_CardType != null && vnp_CardType.isEmpty()) {
            vnp_CardType = "QRCODE";
        }

        if (vnp_BankCode != null && vnp_BankCode.isEmpty()) {
            vnp_BankCode = "VNPAY";
        }

        if (vnp_BankCode.equalsIgnoreCase("VNPAY") && vnp_CardType.equalsIgnoreCase("QRCODE")) {
            request.getRequestDispatcher("./Common/CartContact.jsp").forward(request, response);
        } else {
            // get cart session
            Cart cart = (Cart) session.getAttribute("cart");
            //get id user session
            String uid = session.getAttribute("id") + "";
            if (uid != null && uid.isEmpty()) {
                uid = null;
            }
            if (uid == null) {
                request.getRequestDispatcher("./Common/Page404.jsp").forward(request, response);
            } else {
                String note = session.getAttribute("noteCart") + "";
                Order order = (Order) session.getAttribute("OrderInfo");
                UserDAO dao = new UserDAO();
                User user = dao.getProfile(uid);
                OrderDAO odao = new OrderDAO();
                //add order when user pay online sucessful
                odao.addOrder(user, cart, order.getFullname(), order.getMobile(), order.getAddress(), order.getNote());
                //send mail after customer pay successful
                String htmlContent = "<html>\n"
                        + "    <head>\n"
                        + "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                        + "    </head>\n"
                        + "    <body>\n"
                        + "        <h1 style=\"color: #1877F2;\">Cảm ơn quý khách đã mua hàng</h1>\n"
                        + "        <h2>Xin chào" + order.getFullname() + "cảm ơn bạn đã mua hàng tại Shoes shop của chúng tôi</h2>\n"
                        + "        <p>Chúng tôi hy vọng quý khách hài lòng với trải nghiệm mua sắm và các sản phẩm đã chọn.</p>\n"
                        + "        <p>Đơn hàng của quý khách hiện đã được tiếp nhận. Bạn có thể xem đơn bạn đã mua tại phần đơn hàng của tôi.</p>\n"
                        + "        <br>\n"
                        + "        <p>Địa chỉ giao hàng:" + order.getAddress() + "</p>\n"
                        + "        <p>Người nhận:" + order.getFullname() + "</p>\n"
                        + "        <p>Số điện thoại:" + order.getMobile() + "</p>\n"
                        + "        <br>\n"
                        + "        <p>Hình thức thanh toán: Thanh toán online</p>\n"
                        + "        <p>Thời gian giao hàng dự kiến</p>\n"
                        + "        <p style=\"color: red;\">Lưu ý:</p>\n"
                        + "        <p>- Thời gian giao hàng trên được tính kể từ ngày đơn vị vận chuyển bắt đầu phát hàng đi, chưa bao gồm thời gian xử lý đơn hàng và không giao hàng vào ngày Chủ nhật và ngày Lễ.</p>\n"
                        + "        <p>- Trong thời gian đơn hàng được phát đi, quý khách có nhu cầu thay đổi thông tin địa chỉ nhận hàng, nếu trái tuyến đường đang vận chuyển trước đó sẽ phải thanh toán thêm phí vận chuyển phát sinh.</p>\n"
                        + "    </body>\n"
                        + "</html>";
                SendMail.send("Shoes shop", htmlContent, user.getEmail());
                session.removeAttribute("cart");
                session.removeAttribute("OrderInfo");
                session.removeAttribute("noteCart");
                session.removeAttribute("userAddress");
                session.setAttribute("sizeCart", 0);
                request.getRequestDispatcher("./Common/CartCompletion.jsp").forward(request, response);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
