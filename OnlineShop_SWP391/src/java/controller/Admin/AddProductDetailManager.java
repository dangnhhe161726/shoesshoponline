/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import LocationFile.Location;
import dao.ProductDAO;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import model.Category;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Admin
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2,
        maxFileSize = 1024 * 1024 * 10,
        maxRequestSize = 1024 * 1024 * 50)
@WebServlet(name = "AddProductDetailManager", urlPatterns = {"/addproductdetailmanager"})
public class AddProductDetailManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddProductDetailManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddProductDetailManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO dao = new ProductDAO();
        ArrayList<Category> listCategory = dao.getallCategory();
        request.setAttribute("listCa", listCategory);
        request.getRequestDispatcher("./Manager/AddProductDetailManager.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Location lo = new Location();
        ProductDAO dao = new ProductDAO();
        String productName = request.getParameter("productName");
        String category = request.getParameter("category");
        String salePrices = request.getParameter("salePrices");
        String originalPrices = request.getParameter("originalPrices");
        String brefInfo = request.getParameter("brefInfo");
        String productDetail = request.getParameter("productDetail");
        //get parameter main image of the product
        Part partFileImageUpload = request.getPart("ImageUpload");
        String ImageUpload = "";
        if (partFileImageUpload == null) {
            ImageUpload = "";
        } else {
            ImageUpload = partFileImageUpload.getSubmittedFileName();
        }
        //get parameter sub image of the product
        Part partFileSubImg = request.getPart("subImg");
        String subImg = "";
        if (partFileSubImg == null) {
            subImg = "";
        } else {
            subImg = partFileSubImg.getSubmittedFileName();
        }
        //get parameter size and quantity of product
        String size = request.getParameter("size");
        String quantity = request.getParameter("quantity");
        //check data get file jsp
        if (salePrices == null) {
            salePrices = "0";
        }
        if (originalPrices == null) {
            originalPrices = "0";
        }
        if (size == null) {
            size = "0";
        }
        if (quantity == null) {
            quantity = "0";
        }

        //load image to file img
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletContext servletContext = this.getServletConfig().getServletContext();
        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(repository);
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        OutputStream out = null;
        InputStream filecontent = null;
        StringBuilder absolutePath = Location.getRelativePath(request.getServletContext(), lo.LOADIMAGETOFILEIMG);
        try {
            //read the image loaded into the file for main image of product
            File fileImageUpload = new File(absolutePath + ImageUpload);
            out = new FileOutputStream(fileImageUpload);
            filecontent = partFileImageUpload.getInputStream();
            int readImageUpload = 0;
            final byte[] bytesImageUpload = new byte[1024];
            while ((readImageUpload = filecontent.read(bytesImageUpload)) != -1) {
                out.write(bytesImageUpload, 0, readImageUpload);
            }
            //read the image loaded into the file for sub image of product
            File fileSubImg = new File(lo.LOADIMAGETOFILEIMG + subImg);
            out = new FileOutputStream(fileSubImg);
            filecontent = partFileSubImg.getInputStream();
            int readSubImg = 0;
            final byte[] bytesSubImg = new byte[1024];
            while ((readSubImg = filecontent.read(bytesSubImg)) != -1) {
                out.write(bytesSubImg, 0, readSubImg);
            }
        } catch (FileNotFoundException fne) {
            System.out.println(fne);
        }
        int pid = dao.addProductDetail(productName, Double.parseDouble(originalPrices), Double.parseDouble(salePrices), productDetail, brefInfo, Integer.parseInt(category), Integer.parseInt(size), Integer.parseInt(quantity), ImageUpload, subImg);
        response.sendRedirect("productdetailmanager?id=" + pid);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
