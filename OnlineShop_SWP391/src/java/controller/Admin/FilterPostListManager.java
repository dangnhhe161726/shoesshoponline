/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import dao.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Blog;
import model.CategoryBlog;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "FilterPostListManager", urlPatterns = {"/filterpostlistmanager"})
public class FilterPostListManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FilterPostListManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FilterPostListManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDAO dao = new PostDAO();
        ArrayList<CategoryBlog> listCategoryBlog = dao.getAllCategoryBlog();
        request.setAttribute("listCB", listCategoryBlog);
        ArrayList<User> listAu = dao.getallUserRoleMarketing();
        request.setAttribute("listAu", listAu);
        ArrayList<User> listu = dao.getallUserRoleMarketing();
        request.setAttribute("listb", listu);
        ArrayList<Blog> listb = null;
        String filterPost = request.getParameter("filterPost");
        request.setAttribute("optionFilter", filterPost);

        if (Integer.parseInt(filterPost) == 0) {
            listb = dao.getAllPostandAuthor();
            request.setAttribute("listb", listb);

        } else {
            if (Integer.parseInt(filterPost) == -1000 || Integer.parseInt(filterPost) == -2000) {
                //Filter post by status
                if (Integer.parseInt(filterPost) == -1000) {
                    listb = dao.getAllPostandAuthor();
                    ArrayList<Blog> listPostbyStatus = new ArrayList<>();
                    for (Blog blog : listb) {
                        if (blog.isStatus()) {
                            listPostbyStatus.add(blog);
                        }
                    }
                    request.setAttribute("listb", listPostbyStatus);
                }
                if (Integer.parseInt(filterPost) == -2000) {
                    listb = dao.getAllPostandAuthor();
                    ArrayList<Blog> listPostbyStatus = new ArrayList<>();
                    for (Blog blog : listb) {
                        if (!blog.isStatus()) {
                            listPostbyStatus.add(blog);
                        }
                    }
                    request.setAttribute("listb", listPostbyStatus);
                }
            }//Filter post by category
            else {
                if (Integer.parseInt(filterPost) > 0) {
                    listb = dao.getAllPostByCateId(filterPost);
                    request.setAttribute("listb", listb);
                } else {
                    int a = Integer.parseInt(filterPost) * (-1);
                    request.setAttribute("optionFilter", a);
                    listb = dao.getAllPostByAuthorId(a);
                    request.setAttribute("listb", listb);
                }
            }

        }
        request.getRequestDispatcher("./Manager/PostManager.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
