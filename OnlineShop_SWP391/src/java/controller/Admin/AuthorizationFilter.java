/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Filter.java to edit this template
 */
package controller.Admin;

import dao.AuthorizationDAO;
import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

/**
 *
 * @author asus
 */
public class AuthorizationFilter implements Filter {

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public AuthorizationFilter() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String url = httpRequest.getServletPath();
        AuthorizationDAO dao = new AuthorizationDAO();
        HttpSession session = httpRequest.getSession();
        Integer role_id = (Integer) session.getAttribute("role_id");
        System.out.println("Authorcheck: url request: " + url+"||||role_id of user: " + role_id);
        if (role_id == null) {
            role_id = 0;
        }
        if (!dao.authorizationCheck(role_id, url)) {
            switch (role_id) {
                case 1:
                    httpResponse.sendRedirect("admindashboard");
                    break;
                case 2:
                    httpResponse.sendRedirect("orderdashboard");
                    break;
                case 3:
                    httpResponse.sendRedirect("marketingdashboard");
                    break;
                case 4:
                    httpResponse.sendRedirect("home");
                    break;
                case 5:
                    httpResponse.sendRedirect("orderdashboard");
                    break;
                case 0:
                    httpResponse.sendRedirect("home");
                    break;
            }

            return;
        }

        Throwable problem = null;
        try {
            chain.doFilter(request, response);
        } catch (Throwable t) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            problem = t;
            t.printStackTrace();
        }

    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

}
