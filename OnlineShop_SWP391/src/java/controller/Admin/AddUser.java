/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.Admin;

import Service.SendMail;
import static controller.Merge.Register.checkEmail;
import static controller.Merge.Register.checkPassword;
import static controller.Merge.Register.checkPhone;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.eclipse.jdt.internal.compiler.parser.Parser.name;

/**
 *
 * @author asus
 */
public class AddUser extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddUser</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddUser at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        request.getRequestDispatcher("Manager/AddUser.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        String mobile = request.getParameter("mobile");
        String gender = request.getParameter("gender");
        String role = request.getParameter("role");
        String status = request.getParameter("status");
        String password = request.getParameter("password");
        String error = "";
        UserDAO dao = new UserDAO();

        if (checkPassword(password) && checkPhone(mobile) && !dao.checkEmail(email)) {
            dao.insertUser(fullname, email, password, address, mobile, gender, role, status);
            String htmlContent = "<html><head><style>body { background-color: lightblue; color: darkblue; font-family: Arial, sans-serif; }</style></head><body>"
                    + "<h1>Thông báo: Bạn đã được quản trị viên cấp tài khoản</h1>"
                    + "<p>Tài khoản của bạn:</p><br>"
                    + "<p>Email: "+ email +"</p><br>"
                    + "<p>Mật khẩu: "+ password +"</p><br>"              
                    + "</body></html>";
            SendMail.send("Hello", htmlContent, email);
            request.getRequestDispatcher("userlistforadmin").forward(request, response);
        } else {
            if (dao.checkEmail(email)) {
                error += "Email đã tồn tại !";
                request.setAttribute("error", error);
            }
            if (!checkEmail(email)) {
                error += "Email phải có đuôi @gmail.com hoặc @fpt.edu.vn<br>";
                request.setAttribute("error", error);
            }
            if (!checkPassword(password)) {
                error += "Password phải có chữ cái in hoa và ký tự đặc biệt<br>";
                request.setAttribute("error", error);
            }
            if (!checkPhone(mobile)) {
                error += "Số điện thoại chỉ có 10 chữ số<br>";
                request.setAttribute("error", error);
            }

            request.getRequestDispatcher("./Manager/AddUser.jsp").forward(request, response);
        }
        
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static boolean checkEmail(String email) {
            String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                    + "[a-zA-Z0-9_+&*-]+)*@"
                    + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                    + "A-Z]{2,7}$";

            Pattern pat = Pattern.compile(emailRegex);
            if (email == null) {
                return false;
            }
            return pat.matcher(email).matches();
        }

        public static boolean checkPassword(String password) {

            // Regex to check valid password.
            String regex = "^(?=.*[0-9])"
                    + "(?=.*[a-z])(?=.*[A-Z])"
                    + "(?=.*[@#$%^&+=])"
                    + "(?=\\S+$).{8,32}$";

            // Compile the ReGex
            Pattern p = Pattern.compile(regex);

            // If the password is empty
            // return false
            if (password == null) {
                return false;
            }

            // Pattern class contains matcher() method
            // to find matching between given password
        // and regular expression.
        Matcher m = p.matcher(password);

        // Return if the password
        // matched the ReGex
        return m.matches();
    }

    public static boolean checkPhone(String phone) {
        if (phone.matches("^0\\d{9}$") && !phone.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
