/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import Service.SendMail;
import static controller.Merge.Register.checkEmail;
import static controller.Merge.Register.checkPassword;
import static controller.Merge.Register.checkPhone;
import dao.MarketingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class AddCustomer extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddCustomer</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddCustomer at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("./Manager/AddCustomer.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String gender = request.getParameter("gender");
        String mobile = request.getParameter("mobile");
        String address = request.getParameter("address");
        String status = "1";
        String error = "";
        MarketingDAO dao = new MarketingDAO();

        if (checkPassword(password) && checkPhone(mobile) && !dao.checkEmail(email) && !dao.checkPhone(mobile)) {
            dao.insertCustomer(name, email, password, address, mobile, gender);
            String htmlContent = "<html><head><style>body { background-color: lightblue; color: darkblue; font-family: Arial, sans-serif; }</style></head><body>"
                    + "<h1>Tài khoản của bạn là:</h1>"
                    + "<p><strong>Account:</strong> " + email + "</p>"
                    + "<p><strong>Password:</strong> " + password + "</p>"
                    + "<p>Chào mừng bạn tới với trang web của chúng tôi!</p>";

            SendMail.send("Xin chào bạn", htmlContent, email);
            response.sendRedirect("listcustomer");

        } else {
            if (!checkEmail(email)) {
                error += "Email phải có đuôi @gmail.com hoặc @fpt.edu.vn<br>";
                request.setAttribute("error", error);
            }
            if (dao.checkEmail(email)) {
                error += "Email đã tồn tại !<br>";
                request.setAttribute("error", error);
            }
            if (dao.checkPhone(mobile)) {
                error += "Số điện thoại đã tồn tại !<br>";
                request.setAttribute("error", error);
            }
            if (!checkPassword(password)) {
                error += "Password phải có chữ cái in hoa và ký tự đặc biệt<br>";
                request.setAttribute("error", error);
            }
            if (!checkPhone(mobile)) {
                error += "Số điện thoại chỉ có 10 chữ số<br>";
                request.setAttribute("error", error);
            }
            
            request.getRequestDispatcher("./Manager/AddCustomer.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override

    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
