/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import dao.OrderSaleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Order;
import model.StatusOrder;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "OrderManager", urlPatterns = {"/ordermanager"})
public class OrderManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderSaleDAO DAO = new OrderSaleDAO();
        HttpSession session = request.getSession();

        //get id user
        int u = (int) session.getAttribute("id");

        ArrayList<Order> listO = DAO.getallOrders();
        ArrayList<Order> listObyId = DAO.getSaleOrderByID(u);

        request.setAttribute("listObyId", listObyId);
        request.setAttribute("listO", listO);
        request.getRequestDispatcher("Manager/OrderManager.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        OrderSaleDAO dao = new OrderSaleDAO();

        //get id user
        int u = (int) session.getAttribute("id");
        ArrayList<User> listSale = dao.getallSale();
        ArrayList<StatusOrder> listStatus = dao.getallStatus();

        ArrayList<Order> listO = dao.getallOrders();
        ArrayList<Order> listObyId = dao.getSaleOrderByID(u);

        request.setAttribute("listObyId", listObyId);

        request.setAttribute("listStatus", listStatus);
        request.setAttribute("listSale", listSale);
        request.setAttribute("listO", listO);

        String mainSelect = request.getParameter("mainSelect");

        if (mainSelect != null && mainSelect.isEmpty()) {
            mainSelect = null;
        }
        if (mainSelect == null) {
            session.setAttribute("option", mainSelect);
            request.getRequestDispatcher("./Manager/OrderManager.jsp").forward(request, response);
        } else {
            if (Integer.parseInt(mainSelect) == 0) {
                session.setAttribute("option", mainSelect);
                request.getRequestDispatcher("./Manager/OrderManager.jsp").forward(request, response);
            }
            if (Integer.parseInt(mainSelect) == 1) {
                session.setAttribute("option", mainSelect);
                request.getRequestDispatcher("./Manager/OrderManager.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
