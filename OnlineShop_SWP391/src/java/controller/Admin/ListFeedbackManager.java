/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import dao.MarketingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ListFeedbackManager", urlPatterns = {"/listfeedbackmanager"})
public class ListFeedbackManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListFeedbackManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListFeedbackManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MarketingDAO mdao = new MarketingDAO();
        ArrayList<User> unsortedList = mdao.getCustomerbyFeedBackAndProduct1();
        String option = request.getAttribute("option") + "";
        String option1 = request.getAttribute("option1") + "";
        if (option != null && option.isEmpty() && (option1 != null && option1.isEmpty())) {
            option = "2";
            option1 = "6";
        }
        request.setAttribute("option", option);
        request.setAttribute("option1", option1);
        request.setAttribute("alist", unsortedList);
        request.getRequestDispatcher("./Manager/ListFeedbackManager.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MarketingDAO dao = new MarketingDAO();
        String option = request.getParameter("op");
        String option1 = request.getParameter("op1");
        request.setAttribute("option", option);
        request.setAttribute("option1", option1);

        try {
            if ("2".equals(option) && "6".equals(option1)) {
                response.sendRedirect("listfeedbackmanager");
            } else {
                ArrayList<User> lists = new ArrayList<>();
                if ("2".equals(option)) {
                    lists = dao.getCustomerFeedBackAndProductbyStar(option1);
                } else if ("6".equals(option1)) {
                    lists = dao.getCustomerFeedBackAndProductbyStatus(option);
                } else {
                    lists = dao.getCustomerFeedBackAndProductbyStatusAndStar(option, option1);
                }
                request.setAttribute("alist", lists);
                request.getRequestDispatcher("./Manager/ListFeedbackManager.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
