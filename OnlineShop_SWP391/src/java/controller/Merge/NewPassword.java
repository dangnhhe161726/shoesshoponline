/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.Merge;

import static controller.Merge.ChangePassword.checkPassword;
import dao.UserDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Servlet implementation class NewPassword
 */
public class NewPassword extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String newPassword = request.getParameter("newpassword");
        String reNewPassword = request.getParameter("renewpassword");
        RequestDispatcher dispatcher = null;
        String email = (String) session.getAttribute("email");
        System.out.println(email);
        if (newPassword.equals(reNewPassword) && !newPassword.equalsIgnoreCase("") && checkPassword(newPassword)) {

            try {
                UserDAO userDAO = new UserDAO();
                int id = userDAO.getIDByEmail(email);
                userDAO.chagePassword(id, newPassword);
                request.setAttribute("resetsuccess", "Cập nhật mật khẩu thành công!");
                request.getRequestDispatcher("Common/Login.jsp").forward(request, response);
//                } else {
//                    request.setAttribute("status", "resetFailed");
//                    dispatcher = request.getRequestDispatcher("../Login");
//                }
//                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
                request.setAttribute("error", "Mật khẩu mới không hợp lệ!<br>Vui lòng kiểm tra lại!");
                request.getRequestDispatcher("Common/NewPassword.jsp").forward(request, response);
            }
    }

    public static boolean checkPassword(String password) {

        // Regex to check valid password.
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,32}$";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the password is empty
        // return false
        if (password == null) {
            return false;
        }

        // Pattern class contains matcher() method
        // to find matching between given password
        // and regular expression.
        Matcher m = p.matcher(password);

        // Return if the password
        // matched the ReGex
        return m.matches();
    }

}
