/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Merge;

import dao.ProductDAO;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Category;
import model.User;

/**
 *
 * @author Admin
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("./Common/Login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        UserDAO dao = new UserDAO();
        User user = dao.getLoginInfo(email, password);
        Object ob = session.getAttribute("cart");
        if (user == null || user.getStatus() == 0) {
            request.setAttribute("error", "<b>Thông tin đăng nhập không chính xác!<br>Vui lòng kiểm tra lại Email và Mật khẩu hoặc Trạng thái của bạn!<b>");
            request.setAttribute("email", email);
            request.getRequestDispatcher("./Common/Login.jsp").forward(request, response);
        }

        if (user != null && ob == null) {
            session.setAttribute("id", user.getUserId());
            session.setAttribute("role_id", user.getRoleId());
            session.setAttribute("fullName", user.getFullName());
            switch (user.getRoleId()) {
                case 1:
                    response.sendRedirect("admindashboard");
                    break;
                case 2:
                    response.sendRedirect("orderdashboard");
                    break;
                case 3:
                    response.sendRedirect("marketingdashboard");
                    break;
                case 4:
                    response.sendRedirect("home");
                    break;
                case 5:
                    response.sendRedirect("orderdashboard");
                    break;
            }
            response.sendRedirect("home");
        }
        if (user != null && ob != null) {
            session.setAttribute("id", user.getUserId());
            session.setAttribute("role_id", user.getRoleId());
            session.setAttribute("fullName", user.getFullName());
            switch (user.getRoleId()) {
                case 1:
                    response.sendRedirect("admindashboard");
                    break;
                case 2:
                    response.sendRedirect("orderdashboard");
                    break;
                case 3:
                    response.sendRedirect("marketingdashboard");
                    break;
                case 4:
                    User user1 = dao.getProfile(user.getUserId() + "");
                    session.setAttribute("userAddress", user1);
                    request.getRequestDispatcher("./Common/CartContact.jsp").forward(request, response);
                    break;
                case 5:
                    response.sendRedirect("orderdashboard");
                    break;
            }
            response.sendRedirect("home");

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
