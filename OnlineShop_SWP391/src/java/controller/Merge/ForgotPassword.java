/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Merge;

import dao.UserDAO;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class ForgotPassword
 */
public class ForgotPassword extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("Common/ForgotPassword.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        final String check = "XÁC NHẬN TÀI KHOẢN";
        final String maOtp = "Mã OTP của bạn là: ";
        String email = request.getParameter("email");

        int otpvalue = 0;

        HttpSession mySession = request.getSession();
        UserDAO userDAO = new UserDAO();
        int id = userDAO.getIDByEmail(email);
        if (id == 0) {
            request.setAttribute("error", "Email không hợp lệ hoặc chưa được đăng ký!");
            request.getRequestDispatcher("Common/ForgotPassword.jsp").forward(request, response);
        }

        if (email != null || !email.equals("")) {
            // sending otp
            Random rand = new Random();
            otpvalue = rand.nextInt(1255650);

            String to = email;// change accordingly
            // Get the session object
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("dinhmanh3802@gmail.com", "iggnilikheekzaly");// Put your email
                    // id anda
                    // password here
                }
            });
            //compose message
            try {
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(email));// change accordingly
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject(check,"UTF-8");
                message.setText(maOtp + otpvalue, "UTF-8");
                // send message
                Transport.send(message);
                System.out.println("message sent successfully");
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }

            request.setAttribute("message", "OTP đã được gửi đến Email của bạn!");
            mySession.setAttribute("otp", otpvalue);
            mySession.setAttribute("otpExpirationTime", System.currentTimeMillis() + 30000); // 2 minutes = 120000 milliseconds
            mySession.setAttribute("email", email);
            request.getRequestDispatcher("Common/EnterOTP.jsp").forward(request, response);
        }
    }

}
