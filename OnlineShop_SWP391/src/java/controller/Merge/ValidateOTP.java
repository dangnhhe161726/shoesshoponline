/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Merge;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class ValidateOtp
 */
public class ValidateOTP extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            int value = Integer.parseInt(request.getParameter("otp"));
            HttpSession session = request.getSession();
            RequestDispatcher dispatcher = null;

            Long otpExpirationTime = (Long) session.getAttribute("otpExpirationTime");
            if (otpExpirationTime != null && System.currentTimeMillis() < otpExpirationTime) {
                // Otp còn tồn tại và chưa hết hạn, bạn có thể truy cập vào nó ở đây
                int otp = (int) session.getAttribute("otp");
                // Sử dụng otp

                if (value == otp) {
                    request.setAttribute("email", request.getParameter("email"));
                    request.setAttribute("status", "success");
                    dispatcher = request.getRequestDispatcher("Common/NewPassword.jsp");
                    dispatcher.forward(request, response);
                } else {
                    throw new Exception();
                }

            } else {
                //OTP out of time
                request.setAttribute("message", "OTP đã quá hạn!");
                session.removeAttribute("otp");
                session.removeAttribute("otpExpirationTime");
                request.getRequestDispatcher("Common/EnterOTP.jsp").forward(request, response);
            }

        } catch (Exception e) {
            request.setAttribute("message", "Sai OTP!");
            request.getRequestDispatcher("Common/EnterOTP.jsp").forward(request, response);
        }

    }
}
