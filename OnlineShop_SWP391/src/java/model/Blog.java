/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author ADMIN
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Blog {

    private int blogId;
    private String title;
    private Date updateDate;
    private String content;
    private String thumbnail;
    private String briefInfor;
    private boolean status;
    public User u;
    public CategoryBlog cb;

    public Blog(int blogId, String title, String thumbnail, boolean status, User u, CategoryBlog cb) {
        this.blogId = blogId;
        this.title = title;
        this.thumbnail = thumbnail;
        this.status = status;
        this.u = u;
        this.cb = cb;
    }

    public Blog(int blogId, String title, Date updateDate, String content, String thumbnail, String briefInfor, boolean status) {
        this.blogId = blogId;
        this.title = title;
        this.updateDate = updateDate;
        this.content = content;
        this.thumbnail = thumbnail;
        this.briefInfor = briefInfor;
        this.status = status;
    }

}
