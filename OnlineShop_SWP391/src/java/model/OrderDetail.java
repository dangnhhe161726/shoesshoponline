/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class OrderDetail {

    private int OrderDetailId;
    private int quantity;
    private int orderId;
    private int productId;
    private int size;
    private double totalCost;
    public StatusOrder statusOrder;
    public Category ca;

    public OrderDetail(int quantity, int size, StatusOrder statusOrder, Category ca) {
        this.quantity = quantity;
        this.size = size;
        this.statusOrder = statusOrder;
        this.ca = ca;
    }

    public OrderDetail(int OrderDetailId, int quantity, StatusOrder statusOrder) {
        this.OrderDetailId = OrderDetailId;
        this.quantity = quantity;
        this.statusOrder = statusOrder;
    }

    public OrderDetail(int quantity, StatusOrder statusOrder) {
        this.quantity = quantity;
        this.statusOrder = statusOrder;
    }

    public OrderDetail(double totalCost) {
        this.totalCost = totalCost;
    }

    public OrderDetail(double totalCost, Category ca) {
        this.totalCost = totalCost;
        this.ca = ca;
    }

    public OrderDetail(int OrderDetailId, int quantity, int orderId, int productId, double totalCost, StatusOrder statusOrder) {
        this.OrderDetailId = OrderDetailId;
        this.quantity = quantity;
        this.orderId = orderId;
        this.productId = productId;
        this.totalCost = totalCost;
        this.statusOrder = statusOrder;
    }

}
