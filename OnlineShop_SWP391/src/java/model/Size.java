/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Size {

    private int sizeId;
    private int sizeName;
    private boolean status;
    private int quantity;

    public Size(int sizeName) {
        this.sizeName = sizeName;
    }
    

    @Override
    public String toString() {
        return "Size{" + "sizeId=" + sizeId + ", sizeName=" + sizeName + ", status=" + status + '}';
    }

}
