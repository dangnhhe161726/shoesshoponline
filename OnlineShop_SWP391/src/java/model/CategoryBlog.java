/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class CategoryBlog {
    private int categoryBlogId;
    private String categoryBlogName;
    private boolean status;

    public CategoryBlog(String categoryBlogName) {
        this.categoryBlogName = categoryBlogName;
    }

    public CategoryBlog(int categoryBlogId, String categoryBlogName) {
        this.categoryBlogId = categoryBlogId;
        this.categoryBlogName = categoryBlogName;
    }
    
}
