/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class FeedBack {

    private int feedbackId;
    private String FullName;
    private int ratedStar;
    private String feedback;
    private String image;
    private boolean status;
    private Date date;
    private User user;
    public Category c;

    public FeedBack(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    public FeedBack(int feedbackId, Category c) {
        this.feedbackId = feedbackId;
        this.c = c;
    }


    public FeedBack(int feedbackId, String FullName, int ratedStar, String feedback, String image, boolean status, Date date, User user) {
        this.feedbackId = feedbackId;
        this.FullName = FullName;
        this.ratedStar = ratedStar;
        this.feedback = feedback;
        this.image = image;
        this.status = status;
        this.date = date;
        this.user = user;
    }
    

    public FeedBack(String feedback, boolean status, int ratedStar) {
        this.feedback = feedback;
        this.status = status;
        this.ratedStar = ratedStar;
    }
    
}
