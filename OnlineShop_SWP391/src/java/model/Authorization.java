/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author asus
 */
public class Authorization {
    private int id;
    private int roleId;
    private String url;
    private String featureName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String FeatureName) {
        this.featureName = FeatureName;
    }

    public Authorization() {
    }

    public Authorization(int ID, int roleID, String url, String FeatureName) {
        this.id = ID;
        this.roleId = roleID;
        this.url = url;
        this.featureName = FeatureName;
    }
    
    
}
