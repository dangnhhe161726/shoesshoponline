/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class StatusOrder {

    private int StatusOrderId;
    private String StatusName;
    Product product;
    ProductImages pi;

    public StatusOrder(int StatusOrderId, String StatusName) {
        this.StatusOrderId = StatusOrderId;
        this.StatusName = StatusName;
    }

    public StatusOrder(int StatusOrderId, String StatusName, ProductImages pi) {
        this.StatusOrderId = StatusOrderId;
        this.StatusName = StatusName;
        this.pi = pi;
    }

    public StatusOrder(int StatusOrderId, String StatusName, Product product) {
        this.StatusOrderId = StatusOrderId;
        this.StatusName = StatusName;
        this.product = product;
    }

}
