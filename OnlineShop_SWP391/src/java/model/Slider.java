/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Slider {

    private int id;
    private String title;
    private String img;
    private String backlink;
    private String note;
    private boolean status;
    private int updatedBy;
    public User user;

    public Slider(int id, String title, String img, String backlink, boolean status) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.backlink = backlink;
        this.status = status;
    }

    public Slider(String title, String img, String backlink, String note, boolean status) {
        this.title = title;
        this.img = img;
        this.backlink = backlink;
        this.note = note;
        this.status = status;
    }

    public Slider(int id, String title, String img, String backlink, String note, boolean status) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.backlink = backlink;
        this.note = note;
        this.status = status;
    }

    public Slider(int id, String title, String img, String backlink, String note, boolean status, User u) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.backlink = backlink;
        this.note = note;
        this.status = status;
        this.user = u;
    }

}
